function doAjaxPost() {
    // get the form values
    var match_id = $('#match_id').val();
    var bet_name = $('#bet_name').val();

    $.ajax({
        type: "POST",
        url: "http://localhost:1923/gamblering-social-network/pariuri/adauga-meci.htm",
        data: "match_id=" + match_id + "&bet_name=" + bet_name,
        success: function(response){
            if(response.status == "SUCCESS"){
                userInfo = "<ol>";
                for(i =0 ; i < response.result.length ; i++){
                    userInfo += "<br><li><b>Meci</b> : " + response.result[i].match.home_team.team_name +
                    "-" + response.result[i].match.home_team.team_name;
                 }
                 userInfo += "</ol>";
                 $('#info').html("Meciul a fost adaugat cu success" + userInfo);
                 $('#name').val('');
                 $('#education').val('');
                 $('#error').hide('slow');
                 $('#info').show('slow');
             }else{
                 errorInfo = "";
                 for(i =0 ; i < response.result.length ; i++){
                     errorInfo += "<br>" + (i + 1) +". " + response.result[i].code;
                 }
                 $('#error').html("Please correct following errors: " + errorInfo);
                 $('#info').hide('slow');
                 $('#error').show('slow');
             }
         },
         error: function(e){
             alert('Eroare: ' + e);
         }
    });
}