<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../lib/taglib.jsp"%>
<link rel="stylesheet"
	href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/css/bootstrap-select.min.css" />
<script
	src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/js/bootstrap-select.min.js"></script>

<style>
.top-buffer {
	margin-top: 20px;
}

.mb {
	width: 100px;
}
</style>
<div class="col-md-8">
	<div class="row">
		<div class="col-sm-6 col-md-4">
			<div class="statbox widget box box-shadow">
				<div class="widget-content">
					<div class="visual green">
						<i class="icon-hand-up "></i>
					</div>
					<div class="title">Ultimile bilete</div>
					<div class="value">
						<h5>Aceasta pagina va ofera ultimile bilete jucate de
							tipsterii inregistrati.</h5>
					</div>
					<a class="more"
						href="<spring:url value="/pronosticuri/ultimile-bilete/1.html"/>">
						Vezi biletele<i class="pull-right icon-angle-right"></i>
					</a>
				</div>
			</div>
		</div>

		<div class="col-sm-6 col-md-4">
			<div class="statbox widget box box-shadow">
				<div class="widget-content">
					<div class="visual blue">
						<i class="icon-hand-up "></i>
					</div>
					<div class="title">Cele mai votate</div>
					<div class="value">
						<h5>Aceasta pagina va ofera cele mai votate bilete de catre
							vizitatori.</h5>
					</div>
					<a class="more"
						href="<spring:url value="/pronosticuri/cele-mai-multe-voturi/1.html"/>">
						Vezi biletele<i class="pull-right icon-angle-right"></i>
					</a>
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-md-4">
			<div class="statbox widget box box-shadow">
				<div class="widget-content">
					<div class="visual red">
						<i class="icon-certificate "></i>
					</div>
					<div class="title">Cele mai mari cote</div>
					<div class="value">
						<h5>
							Aceasta pagina va ofera biletele cu cele mai mari cote. <br>
							<br>
						</h5>
					</div>
					<a class="more"
						href="<spring:url value="pronosticuri/cele-mai-mari-cote/1.html"/>">Vezi
						biletele<i class="pull-right icon-angle-right"></i>
					</a>
				</div>
			</div>
			<!-- /.smallstat -->
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 col-md-4">
			<div class="statbox widget box box-shadow">
				<div class="widget-content">
					<div class="visual yellow">
						<i class="icon-user"></i>
					</div>
					<div class="title">Cei mai buni tipsteri</div>
					<div class="value">
						<h5>
							Aceasta pagina va ofera bilete jucate de profesionisti.<br>
							<br> <br>
						</h5>
					</div>
					<a class="more" href="<spring:url value="pronosticuri/clasamente.html"/>">Vezi biletele <i
						class="pull-right icon-angle-right"></i></a>
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-md-4">
			<div class="statbox widget box box-shadow">
				<div class="widget-content">
					<div class="visual red">
						<i class="icon-user"></i>
					</div>
					<div class="title">Dupa miza si siguranta</div>
					<div class="value">
						<h5>
							Aceasta pagina va ofera cele mai sigure bilete.<br> <br>
						</h5>
					</div>
					<a class="more" href="javascript:void(0);">Vezi biletele <i
						class="pull-right icon-angle-right"></i></a>
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-md-4">
			<div class="statbox widget box box-shadow">
				<div class="widget-content">
					<div class="visual green">
						<i class="icon-user"></i>
					</div>
					<div class="title">Ultimile bilete castigatoare</div>
					<div class="value">
						<h5>Aceasta pagina va ofera ultimile bilete castigatoare
							oferite de tipsteri.</h5>
					</div>
					<a class="more"
						href="<spring:url value="pronosticuri/bilete-castigatoare/1.html"/>">Vezi
						biletele <i class="pull-right icon-angle-right"></i>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Cautari dupa cote</div>
				<div class="panel-body">
					<div class="row">
						<form:form action="min-odd.html" method="POST"
							modelAttribute="min-odd">
							<div class="form-group">
								<label class="col-md-2 control-label">Alege cota minima:</label>
								<div class="col-md-8">
									<div class="input-group">
										<form:input path="odd_value" type="text"
											class="form-control input-number" min="1" />
										<span class="input-group-btn"> <form:button
												class="btn btn-success" type="submit">Cauta</form:button></span>
									</div>
								</div>
							</div>
						</form:form>
					</div>
					<div class="row top-buffer">
						<form:form action="max-odd.html" method="POST"
							modelAttribute="min-odd">
							<div class="form-group">
								<label class="col-md-2 control-label">Alege cota maxima:</label>
								<div class="col-md-8">
									<div class="input-group">
										<form:input path="odd_value" type="text"
											class="form-control input-number" min="1" />
										<span class="input-group-btn"> <form:button
												class="btn btn-success" type="submit">Cauta</form:button></span>
									</div>
								</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
