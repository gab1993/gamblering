<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../lib/taglib.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<style>
.top-buffer {
	margin-top: 20px;
}

.winner-status {
	background: #00CC00;
	color: white;
}

.looser-status {
	background: #A30000;
	color: white;
}

.not-played-status {
	background: #CACABC;
	color: white;
}
</style>
<c:url var="firstUrl" value="/pages/1" />
<c:url var="lastUrl" value="/pages/${ticketsLog.totalPages}" />
<c:url var="prevUrl" value="/pages/${currentIndex - 1}" />
<c:url var="nextUrl" value="/pages/${currentIndex + 1}" />
<div class="col-md-8">
	<c:if test="${success eq true }">
		<div class="alert alert-success">Multumim pentru votul acordat!
		</div>
	</c:if>
	<c:if test="${anonymous_vote eq true }">
		<div class="alert alert-danger">Trebuie sa fiti logat pentru a
			putea vota!</div>
	</c:if>
	<c:if test="${vote_exists eq true }">
		<div class="alert alert-danger">Ati votat deja acest bilet!</div>
	</c:if>
	<div class="pagination">
		<c:forEach items="${tickets}" var="ticket">
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th class="ticket-header">Meci</th>
							<th class="ticket-header">Data</th>
							<th class="ticket-header">Pronostic</th>
							<th class="ticket-header">Cota</th>
							<th class="ticket-header">Stare</th>
						</tr>
					</thead>
					<tbody class="searchable">
						<c:forEach items="${ticket.matchBets}" var="bet">
							<tr>
								<td class="ticket-content">${bet.match.home_team.team_name}-
									${bet.match.away_team.team_name}</td>
								<td class="ticket-content">${bet.match.match_time}</td>
								<td class="ticket-content">${bet.bet_name}</td>
								<td class="ticket-content">${bet.odd}</td>

								<c:choose>
									<c:when test="${bet.winner == 1}">
										<td class="ticket-content winner-status"><strong>Castigat</strong>
									</c:when>
									<c:when test="${bet.winner == 0 }">
										<td class="ticket-content looser-status"><strong>Pierdut</strong>
										</td>
									</c:when>
									<c:otherwise>
										<td class="ticket-content not-played-status"><strong>Neinceput</strong>
										</td>
									</c:otherwise>
								</c:choose>

							</tr>
						</c:forEach>
					</tbody>
					<tfoot>
						<tr>
							<td class="ticket-content text-center" colspan="1"><label
								class="ticket-footer"> <strong>Voturi:</strong>
									${fn:length(ticket.votes)}
							</label></td>
							<td class="ticket-content text-center" colspan="2"><form:form
									modelAttribute="vote" method="POST">
									<form:input path="ticket_id" value="${ticket.bet_id}"
										type="hidden" />
									<button type="submit" class="btn btn-success btn-xs">Voteaza</button>
								</form:form></td>
							<td class="ticket-content">Status bilet -></td>
							<c:if test="${ticket.validated == -1 }">
								<td colspan="1" class="not-played-status"><strong>Nevalidat</strong></td>
							</c:if>
							<c:if test="${ticket.validated == 1 }">
								<td colspan="1" class="winner-status"><strong>Castigat</strong></td>
							</c:if>
							<c:if test="${ticket.validated == 0 }">
								<td colspan="1" class="looser-status"><label
									class="pull-center">Pierdut</label></td>
							</c:if>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="table-responsive">
				<table class="table table-bordered">
					<tr>
						<td class="ticket-footer">Tipster: <a
							href="<spring:url value="/profil/${ticket.tipster.username}.html"/>">
								<font color="white">${ticket.tipster.username} </font>
						</a></td>
						<td class="ticket-footer">Plasat pe ${ticket.betTime}</td>
						<td colspan="2" class="ticket-footer">Cota finala: <fmt:formatNumber
								type="number" pattern="#.##" value="${ticket.total_odd}" />
						</td>
						<td class="ticket-footer">Miza: ${ticket.stake}</td>
					</tr>
				</table>
			</div>
			<br>
		</c:forEach>
		<c:choose>
			<c:when test="${currentIndex == 1}">
				<li class="disabled"><a href="#">&lt;&lt;</a></li>
				<li class="disabled"><a href="#">&lt;</a></li>
			</c:when>
			<c:otherwise>
				<li><a href="${firstUrl}">&lt;&lt;</a></li>
				<li><a href="${prevUrl}">&lt;</a></li>
			</c:otherwise>
		</c:choose>
		<c:forEach var="i" begin="${beginIndex}" end="${endIndex}">
			<c:url var="pageUrl" value="/pages/${i}" />
			<c:choose>
				<c:when test="${i == currentIndex}">
					<li class="active"><a href="${pageUrl}"><c:out
								value="${i}" /></a></li>
				</c:when>
				<c:otherwise>
					<li><a href="${pageUrl}"><c:out value="${i}" /></a></li>
				</c:otherwise>
			</c:choose>
		</c:forEach>
		<c:choose>
			<c:when test="${currentIndex == ticketsLog.totalPages}">
				<li class="disabled"><a href="#">&gt;</a></li>
				<li class="disabled"><a href="#">&gt;&gt;</a></li>
			</c:when>
			<c:otherwise>
				<li><a href="${nextUrl}">&gt;</a></li>
				<li><a href="${lastUrl}">&gt;&gt;</a></li>
			</c:otherwise>
		</c:choose>
	</div>
</div>
