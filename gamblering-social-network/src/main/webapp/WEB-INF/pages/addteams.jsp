<%@ include file="../lib/taglib.jsp"%>
<style>
.top-buffer {
	margin-top: 20px;
}
</style>
<div class="row row top-buffer">
	<c:if test="${success eq  true}">
		<div class="alert alert-success">Ati adaugat o noua echipa!</div>
	</c:if>
</div>
<div class="col-md-8">
	<div class="widget box">
		<div class="widget-header">
			<h4>
				<i class="icon-reorder"></i> Adauga echipa
			</h4>
		</div>
		<div class="widget-content">
			<div class="row">
				<form:form id="addleagues" modelAttribute="team" class="form-inline">
					<div class="col-md-5">
						<div class="form-group">
							<form:select path="league_id" class="selectpicker"
								data-live-search="true" placeholder="Alege campionat">
								<c:forEach items="${admin_leagues}" var="league">
									<form:option value="${league.league_id}">${league.league_name}
															&nbsp; <strong>${league.league_country}</strong>
									</form:option>
								</c:forEach>
							</form:select>
							<span class="help-block align-center">Alege campionatul</span>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<form:input id="team_name" path="team_name"></form:input>
							<span class="help-block align-center">Numele echipei</span>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<form:button class="btn btn-success button-next" type="submit">Salveaza</form:button>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>

<div class="col-md-4">
	<div class="widget box">
		<div class="widget-header">
			<h4>
				<i class="icon-info-sign"></i> Ligi curente
			</h4>
		</div>
		<ul class="list-group">
			<c:forEach items="${admin_leagues}" var="league">

				<li class="list-group-item text-left"><a
					href="<spring:url value="/liga/${league.league_id}.html"/>"> <label
						class="name"> ${league.league_country}
							(${league.league_name}) </label>
				</a>
					<div class="break"></div></li>
			</c:forEach>
		</ul>
	</div>
</div>