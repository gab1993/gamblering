<%@ include file="../lib/taglib.jsp"%>


<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<title>Pagina logare</title>

<!--=== CSS ===-->

<!-- Bootstrap -->
<link
	href="<%=request.getContextPath()%>/resources/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />

<!-- Theme -->
<link href="<%=request.getContextPath()%>/resources/assets/css/main.css"
	rel="stylesheet" type="text/css" />
<link
	href="<%=request.getContextPath()%>/resources/assets/css/plugins.css"
	rel="stylesheet" type="text/css" />
<link
	href="<%=request.getContextPath()%>/resources/assets/css/responsive.css"
	rel="stylesheet" type="text/css" />
<link
	href="<%=request.getContextPath()%>/resources/assets/css/icons.css"
	rel="stylesheet" type="text/css" />

<!-- Login -->
<link
	href="<%=request.getContextPath()%>/resources/assets/css/login.css"
	rel="stylesheet" type="text/css" />

<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/assets/css/fontawesome/font-awesome.min.css">
<!--[if IE 7]>
		<link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
	<![endif]-->

<!--[if IE 8]>
		<link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
	<![endif]-->
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700'
	rel='stylesheet' type='text/css'>

<!--=== JavaScript ===-->

<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/assets/js/libs/jquery-1.10.2.min.js"></script>

<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/assets/js/libs/lodash.compat.min.js"></script>

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
		<script src="assets/js/libs/html5shiv.js"></script>
	<![endif]-->

<!-- Beautiful Checkboxes -->
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/plugins/uniform/jquery.uniform.min.js"></script>

<!-- Form Validation -->
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/plugins/validation/jquery.validate.min.js"></script>

<!-- Slim Progress Bars -->
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/plugins/nprogress/nprogress.js"></script>

<!-- App -->
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/assets/js/login.js"></script>
<script>
	$(document).ready(function() {
		"use strict";

		Login.init(); // Init login JavaScript
	});
</script>
</head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<body class="login">
	<!-- Logo -->
	<div class="logo">
		<strong>GAMBLER</strong>ING
	</div>
	<!-- /Logo -->

	<!-- Login Box -->
	<div class="box">
		<div class="content">
			<c:if test="${failure eq true}">
				<div class="alert alert-warning">Username sau parola gresita.</div>
			</c:if>
			<form role="form"
				class="form-vertical login-form"
				action="<spring:url value="/j_spring_security_check"/>"
				method="POST">
				
				<h3 class="form-title">Formular logare</h3>
				<div class="alert fade in alert-danger" style="display: none;">
					<i class="icon-remove close" data-dismiss="alert"></i> Introdu username-ul si parola.
				</div>

				<!-- Input Fields -->
				<div class="form-group">
					<div class="input-icon">
						<i class="icon-user"></i> 
						
							<input type="text" name="j_username"
								   class="form-control" placeholder="Username" autofocus="autofocus"
								   data-rule-required="true"
								   data-msg-required="Please enter your username." />
					</div>
				</div>
				
				<div class="form-group">
					<div class="input-icon">
						<i class="icon-lock"></i> 
						<input type="password" name="j_password"
							   class="form-control" placeholder="Password"
							   data-rule-required="true"
							   data-msg-required="Please enter your password." />
					</div>
				</div>
				<!-- /Input Fields -->

				<!-- Form Actions -->
				<div class="form-actions">
					<label class="checkbox pull-left"><input type="checkbox"
						class="uniform" name="remember"> Salveaza parola</label>
					<button type="submit" class="btn btn-success pull-right">
						Logheaza-ma <i class="icon-angle-right"></i>
					</button>
				</div>
			</form>
			<!-- /Login Formular -->



			<!-- Forgot Password Form -->
			<div class="inner-box">
				<div class="content">
					<!-- Close Button -->
					<i class="icon-remove close hide-default"></i>

					<!-- Link as Toggle Button -->
					<a href="#" class="forgot-password-link">Ai uitat parola?</a>

					<!-- Forgot Password Formular -->
					<form class="form-vertical forgot-password-form hide-default"
						action="login.html" method="post">
						<!-- Input Fields -->
						<div class="form-group">
							<!--<label for="email">Email:</label>-->
							<div class="input-icon">
								<i class="icon-envelope"></i> <input type="text" name="email"
									class="form-control" placeholder="Enter email address"
									data-rule-required="true" data-rule-email="true"
									data-msg-required="Please enter your email." />
							</div>
						</div>
						<!-- /Input Fields -->

						<button type="submit" class="submit btn btn-default btn-block">
							Reseteaza parola</button>
					</form>
					<!-- /Forgot Password Formular -->

					<!-- Shows up if reset-button was clicked -->
					<div class="forgot-password-done hide-default">
						<i class="icon-ok success-icon"></i>
						<!-- Error-Alternative: <i class="icon-remove danger-icon"></i> -->
						<span>Ti-a fost trimis un email cu parola.</span>
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /Forgot Password Form -->
		</div>
		<!-- /Login Box -->
	</div>
	<!-- Single-Sign-On (SSO) -->
	<div class="single-sign-on">
		<span>sau</span>

		<button class="btn btn-facebook btn-block">
			<i class="icon-facebook"></i> Sign in with Facebook
		</button>

		<button class="btn btn-twitter btn-block">
			<i class="icon-twitter"></i> Sign in with Twitter
		</button>

		<button class="btn btn-google-plus btn-block">
			<i class="icon-google-plus"></i> Sign in with Google
		</button>
	</div>
	<!-- /Single-Sign-On (SSO) -->

	<!-- Footer -->
	<div class="footer">
		<a href="<spring:url value='/inregistrare.html'/>">Nu ai un cont pe Gamblering? <strong>inregistreaza-te acum!</strong></a>
		<p>
			Drepturi de autor Tifui Gabriel
		</p>
	</div>
	<!-- /Footer -->
</body>
</html>