<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../lib/taglib.jsp"%>

<div class="row row top-buffer">
	<div class="col-md-6">
		<div class="colm-md-4">
			<div class="row clearfix">
				<div class="col-md-12 column">
					<div class="widget box">
						<div class="widget-header">
							<h4>
								<i class="icon-reorder"></i> Adauga meci
							</h4>
						</div>
						<div class="widget-content">
							<div class="panel panel-primary filterable">
								<div class="pull-right">
									<button class="btn btn-default btn-xs btn-filter">
										<span class="glyphicon glyphicon-filter"></span> Filter
									</button>
								</div>
								<table class="table">
									<thead>
										<tr class="filters">
											<th><input type="text" class="form-control"
												placeholder="Id liga" disabled></th>
											<th><input type="text" class="form-control"
												placeholder="Nume liga" disabled></th>
											<th><input type="text" class="form-control"
												placeholder="Tara" disabled></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${admin_leagues}" var="league">
											<tr>
												<td>${league.league_id}</td>
												<td>${league.league_name }</td>
												<td>${league.league_country }</td>
												<td><a
													href="<spring:url value="/administratie/adauga-meci/liga/${league.league_id}.html"/>">
														Adauga meci </a></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="colm-md-4">
			<div class="row clearfix">
				<div class="col-md-12 column">
					<div class="widget box">
						<div class="widget-header">
							<h4>
								<i class="icon-reorder"></i> Adauga scoruri
							</h4>
						</div>
						<div class="widget-content">
							<div class="panel panel-primary filterable">
								<div class="pull-right">
									<button class="btn btn-default btn-xs btn-filter">
										<span class="glyphicon glyphicon-filter"></span> Filter
									</button>
								</div>
								<table class="table">
									<thead>
										<tr class="filters">
											<th><input type="text" class="form-control"
												placeholder="Id liga" disabled></th>
											<th><input type="text" class="form-control"
												placeholder="Nume liga" disabled></th>
											<th><input type="text" class="form-control"
												placeholder="Tara" disabled></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${admin_leagues}" var="league">
											<tr>
												<td>${league.league_id}</td>
												<td>${league.league_name }</td>
												<td>${league.league_country }</td>
												<td><a
													href="<spring:url value="/administratie/adauga-scor/liga/${league.league_id}.html"/>">
														Adauga scoruri </a></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>