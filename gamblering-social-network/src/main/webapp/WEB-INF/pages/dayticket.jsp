<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../lib/taglib.jsp"%>

<div class="col-md-8">
	<ul class="widget-products">
		<c:forEach items="${tickets}" var="ticket">
			<li><a href="#"> <span class="img"> <img
						class="img-thumbnail" src="http://lorempixel.com/400/400/food/1/"
						alt="">
				</span> <span class="product clearfix"> <span class="name">
							Biletul zilei ${ticket.betTime} </span> <span class="price"> <i
							class="fa fa-money"></i> Cota: ${ticket.total_odd}
					</span> <span class="warranty"> <i class="fa fa-certificate"><button
									class="btn btn-success">Voteaza</button></i> Voturi:
							${ticket.vnumber}
					</span>
				</span>
			</a></li>
		</c:forEach>
	</ul>
</div>
<div class="col-md-4"></div>