<%@ include file="../lib/taglib.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<style>
.spacer {
	margin-top: 40px; /* define margin as you see fit */
}

.dropdown-menu li a:hover :focus {
	color: black; /*color turns white if not changed*/
	background: none;
}
</style>
<body>
	<div class="page-header">
		<div class="page-title">
			<h3>Profilul lui ${auth_user_username}</h3>
		</div>

		<!-- Page Stats -->
		<ul class="page-stats">
			<li>
				<div class="summary">
					<span>Bilete castigatoare</span>
					<h3>${status}</h3>
				</div>
			</li>
			<li>
				<div class="summary">
					<span>Banca</span>
					<h3>${bankroll}LEI</h3>
				</div>
			</li>
		</ul>
		<!-- /Page Stats -->
	</div>
	<!-- /Page Header -->

	<!--=== Page Content ===-->
	<!--=== Inline Tabs ===-->
	<div class="row">
		<div class="col-md-12">
			<!-- Tabs-->
			<div class="tabbable tabbable-custom tabbable-full-width">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab_overview" data-toggle="tab">Profil</a></li>
					<li><a href="#tab_edit_account" data-toggle="tab">Editeaza
							profilul</a></li>

				</ul>
				<div class="tab-content row">
					<!--=== Overview ===-->
					<div class="tab-pane " id="tab_tickets">
						<p>Bilete</p>
					</div>
					<div class="tab-pane active" id="tab_overview">
						<div class="col-md-3">
							<div class="list-group">
								<li class="list-group-item no-padding "><img
									src="<%=request.getContextPath()%>/resources/assets/img/demo/avatar-large.jpg"
									alt=""></li> <a href="javascript:void(0);"
									class="list-group-item"> <span>${auth_user_role}</span></a> <a
									href="javascript:void(0);" class="list-group-item">Bilete</a> <a
									href="javascript:void(0);" class="list-group-item">Mesaje</a> <a
									href="javascript:void(0);" class="list-group-item"><span
									class="badge">${auth_followers_no}</span>Urmaritori</a> <a
									href="javascript:void(0);" class="list-group-item">Setari</a>
							</div>
						</div>

						<div class="col-md-9">
							<div class="row profile-info">
								<div class="col-md-7">
									<div class="widget">
										<div class="widget-header">
											<h4>
												<i class="icon-user"></i>Informatii generale
											</h4>
										</div>
										<div class="widget-content">
											<dl class="dl-horizontal">
												<dt>Nume:</dt>
												<dd>&nbsp;${my_profile.first_name}</dd>
											</dl>

											<dl class="dl-horizontal">
												<dt>Prenume:</dt>
												<dd>&nbsp;${my_profile.second_name}</dd>
											</dl>

											<dl class="dl-horizontal">
												<dt>Sex:</dt>
												<dd>&nbsp;${my_profile.gender}</dd>
											</dl>

											<dl class="dl-horizontal">
												<dt>Data nastere:</dt>
												<dd>&nbsp;${my_profile.birthdate}</dd>
											</dl>

											<dl class="dl-horizontal">
												<dt>Tara:</dt>
												<dd>&nbsp;${my_profile.country}</dd>
											</dl>

											<dl class="dl-horizontal">
												<dt>Oras:</dt>
												<dd>&nbsp;${my_profile.city}</dd>
											</dl>
											<dl class="dl-horizontal">
												<dt>Betagency:</dt>
												<dd>&nbsp;${my_profile.betagency}</dd>
											</dl>
										</div>
									</div>
								</div>
								<div class="col-md-5">
									<div class="widget">
										<div class="widget-header">
											<h4>
												<i class="icon-list-alt"></i> Ultimul bilet
											</h4>
										</div>
										<div class="widget-content">
											<table class="table table-responsive">
												<thead>
													<tr>
														<td class="ticket-header">Meci</td>
														<td class="ticket-header">Pariu</td>
														<td class="ticket-header">Cota</td>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${last_ticket.matchBets}" var="bet">
														<tr>
															<td class="ticket-content">${bet.match.home_team.team_name}-
																${bet.match.away_team.team_name}</td>
															<td class="ticket-content">${bet.bet_name}</td>
															<td class="ticket-content">${bet.odd}</td>
														</tr>
													</c:forEach>
												</tbody>
												<tfoot>
													<tr>
														<td class="ticket-footer">Cota: <fmt:formatNumber
																type="number" pattern="#.##"
																value="${last_ticket.total_odd}" />
														</td>
														<td class="ticket-footer">Miza: ${last_ticket.stake}</td>
														<c:set var="castig"
															value="${last_ticket.total_odd * last_ticket.stake}" />
														<td class="ticket-footer">Castig: <fmt:formatNumber
																type="currency" value="${castig}" /></td>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
							<!-- /.row -->
							<div class="row spacer"></div>
							<div class="row">
								<div class="col-md-12">
									<div class="widget">
										<div class="widget-header">Ultimile bilete</div>
										<!--Widget Header-->
										<div class="widget-body">
											<div class="widget-main no-padding">
												<div class="tickets-container">
													<ul class="tickets-list">
														<c:forEach items="${last_five}" var="ticket">
															<li class="ticket-item">
																<div class="row">
																	<div class="ticket-user col-md-6 col-sm-12">
																		Cota totala:
																		<fmt:formatNumber type="number" pattern="#.##"
																			value="${ticket.total_odd}" />
																	</div>
																	<div class="ticket-time  col-md-4 col-sm-6 col-xs-12">
																		<div class="divider hidden-md hidden-sm hidden-xs"></div>
																		<i class="fa fa-clock-o"></i> <span class="time">
																			${ticket.betTime} </span>
																	</div>
																	<div class="ticket-type  col-md-2 col-sm-6 col-xs-12">
																		<span class="divider hidden-xs"></span> <span
																			class="type">Vezi biletul</span>
																	</div>
																	<div class="ticket-state bg-yellow">
																		<i class="fa fa-info">${ticket.stake}</i>
																	</div>
																</div>
															</li>
														</c:forEach>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- /Striped Table -->
							</div>
							<!-- /.row -->
						</div>
						<!-- /.col-md-9 -->
					</div>
					<!-- /Overview -->

					<%@ taglib prefix="form"
						uri="http://www.springframework.org/tags/form"%>

					<!--=== Edit Account ===-->
					<div class="tab-pane" id="tab_edit_account">

						<form:form class="form-horizontal" modelAttribute="profile">
							<div class="col-md-12">
								<div class="widget">
									<div class="widget-header">
										<h4>Informatii generale</h4>
									</div>
									<div class="widget-content">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="col-md-4 control-label">Nume: </label>
													<div class="col-md-8">
														<form:input type="text" path="first_name" name="regular"
															class="form-control" value="${my_profile.first_name}" />
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-4 control-label">Prenume: </label>
													<div class="col-md-8">
														<form:input type="text" path="second_name" name="regular"
															class="form-control" value="${my_profile.second_name }" />
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-4 control-label">Tara: </label>
													<div class="col-md-8">
														<form:select path="country" data-placeholder="Alege tara"
															class="form-control">
															<c:forEach items="${all_countries}" var="country">
															<form:option value="${country }" selected="selected">${country}</form:option>
															</c:forEach>
														</form:select>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-4 control-label">Oras: </label>
													<div class="col-md-8">
														<form:input type="text" path="city" name="regular"
															class="form-control" value="${my_profile.city}" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-4 control-label">Agentie</label>
													<div class="col-md-8">
														<form:select path="betagency"
															data-placeholder="Casa de pariuri" class="form-control">
															<form:option value="Bet365" selected="selected">Bet365</form:option>
															<form:option value="Bwin" selected="selected">Bwin</form:option>
															<form:option value="SportingBet" selected="selected">SportingBet</form:option>
														</form:select>
													</div>
												</div>
											</div>


											<div class="col-md-6">
												<div class="form-group">
													<label class="col-md-4 control-label">Sex:</label>
													<div class="col-md-8">
														<form:select class="form-control" path="gender">
															<form:option value="Barbat" selected="selected">Masculin</form:option>
															<form:option value="Femeie">Feminin</form:option>
														</form:select>
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-4 control-label">Data nastere:</label>
													<div class="col-md-8">
														<div class='input-group date' id='datetimepicker1'>
															<form:input type='text' path="birthdate"
																class="form-control" />
															<span class="input-group-addon"> <span
																class="glyphicon glyphicon-calendar"></span>
															</span>
														</div>
													</div>
												</div>

											</div>
											<!-- /.row -->
										</div>
										<!-- /.widget-content -->
									</div>
									<!-- /.widget -->
									<div class="form-actions">
										<input type="submit" value="Salveaza"
											class="btn btn-primary pull-right" />
									</div>
								</div>
							</div>
						</form:form>
						<!-- /.col-md-12 -->

						<!--  detalii cont -->
						<form:form>
							<div class="col-md-12 form-vertical no-margin">
								<div class="widget">
									<div class="widget-header">
										<h4>Settings</h4>
									</div>
									<div class="widget-content">
										<div class="row">
											<div class="col-md-4 col-lg-2">
												<strong class="subtitle padding-top-10px">Username:
												</strong>
												<p class="text-muted">Username-ul nu se poate schimba.</p>
											</div>

											<div class="col-md-8 col-lg-10">
												<div class="form-group">
													<label class="control-label padding-top-10px">Username:</label>
													<input type="text" name="username" class="form-control"
														value="${my_profile.username}" disabled="disabled">
												</div>
											</div>
										</div>
										<!-- /.row -->
										<div class="row">
											<div class="col-md-4 col-lg-2">
												<strong class="subtitle">Schimba parola</strong>
												<p class="text-muted">Poti folosi acest formular pentru
													a schimba parola</p>
											</div>

											<div class="col-md-8 col-lg-10">
												<div class="form-group">
													<label class="control-label">Parola veche:</label> <input
														type="password" name="old_password" class="form-control"
														placeholder="Lasa acest spatiu gol daca nu vrei sa schimbi parola">
												</div>

												<div class="form-group">
													<label class="control-label">Parola noua:</label> <input
														type="password" name="new_password" class="form-control"
														placeholder="Lasa acest spatiu gol daca nu vrei sa schimbi parola">
												</div>

												<div class="form-group">
													<label class="control-label">Repeta parola noua:</label> <input
														type="password" name="new_password_repeat"
														class="form-control"
														placeholder="Lasa acest spatiu gol daca nu vrei sa schimbi parola">
												</div>

											</div>
										</div>
										<!-- /.row -->
									</div>
									<!-- /.widget-content -->
								</div>
								<!-- /.widget -->

								<div class="form-actions">
									<input type="submit" value="Salveaza"
										class="btn btn-primary pull-right">
								</div>
							</div>
							<!-- /.col-md-12 -->
						</form:form>
					</div>
					<!-- /Edit Account -->
				</div>
				<!-- /.tab-content -->
			</div>
			<!--END TABS-->
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			$('#datetimepicker1').datetimepicker();
		});
	</script>
</body>