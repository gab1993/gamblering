<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../lib/taglib.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="col-md-8">
	<div class="widget box">
		<div class="widget-header winner-status">
			<h4>
				<i class="icon-reorder"></i> Dupa castigurile din luna recenta
			</h4>
			<div class="toolbar no-padding">
				<div class="btn-group">
					<span class="btn btn-xs widget-collapse"><i
						class="icon-angle-down"></i></span>
				</div>
			</div>
		</div>
		<div class="widget-content no-padding">
			<table
				class="table table-striped table-bordered table-hover table-checkable table-responsive datatable">
				<thead>
					<tr>
						<th data-class="expand">Username</th>
						<th>Banca</th>
						<th>Cota cumulata</th>
						<th>Bilete castigatoare</th>
						<th data-hide="phone,tablet">Status</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${tipsters}" var="tipster">
						<tr>
							<td><a
								href="<spring:url value="/profil/${tipster.username}.html"/>">${tipster.username}</a></td>
							<td>${tipster.bankroll}</td>
							<td>${tipster.total_odd}</td>
							<td>${tipster.status}</td>
							<td><span class="label label-success">V.I.P</span></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>