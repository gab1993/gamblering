<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../lib/taglib.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
.top-buffer {
	margin-top: 20px;
}

.popular-post {
	background: #f9f9f9;
}

.line-item {
	padding: 30px 0px 0px;
	box-shadow: 5px 5px 5px #D8D8D8;
	border: 1px #e4e4e4 solid;
}

.content-image {
	float: right;
	margin-bottom: 10px;
	margin-left: 25px;
	margin-top: 5px;
}

.post-title {
	margin-bottom: 7px;
	margin-top: 3px;
}

.post-title, post-title a {
	font-size: 18px;
	line-height: 20px;
}

.summary {
	font-size: 14px;
	line-height: 18px;
	margin-bottom: 8px;
	with: 100%;
}

.teaser {
	margin-bottom: 32px;
	margin-top: 19px;
	text-align: center;
}

.article-info {
	padding: 0 29px;
}

.item-odd .article-category {
	margin-top: 8px;
}

.article-title {
	margin-bottom: 7px;
	margin-top: 3px;
}

.article-title, .subfeatured .article-title a {
	font-size: 18px;
	font-weight: normal;
	line-height: 20px;
}

.article-summary {
	color: #666;
	font-size: 14px;
	line-height: 16px;
	max-height: 90px;
	overflow: hidden;
	font-size: 14px;
	line-height: 16px;
	max-height: 90px;
	overflow: hidden;
	max-height: 90px;
	overflow: hidden;
	font-size: 14px;
	line-height: 16px;
	max-height: 90px;
	overflow: hidden;
	line-height: 16px;
	max-height: 90px;
	overflow: hidden;
	font-size: 14px;
	line-height: 16px;
	max-height: 90px;
}

.footer {
	position: absolute;
	bottom: 0px;
	width: 100%;
}

.panel-footer {
	margin-top: 30px;
	background-color: #E9E9E9;
	border-bottom-right-radius: 3px;
	border-bottom-left-radius: 3px;
}

.padded {
	padding: 2px;
}
</style>

<div class="col-md-8">
	<article>
		<div class="line-item hf-item-odd clearfix">
			<div class="hf-info">
				<h2 class="post-title">
					<a class="article-link" href=""> Gamblering - Cine suntem noi?<span
						class="overlay article-overlay"></span>
					</a>
				</h2>
				<div class="summary">
					<br>
					<div align="center">
						<iframe width="560" height="315"
							src="https://www.youtube.com/embed/XAdU_P9m1J8" frameborder="0"
							allowfullscreen></iframe>
					</div>
					<br> <br> Aici puteti gasi cele mai bune ponturi si
					recomadari, analize si pronosticuri sportive pentru pariuri. <br>
					<br> Toate informatiile si stirile de ultima ora puse la
					dispozitia ta pentru a putea face cele mai bune alegeri pentru
					biletul zilei. Biletul zilei este o rubrica unde zilnic gasesti
					propuneri din: fotbal, tenis, baschet si alte sporturi. <br> <br>
					Fie ca sunt cote mici sau mari fie ca sunt pronosticuri single sau
					combinate toate au un singur scop: biletul zilei sa fie castigator.
					Desi noi propunem mai multe variante, voi studiati-le pe toate,
					alegeti doar care vi se par cele mai bine si adaugati-le pe biletul
					zilei. <br> <br> Va dorim sa aveti o zi buna si castiguri
					cat mai mari cu BILETUL ZILEI!
				</div>

			</div>
			<br>
		</div>
	</article>
	<div class="page-header">
		<h1>Noutati</h1>
	</div>
	<c:forEach items="${posts}" var="post">
		<article>
			<div class="line-item hf-item-odd clearfix">
				<div class="content-image">
					<a class="image-link article-link" href="${post.link}"> <img
						class="img-thumbnail" src="${post.img_link}"> <span
						class="overlay article-overlay"></span>
					</a>
				</div>
				<div class="hf-info">
					<h2 class="post-title">
						<a class="article-link" href="${post.link}"> ${post.title } <span
							class="overlay article-overlay"></span>
						</a>
					</h2>
					<div class="summary">${post.description}</div>
					<div class="hf-tags">
						<div class="label label-default">pariuri</div>
						<div class="label label-default">case pariuri</div>
						<div class="label label-default">bet365</div>
						<div class="label label-default">pariuri live</div>

					</div>
				</div>
				<br>
				<div class="panel-footer">
					<div class="row">
						<div class="col-md-6">
							<a href="${post.link}">
								<button class="btn btn-sm btn-info">Citeste mai mult</button>
							</a>
						</div>
						<div class="col-md-6" align="right">Postat pe:
							${post.post_date}</div>
					</div>
				</div>
			</div>
		</article>
		<br>
	</c:forEach>
</div>