<%@ include file="../lib/taglib.jsp"%>

<div class="col-md-12">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<div class="widget box">
				<div class="widget-header">
					<h4>
						<i class="icon-reorder"></i> Adauga scor
					</h4>
				</div>
				<div class="widget-content">
					<form:form class="form-inline" modelAttribute="scores">

						<c:forEach items="${league_matches}" var="match" varStatus="it">
							<div class="row">
								<div class="col-md-3">${match.home_team.team_name}vs
									${match.away_team.team_name}</div>
								<div class="col-md-2">
									<div class="form-group">
										<form:input type="hidden" path="matches[${it.index}].match_id" value="${match.match_id}"/>
										<form:input type="text" path="matches[${it.index}].firstHalfScore.home_team_goals" 
										placeholder="Echipa gazda" />
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<form:input type="text" path="matches[${it.index}].firstHalfScore.away_team_goals"
										placeholder="Echipa oaspete" />
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<form:input type="text" path="matches[${it.index}].secondHalfScore.home_team_goals"
										placeholder="Echipa gazda" />
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<form:input type="text" path="matches[${it.index}].secondHalfScore.away_team_goals"
										placeholder="Echipa oaspete" />
									</div>
								</div>
							</div>
						</c:forEach>
						<button type="submit" class="btn btn-success">Salveaza</button>
					</form:form>

				</div>
			</div>
		</div>
	</div>
</div>
