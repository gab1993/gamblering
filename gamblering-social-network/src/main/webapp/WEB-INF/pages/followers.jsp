<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../lib/taglib.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
.top-buffer {
	margin-top: 20px;
}
</style>

<c:if test="${fn:length(tipsteri_urmariti) eq 0}">
	<div class="alert alert-info">Nu urmariti niciun tipster.</div>
</c:if>

<div class="col-md-8">
	<ul class="list-group">
		<c:forEach items="${tipsteri_urmariti}" var="tipster" varStatus="loop">
			<li class="list-group-item text-left"><img class="img-thumbnail"
				src="http://bootdey.com/img/Content/user_1.jpg"> <label
				class="name"> ${tipster.username} </label> 
				<label class="pull-right">
					<a class="btn btn-success btn-block"
					href="<spring:url value="/profil/${tipster.username}.html"/>">
						Vezi profilul</a> <a class="btn btn-inverse btn-block"
					href="<spring:url value='/pronosticuri/tipster/${tipster.username}/1.html'/>">Bilete</a>

					<button type="button" class="btn btn-block btn-info"
						data-toggle="modal" data-target="#tipster_modal${loop.index}">Informatii</button>

					<div id="tipster_modal${loop.index}" class="modal fade"
						role="dialog">
						<div class="modal-dialog">

							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">${tipster.username}</h4>
								</div>
								<div class="table-responsive">
									<table class="table table-hover">
										<tbody>
											<tr>
												<td>Cont</td>
												<td>User</td>
											</tr>
											<tr>
												<td>Status</td>
												<td>${tipster.status}</td>
											</tr>
											<tr>
												<td>Banca</td>
												<td>${tipster.bankroll}</td>
											</tr>
											<tr>
												<td>Loc in clasament</td>
												<td>1</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default"
										data-dismiss="modal">Close</button>
								</div>
							</div>

						</div>
					</div>
			</label>

				<div class="break"></div></li>

		</c:forEach>
	</ul>
</div>
