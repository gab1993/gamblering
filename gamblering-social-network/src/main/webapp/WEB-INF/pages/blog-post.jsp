<%@ include file="../lib/taglib.jsp"%>

<div class="col-md-2"></div>
<div class="col-md-8">
	<form:form modelAttribute="post" method="POST">
		<div class="form-group">
			<label for="title">Titlul: </label>
			<form:input id="title" type="text" path="title" class="form-control" />
		</div>
		<div class="form-group">
			<label for="category">Categorie: </label>
			<form:input id="category" type="text" path="category"
				class="form-control" />
		</div>
		<div class="form-group">
			<label for="link">Link postare: </label>
			<form:input id="link" type="text" path="link" class="form-control" />
		</div>

		<div class="form-group">
			<label for="img_link">Link imagine: </label>
			<form:input id="img_link" type="text" path="img_link"
				class="form-control" />
		</div>

		<div class="form-group">
			<label for="keywords">Cuvinte cheie</label>
			<form:input id="keywords" type="text" path="keywords"
				class="form-control" />
		</div>
		<div class="form-group">
			<form:textarea class="form-control" path="description"
				placeholder="Text" style="width:100%" name="description" rows="10"
				cols="30"></form:textarea>
		</div>
		<div class="form-group">
			<form:button class="btn btn-sm btn-success" type="submit">Posteaza</form:button>
		</div>
	</form:form>
</div>
<div class="col-md-2"></div>
