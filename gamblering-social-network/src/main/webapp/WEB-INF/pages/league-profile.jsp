<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../lib/taglib.jsp"%>

<ul class="list-group">
	<c:forEach items="${league_teams}" var="team">
		<li> ${team.team_name}</li>
	</c:forEach>
</ul>