<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../lib/taglib.jsp"%>
<style>
.top-buffer {
	margin-top: 20px;
}
</style>
<div class="col-md-8">
	<ul class="list-group">
		<c:forEach items="${country_leagues}" var="league">
			<li class="list-group-item text-left"><img class="img-thumbnail"
				src="http://bootdey.com/img/Content/user_1.jpg"> <label
				class="name"> ${league.league_name } </label> <label
				class="pull-right"> <a class="btn btn-success btn-block"
					href="<spring:url value="/pariuri/liga/${league.league_id}.html"/>">
						Vezi meciurile</a> <a class="btn btn-inverse btn-block" href="#">Top
						meciuri jucate</a> <a class="btn btn-info btn-block" href="#">Top
						tipsteri</a>
			</label>
				<div class="break"></div></li>

		</c:forEach>
	</ul>
</div>

