<%@ include file="../lib/taglib.jsp"%>
<style>
.top-buffer {
	margin-top: 20px;
}

.center {
	margin: 0 auto;
}
</style>
<div class="row row top-buffer">
	<c:if test="${success eq  true}">
		<div class="alert alert-success">Ati adaugat un meci nou</div>
	</c:if>
</div>
<div class="col-md-12">
	<div class="colm-md-4">
		<div class="row clearfix">
			<div class="col-md-12 column">
				<div class="widget box">
					<div class="widget-header">
						<h4>
							<i class="icon-reorder"></i> Adauga meci
						</h4>
					</div>
					<form:form id="addleagues" modelAttribute="match"
						class="form-inline">
						<div class="widget-content">
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<form:select path="home_team.team_id" class="selectpicker"
											data-live-search="true" placeholder="Echipa gazda">
											<c:forEach items="${league_teams}" var="team">
												<form:option value="${team.team_id}">${team.team_name}</form:option>
											</c:forEach>
										</form:select>
										<span class="help-block align-center">Alege echipa
											gazda</span>
									</div>
								</div>

								<div class="col-md-3">
									<div class="form-group">
										<form:select path="away_team.team_id" class="selectpicker"
											data-live-search="true" placeholder="Echipa oaspete">
											<c:forEach items="${league_teams}" var="team">
												<form:option value="${team.team_id}">${team.team_name}</form:option>
											</c:forEach>
										</form:select>
										<span class="help-block align-center">Alege echipa
											oaspete</span>
									</div>
								</div>

								<div class="col-md-3">
									<div class="control-group">
										<div class="controls input-append date form_datetime"
											data-date="2015-09-16T05:25:07Z"
											data-date-format="yyyy-MM-dd" data-link-field="dtp_input1">
											<form:input size="16" type="text" value="" path="match_time" />
											<span class="add-on"><i class="icon-remove"
												style="visibility: hidden;"></i></span> <span class="add-on"><i
												style="visibility: hidden;" class="icon-th"></i></span>
										</div>
										<form:input type="hidden" id="dtp_input1" value=""
											path="match_time_string" />
										<br />
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<form:select path="match_hour" class="selectpicker"
											data-live-search="true" placeholder="Ora meciului">
											<c:forEach items="${hours_list}" var="hour">
												<form:option value="${hour}">${hour}</form:option>
											</c:forEach>
										</form:select>
										<span class="help-block align-center">Alege ora</span>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-1"></div>
								<div class="col-md-10">
									<ul class="list-inline center">
										<c:forEach items="${bet_types}" var="type" varStatus="local">
											<li><span class="help-block align-center"><strong>${type.bet_name}</strong></span>
											<form:input type="text"
													path="matchBetList[${local.index}].odd"
													placeholder="${type.bet_name}" /></li>
											<form:input type="hidden" value="${type.bet_name}"
												path="matchBetList[${local.index}].bet_name" />
											<c:set var="last_category" value="${type.bet_category}"></c:set>
										</c:forEach>
									</ul>
								</div>
								<div class="col-md-1"></div>
							</div>
							<div class="row">
								<div class="pull-right">
									<div class="col-md-2">
										<div class="form-group">
											<label class="pull-right"> <form:button
													class="btn btn-success button-next" type="submit">Salveaza</form:button>
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
	</div>

</div>
<script type="text/javascript">
	$('.form_datetime').datetimepicker({
		//language:  'fr',
		weekStart : 1,
		todayBtn : 1,
		autoclose : 1,
		todayHighlight : 1,
		startView : 2,
		forceParse : 0,
		showMeridian : 1
	});
</script>