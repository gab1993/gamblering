<%@ include file="../lib/taglib.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<style>
.spacer {
	margin-top: 40px; /* define margin as you see fit */
}
.top-buffer {
	margin-top: 20px;
}

.winner-status {
	background: #00CC00;
	color: white;
}

.looser-status {
	background: #A30000;
	color: white;
}

.not-played-status {
	background: #CACABC;
	color: white;
}
</style>
<body>
	<div class="page-header">
		<div class="page-title">
			<h3>Profilul lui ${tipster_profile.username}</h3>
		</div>

		<!-- Page Stats -->
		<ul class="page-stats">
			<li>
				<div class="summary">
					<span>Bilete castigatoare</span>
					<h3>${status}</h3>
				</div>
			</li>
			<li>
				<div class="summary">
					<span>Banca</span>
					<h3>${tipster_bankroll}LEI</h3>
				</div>
			</li>
		</ul>
		<!-- /Page Stats -->
	</div>
	<!-- /Page Header -->

	<!--=== Page Content ===-->
	<!--=== Inline Tabs ===-->
	<div class="row">
		<div class="col-md-12">
			<!-- Tabs-->
			<div class="tabbable tabbable-custom tabbable-full-width">
				<c:if test="${follow eq true}">
					<div class="alert alert-warning">Urmariti deja aceasta
						persoana.</div>
				</c:if>
				<c:if test="${follow eq false}">
					<div class="alert alert-warning">Nu urmariti aceasta
						persoana.</div>
				</c:if>
				<ul class="nav nav-tabs">
					<c:if test="${auth_user_username != tipster_profile.username}">
						<c:if test="${isfollowed eq 0 }">
							<li class="disabled"><form:form method="POST"
									modelAttribute="follower"
									action="http://localhost:1923/gamblering-social-network/follow_profile.html"
									enctype="application/x-www-form-urlencoded">
									<form:input type="hidden" path="followed_username"
										value="${tipster_profile.username}" />
									<button type="submit" class="btn btn-primary nav nav-tabs ">Urmareste</button>
								</form:form></li>
						</c:if>
						<c:if test="${isfollowed eq 1 }">
							<li class="disabled"><form:form method="POST"
									modelAttribute="follower"
									action="http://localhost:1923/gamblering-social-network/unfollow_profile.html"
									enctype="application/x-www-form-urlencoded">
									<form:input type="hidden" path="followed_username"
										value="${tipster_profile.username}" />
									<button type="submit" class="btn btn-danger nav nav-tabs ">Nu
										urmari</button>
								</form:form></li>
						</c:if>
					</c:if>
					<li class="active"><a href="#tab_overview" data-toggle="tab">Profil</a></li>
				</ul>
				<div class="tab-content row">
					<!--=== Overview ===-->
					<div class="tab-pane active" id="tab_overview">
						<div class="col-md-3">
							<div class="list-group">
								<li class="list-group-item no-padding"><img
									class="img-responsive"
									src="<%=request.getContextPath()%>/resources/assets/img/demo/avatar-large.jpg"
									alt=""></li> <a href="javascript:void(0);"
									class="list-group-item"><span>${tipster_role.role}</span> </a>
								<a href="javascript:void(0);" class="list-group-item">Bilete</a>
								<a href="javascript:void(0);" class="list-group-item">Mesaje</a>
								<a href="javascript:void(0);" class="list-group-item"><span
									class="badge">${followers_no}</span>Urmaritori</a> <a
									href="javascript:void(0);" class="list-group-item">Setari</a>
							</div>
						</div>

						<div class="col-md-9">
							<div class="row profile-info">
								<div class="col-md-7">
									<div class="widget">
										<div class="widget-header">
											<h4>
												<i class="icon-user"></i>Informatii generale
											</h4>
										</div>
										<div class="widget-content">
											<dl class="dl-horizontal">
												<dt>Nume:</dt>
												<dd>&nbsp;${tipster_profile.first_name}</dd>
											</dl>

											<dl class="dl-horizontal">
												<dt>Prenume:</dt>
												<dd>&nbsp;${tipster_profile.second_name}</dd>
											</dl>

											<dl class="dl-horizontal">
												<dt>Sex:</dt>
												<dd>&nbsp;${tipster_profile.gender}</dd>
											</dl>

											<dl class="dl-horizontal">
												<dt>Data nastere:</dt>
												<dd>&nbsp;${tipster_profile.birthdate}</dd>
											</dl>

											<dl class="dl-horizontal">
												<dt>Tara:</dt>
												<dd>&nbsp;${tipster_profile.country}</dd>
											</dl>

											<dl class="dl-horizontal">
												<dt>Oras:</dt>
												<dd>&nbsp;${tipster_profile.city}</dd>
											</dl>
											<dl class="dl-horizontal">
												<dt>Betagency:</dt>
												<dd>&nbsp;${tipster_profile.betagency}</dd>
											</dl>
										</div>
									</div>
								</div>
								<div class="col-md-5">
									<div class="widget">
										<div class="widget-header">
											<h4>
												<i class="icon-list-alt"></i> Ultimul bilet
											</h4>
										</div>
										<div class="widget-content">
											<table class="table table-responsive">
												<thead>
													<tr>
														<td class="ticket-header">Meci</td>
														<td class="ticket-header">Pariu</td>
														<td class="ticket-header">Cota</td>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${last_ticket.matchBets}" var="bet">
														<tr>
															<td class="ticket-content">${bet.match.home_team.team_name}-
																${bet.match.away_team.team_name}</td>
															<td class="ticket-content">${bet.bet_name}</td>
															<td class="ticket-content">${bet.odd}</td>
														</tr>
													</c:forEach>
												</tbody>
												<tfoot>
													<tr>
														<td class="ticket-footer">Cota: <fmt:formatNumber
																type="number" pattern="#.##"
																value="${last_ticket.total_odd}" />
														</td>
														<td class="ticket-footer">Miza: ${last_ticket.stake}</td>
														<c:set var="castig"
															value="${last_ticket.total_odd * last_ticket.stake}" />
														<td class="ticket-footer">Castig: <fmt:formatNumber
																type="currency" value="${castig}" /></td>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
							<!-- /.row -->
							<div class="row spacer"></div>
							<div class="row">
								<div class="col-md-12">
									<div class="widget">
										<div class="widget-header">Ultimile bilete</div>
										<!--Widget Header-->
										<div class="widget-body">
											<div class="widget-main no-padding">
												<div class="tickets-container">
													<ul class="tickets-list">
														<c:forEach items="${last_five}" var="ticket"
															varStatus="loop">
															<li class="ticket-item">
																<div class="row">
																	<div class="ticket-user col-md-2 col-sm-12">
																		Nevalidat <span class="divider hidden-xs"></span>
																	</div>
																	<div class="ticket-user col-md-2 col-sm-12">
																		<span class="divider hidden-xs"></span> Cota totala:
																		<fmt:formatNumber type="number" pattern="#.##"
																			value="${ticket.total_odd}" />
																	</div>
																	<div class="ticket-time  col-md-4 col-sm-6 col-xs-12">
																		<div class="divider hidden-md hidden-sm hidden-xs"></div>
																		<i class="fa fa-clock-o"></i> <span class="time">
																			${ticket.betTime} </span>
																	</div>
																	<div class="ticket-type  col-md-2 col-sm-3 col-xs-12">
																		<span class="divider hidden-xs"></span> <span
																			class="type"><button type="button"
																				class="btn btn-info" data-toggle="modal"
																				data-target="#${loop.index}">Vezi biletul</button></span>
																	</div>
																	<div class="ticket-type  col-md-2 col-sm-3 col-xs-12">
																		<span class="divider hidden-xs"></span> <span
																			class="type"><button type="button"
																				class="btn btn-success" data-toggle="modal"
																				data-target="#${loop.index}">Pariaza-l</button></span>
																	</div>
																	<div id="${loop.index}" class="modal fade"
																		role="dialog">
																		<div class="modal-dialog">

																			<!-- Modal content-->
																			<div class="modal-content">
																				<div class="modal-header">
																					<button type="button" class="close"
																						data-dismiss="modal">&times;</button>
																					<h4 class="modal-title">Bilet ${ticket.betTime}</h4>
																				</div>
																				<table class="table table-bordered">
																					<thead>
																						<tr>
																							<th class="ticket-header">Meci</th>
																							<th class="ticket-header">Data</th>
																							<th class="ticket-header">Pronostic</th>
																							<th class="ticket-header">Cota</th>
																							<th class="ticket-header">Stare</th>
																						</tr>
																					</thead>
																					<tbody class="searchable">
																						<div class="modal-body">
																							<c:forEach items="${ticket.matchBets}" var="bet">
																						${match.bet_name}		
																						<tr>
																									<td class="ticket-content">${bet.match.home_team.team_name}-
																										${bet.match.away_team.team_name}</td>
																									<td class="ticket-content">${bet.match.match_time}</td>
																									<td class="ticket-content">${bet.bet_name}</td>
																									<td class="ticket-content">${bet.odd}</td>

																									<c:choose>
																										<c:when test="${bet.winner == 1}">
																											<td class="ticket-content winner-status"><strong>Castigat</strong>
																										</c:when>
																										<c:when test="${bet.winner == 0 }">
																											<td class="ticket-content looser-status"><strong>Pierdut</strong>
																											</td>
																										</c:when>
																										<c:otherwise>
																											<td class="ticket-content not-played-status"><strong>Neinceput</strong>
																											</td>
																										</c:otherwise>
																									</c:choose>

																								</tr>
																							</c:forEach>
																						</div>
																					</tbody>
																				</table>
																				<div class="modal-footer">
																					<button type="button" class="btn btn-default"
																						data-dismiss="modal">Inchide</button>
																				</div>
																			</div>

																		</div>
																	</div>
																</div>
															</li>
														</c:forEach>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- /Striped Table -->
							</div>
							<!-- /.row -->
						</div>
						<!-- /.col-md-9 -->
					</div>
				</div>
				<!-- /.tab-content -->
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			$('#datetimepicker1').datetimepicker();
		});
	</script>
</body>