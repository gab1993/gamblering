<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<%@ include file="../lib/taglib.jsp"%>

<style>
.top-buffer {
	margin-top: 20px;
}
</style>
<c:if test="${success eq true}">
	<div class="alert alert-success">Ati adaugat o noua liga.</div>
</c:if>
<div class="col-md-8">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<div class="widget box">
				<div class="widget-header">
					<h4>
						<i class="icon-reorder"></i> Adauga liga
					</h4>
				</div>
				<div class="widget-content">
					<form:form id="addleagues" modelAttribute="league">
						<table class="table table-striped table-bordered table-condensed"
							id="tab_logic">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Nume liga</th>
									<th class="text-center">Tara</th>
								</tr>
							</thead>
							<tbody>
								<tr id="addr0">
									<td>1</td>
									<td><form:input path="league_name" type="text"
											placeholder="Nume liga" class="form-control" /></td>
									<td><form:input path="league_country" type="text"
											placeholder="Tara" class="form-control" /></td>
								</tr>
								<tr id="addr1"></tr>
							</tbody>
							<tfoot>
								<tr>
									<td>
										<div class="btn-toolbar btn-toolbar-demo">
											<form:button id="submit" type="submit"
												class="btn btn-sm btn-success">Salveaza</form:button>
										</div>
									</td>
								</tr>
							</tfoot>
						</table>
					</form:form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-4">
	<div class="widget box">
		<div class="widget-header">
			<h4>Ligi curente</h4>
		</div>
		<ul class="list-group">
			<c:forEach items="${admin_leagues}" var="league">

				<li class="list-group-item text-left"><a
					href="<spring:url value="/liga/${league.league_id}.html"/>"> <label
						class="name"> ${league.league_country}
							(${league.league_name}) </label>
				</a>
					<div class="break"></div></li>
			</c:forEach>
		</ul>
	</div>
</div>