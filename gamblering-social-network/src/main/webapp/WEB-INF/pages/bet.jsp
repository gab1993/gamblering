<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../lib/taglib.jsp"%>

<div class="col-md-8">
	<c:if test="${meci_existent eq true }">
		<div class="alert alert-danger">Meciul exista deja pe bilet.</div>
	</c:if>
	<c:if test="${pariu_existent eq true }">
		<div class="alert alert-danger">Pariul exista pe bilet si a fost
			scos.</div>
	</c:if>
	<div class="alert alert-success">Pentru a adauga un meci pe bilet,
		apasati click dreapta pe cota pronosticului ales.</div>
	<div class="widget box">
		<div class="widget-header">
			<h4>Meciuri</h4>
		</div>
		<br>
		<c:forEach items="${league_matches}" var="match">
			<div class="table-responsive">
				<table class="table table-bordered ">
					<thead>
						<tr>
							<td>${match.home_team.team_name}-${match.away_team.team_name }</td>
							<td>${match.match_time}${match.match_hour }</td>
							<c:forEach items="${match.matchBets}" var="bet1">
								<c:if
									test="${bet1.bet_type.bet_category == 'Victorie' ||  bet1.bet_type.bet_category == 'Sansa dubla'}">
									<td>
										<button class="btn-clean">${bet1.bet_name}</button>
									</td>
								</c:if>
							</c:forEach>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="2"></td>
							<c:forEach items="${match.matchBets}" var="bet1">
								<c:if
									test="${bet1.bet_type.bet_category == 'Victorie' ||  bet1.bet_type.bet_category == 'Sansa dubla'}">
									<td><form:form modelAttribute="matchBet" method="post">
											<form:input type="hidden" path="bet_name"
												value="${bet1.bet_name}" />
											<form:input type="hidden" path="bet_type.bet_name"
												value="${bet1.bet_type.bet_name}" />
											<form:input type="hidden" path="bet_type.bet_category"
												value="${bet1.bet_type.bet_category}" />
											<form:input type="hidden" path="match" value="${bet1.match}" />
											<form:input type="hidden" path="match.home_team.team_name"
												value="${bet1.match.home_team.team_name}" />
											<form:input type="hidden" path="match.away_team.team_name"
												value="${bet1.match.away_team.team_name}" />
											<form:input type="hidden" path="match_id"
												value="${bet1.match_id}" />
											<form:input type="hidden" path="odd" value="${bet1.odd}" />
											<button class="btn-clean" type="submit">
												<strong><fmt:formatNumber type="number"
														value="${bet1.odd}" pattern="#.00">
													</fmt:formatNumber> </strong>
											</button>
										</form:form></td>

								</c:if>
							</c:forEach>
						</tr>
						<tr>
							<td colspan="9">
								<div class="padded">
									<a class="clickable" data-toggle="collapse" id="repriza-1"
										data-target=".repriza1${match.match_id}"> <span
										class="label label-success">Prima repriza</span>
									</a>
								</div>
								<div class="padded">
									<a class="clickable" data-toggle="collapse" id="repriza-2"
										data-target=".repriza1${match.match_id}"> <span
										class="label label-success">A doua repriza</span></a>
								</div>
								<div class="padded">
									<a class="clickable" data-toggle="collapse" id="total-goluri"
										data-target=".pauza-sau-final${match.match_id}"> <span
										class="label label-success">Pauza sau final</span>
									</a>
								</div>
								<div class="padded">
									<a class="clickable" data-toggle="collapse"
										id="pauza-final${match.match_id}"
										data-target=".pauza-final${match.match_id}"><span
										class="label label-success">Pauza final</span> </a>
								</div>
								<div class="padded">
									<a class="clickable" data-toggle="collapse"
										data-target=".ambele-marcheaza${match.match_id}"> <span
										class="label label-success">Ambele marcheaza</span>
									</a>
								</div>
								<div class="padded">
									<a class="clickable" data-toggle="collapse" id="total-goluri"
										data-target=".gg-total-goluri${match.match_id}"> <span
										class="label label-success">Ambele marcheaza si total
											goluri</span>
									</a>
								</div>
								<div class="padded">
									<a class="clickable" data-toggle="collapse" id="total-goluri"
										data-target=".total-goluri${match.match_id}"> <span
										class="label label-success">Total goluri</span>
									</a>
								</div>
								<div class="padded">
									<a class="clickable" data-toggle="collapse" id="total-goluri"
										data-target=".interval-goluri${match.match_id}"> <span
										class="label label-success">Interval goluri</span>
									</a>
								</div>
							</td>

						</tr>
						<tr class="collapse pauza-final${match.match_id}">
							<td colspan="9">
								<table
									class="table table-responsive table-condensed text-center">
									<thead>
										<tr>
											<td class="text-left">Pauza final</td>

											<c:forEach items="${match.matchBets}" var="bet">
												<c:if test="${bet.bet_type.bet_category eq  'Pauza final'}">
													<td class="text-center">${bet.bet_name}</td>
												</c:if>
											</c:forEach>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="text-left">Cota</td>
											<c:forEach items="${match.matchBets}" var="bet">
												<c:if test="${bet.bet_type.bet_category eq  'Pauza final'}">
													<td class="text-center"><form:form
															modelAttribute="matchBet" method="post">
															<form:input type="hidden" path="bet_name"
																value="${bet.bet_name}" />
															<form:input type="hidden" path="bet_type.bet_name"
																value="${bet.bet_type.bet_name}" />
															<form:input type="hidden" path="bet_type.bet_category"
																value="${bet.bet_type.bet_category}" />
															<form:input type="hidden" path="match"
																value="${bet.match}" />
															<form:input type="hidden"
																path="match.home_team.team_name"
																value="${bet.match.home_team.team_name}" />
															<form:input type="hidden"
																path="match.away_team.team_name"
																value="${bet.match.away_team.team_name}" />
															<form:input type="hidden" path="match_id"
																value="${bet.match_id}" />
															<form:input type="hidden" path="odd" value="${bet.odd}" />
															<button class="btn-clean" type="submit">
																<fmt:formatNumber type="number" value="${bet.odd}"
																	pattern="#.00">
																</fmt:formatNumber>
															</button>
														</form:form></td>
												</c:if>
											</c:forEach>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr class="collapse ambele-marcheaza${match.match_id}">
							<td colspan="9">
								<table
									class="table table-responsive table-condensed text-center">
									<thead>
										<tr>
											<td class="text-left">Ambele marcheaza</td>
											<c:forEach items="${match.matchBets}" var="bet">
												<c:if
													test="${bet.bet_type.bet_category eq  'Ambele marcheaza'}">
													<td class="text-center">${bet.bet_name}</td>
												</c:if>
											</c:forEach>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="text-left">Cota</td>
											<c:forEach items="${match.matchBets}" var="bet">
												<c:if
													test="${bet.bet_type.bet_category eq  'Ambele marcheaza'}">
													<td class="text-center"><form:form
															modelAttribute="matchBet" method="post">
															<form:input type="hidden" path="bet_name"
																value="${bet.bet_name}" />
															<form:input type="hidden" path="bet_type.bet_name"
																value="${bet.bet_type.bet_name}" />
															<form:input type="hidden" path="bet_type.bet_category"
																value="${bet.bet_type.bet_category}" />
															<form:input type="hidden" path="match"
																value="${bet.match}" />
															<form:input type="hidden"
																path="match.home_team.team_name"
																value="${bet.match.home_team.team_name}" />
															<form:input type="hidden"
																path="match.away_team.team_name"
																value="${bet.match.away_team.team_name}" />
															<form:input type="hidden" path="match_id"
																value="${bet.match_id}" />
															<form:input type="hidden" path="odd" value="${bet.odd}" />
															<button class="btn-clean" type="submit">
																<fmt:formatNumber type="number" value="${bet.odd}"
																	pattern="#.00">
																</fmt:formatNumber>
															</button>
														</form:form></td>
												</c:if>
											</c:forEach>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr class="collapse repriza1${match.match_id}">
							<td colspan="9">
								<table class="table table-responsive table-bordered text-center">
									<thead>
										<tr>
											<td class="text-left">Repriza 1</td>

											<c:forEach items="${match.matchBets}" var="bet">
												<c:if
													test="${bet.bet_type.bet_category eq  'Prima repriza'}">
													<td class="text-center">${bet.bet_name}</td>
												</c:if>
											</c:forEach>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="text-left">Cota</td>
											<c:forEach items="${match.matchBets}" var="bet">
												<c:if
													test="${bet.bet_type.bet_category eq  'Prima repriza'}">
													<td class="text-center"><form:form
															modelAttribute="matchBet" method="post">
															<form:input type="hidden" path="bet_name"
																value="${bet.bet_name}" />
															<form:input type="hidden" path="bet_type.bet_name"
																value="${bet.bet_type.bet_name}" />
															<form:input type="hidden" path="bet_type.bet_category"
																value="${bet.bet_type.bet_category}" />
															<form:input type="hidden" path="match"
																value="${bet.match}" />
															<form:input type="hidden"
																path="match.home_team.team_name"
																value="${bet.match.home_team.team_name}" />
															<form:input type="hidden"
																path="match.away_team.team_name"
																value="${bet.match.away_team.team_name}" />
															<form:input type="hidden" path="match_id"
																value="${bet.match_id}" />
															<form:input type="hidden" path="odd" value="${bet.odd}" />
															<button class="btn-clean" type="submit">
																<strong><fmt:formatNumber type="number"
																		value="${bet.odd}" pattern="#.00">
																	</fmt:formatNumber></strong>
															</button>
														</form:form></td>
												</c:if>
											</c:forEach>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr class="collapse repriza2${match.match_id}">
							<th colspan="9">
								<table
									class="table table-responsive table-condensed text-center">
									<thead>
										<tr>
											<th class="text-left">Repriza 2</th>

											<c:forEach items="${match.matchBets}" var="bet">
												<c:if test="${bet.bet_type.bet_category eq  'Repriza doi'}">
													<th class="text-center">${bet.bet_name}</th>
												</c:if>
											</c:forEach>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="text-left">Cota</td>
											<c:forEach items="${match.matchBets}" var="bet">
												<c:if test="${bet.bet_type.bet_category eq  'Repriza doi'}">
													<td class="text-center"><form:form
															modelAttribute="matchBet" method="post">
															<form:input type="hidden" path="bet_name"
																value="${bet.bet_name}" />
															<form:input type="hidden" path="bet_type.bet_name"
																value="${bet.bet_type.bet_name}" />
															<form:input type="hidden" path="bet_type.bet_category"
																value="${bet.bet_type.bet_category}" />
															<form:input type="hidden" path="match"
																value="${bet.match}" />
															<form:input type="hidden"
																path="match.home_team.team_name"
																value="${bet.match.home_team.team_name}" />
															<form:input type="hidden"
																path="match.away_team.team_name"
																value="${bet.match.away_team.team_name}" />
															<form:input type="hidden" path="match_id"
																value="${bet.match_id}" />
															<form:input type="hidden" path="odd" value="${bet.odd}" />
															<button class="btn-clean" type="submit">
																<fmt:formatNumber type="number" value="${bet.odd}"
																	pattern="#.00">
																</fmt:formatNumber>
															</button>
														</form:form></td>
												</c:if>
											</c:forEach>
										</tr>
									</tbody>
								</table>
							</th>
						</tr>
						<tr class="collapse total-goluri${match.match_id}">
							<th colspan="9">
								<table
									class="table table-responsive table-condensed text-center">
									<thead>
										<tr>
											<th class="text-left">Total goluri</th>

											<c:forEach items="${match.matchBets}" var="bet">
												<c:if test="${bet.bet_type.bet_category eq  'Total goluri'}">
													<th class="text-center">${bet.bet_name}</th>
												</c:if>
											</c:forEach>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="text-left">Cota</td>
											<c:forEach items="${match.matchBets}" var="bet">
												<c:if test="${bet.bet_type.bet_category eq  'Total goluri'}">
													<td class="text-center"><form:form
															modelAttribute="matchBet" method="post">
															<form:input type="hidden" path="bet_name"
																value="${bet.bet_name}" />
															<form:input type="hidden" path="bet_type.bet_name"
																value="${bet.bet_type.bet_name}" />
															<form:input type="hidden" path="bet_type.bet_category"
																value="${bet.bet_type.bet_category}" />
															<form:input type="hidden" path="match"
																value="${bet.match}" />
															<form:input type="hidden"
																path="match.home_team.team_name"
																value="${bet.match.home_team.team_name}" />
															<form:input type="hidden"
																path="match.away_team.team_name"
																value="${bet.match.away_team.team_name}" />
															<form:input type="hidden" path="match_id"
																value="${bet.match_id}" />
															<form:input type="hidden" path="odd" value="${bet.odd}" />
															<button class="btn-clean" type="submit">
																<fmt:formatNumber type="number" value="${bet.odd}"
																	pattern="#.00">
																</fmt:formatNumber>
															</button>
														</form:form></td>
												</c:if>
											</c:forEach>
										</tr>
									</tbody>
								</table>
							</th>
						</tr>
						<tr class="collapse gg-total-goluri${match.match_id}">
							<th colspan="9">
								<table
									class="table table-responsive table-condensed text-center">
									<thead>
										<tr>
											<th class="text-left">Ambele marcheaza si total goluri</th>

											<c:forEach items="${match.matchBets}" var="bet">
												<c:if
													test="${bet.bet_type.bet_category eq  'Ambele marcheaza si total goluri'}">
													<th class="text-center">${bet.bet_name}</th>
												</c:if>
											</c:forEach>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="text-left">Cota</td>
											<c:forEach items="${match.matchBets}" var="bet">
												<c:if
													test="${bet.bet_type.bet_category eq  'Ambele marcheaza si total goluri'}">
													<td class="text-center"><form:form
															modelAttribute="matchBet" method="post">
															<form:input type="hidden" path="bet_name"
																value="${bet.bet_name}" />
															<form:input type="hidden" path="bet_type.bet_name"
																value="${bet.bet_type.bet_name}" />
															<form:input type="hidden" path="bet_type.bet_category"
																value="${bet.bet_type.bet_category}" />
															<form:input type="hidden" path="match"
																value="${bet.match}" />
															<form:input type="hidden"
																path="match.home_team.team_name"
																value="${bet.match.home_team.team_name}" />
															<form:input type="hidden"
																path="match.away_team.team_name"
																value="${bet.match.away_team.team_name}" />
															<form:input type="hidden" path="match_id"
																value="${bet.match_id}" />
															<form:input type="hidden" path="odd" value="${bet.odd}" />
															<button class="btn-clean" type="submit">
																<fmt:formatNumber type="number" value="${bet.odd}"
																	pattern="#.00">
																</fmt:formatNumber>
															</button>
														</form:form></td>
												</c:if>
											</c:forEach>
										</tr>
									</tbody>
								</table>
							</th>
						</tr>
						<tr class="collapse pauza-sau-final${match.match_id}">
							<th colspan="9">
								<table
									class="table table-responsive table-condensed text-center">
									<thead>
										<tr>
											<th class="text-left">Pauza sau final</th>

											<c:forEach items="${match.matchBets}" var="bet">
												<c:if
													test="${bet.bet_type.bet_category eq  'Pauza sau final'}">
													<th class="text-center">${bet.bet_name}</th>
												</c:if>
											</c:forEach>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="text-left">Cota</td>
											<c:forEach items="${match.matchBets}" var="bet">
												<c:if
													test="${bet.bet_type.bet_category eq  'Pauza sau final'}">
													<td class="text-center"><form:form
															modelAttribute="matchBet" method="post">
															<form:input type="hidden" path="bet_name"
																value="${bet.bet_name}" />
															<form:input type="hidden" path="bet_type.bet_name"
																value="${bet.bet_type.bet_name}" />
															<form:input type="hidden" path="bet_type.bet_category"
																value="${bet.bet_type.bet_category}" />
															<form:input type="hidden" path="match"
																value="${bet.match}" />
															<form:input type="hidden"
																path="match.home_team.team_name"
																value="${bet.match.home_team.team_name}" />
															<form:input type="hidden"
																path="match.away_team.team_name"
																value="${bet.match.away_team.team_name}" />
															<form:input type="hidden" path="match_id"
																value="${bet.match_id}" />
															<form:input type="hidden" path="odd" value="${bet.odd}" />
															<button class="btn-clean" type="submit">
																<fmt:formatNumber type="number" value="${bet.odd}"
																	pattern="#.00">
																</fmt:formatNumber>
															</button>
														</form:form></td>
												</c:if>
											</c:forEach>
										</tr>
									</tbody>
								</table>
							</th>
						</tr>
						<tr class="collapse interval-goluri${match.match_id}">
							<th colspan="9">
								<table
									class="table table-responsive table-condensed text-center">
									<thead>
										<tr>
											<th class="text-left">Pauza sau final</th>

											<c:forEach items="${match.matchBets}" var="bet">
												<c:if
													test="${bet.bet_type.bet_category eq  'Interval goluri'}">
													<th class="text-center">${bet.bet_name}</th>
												</c:if>
											</c:forEach>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="text-left">Cota</td>
											<c:forEach items="${match.matchBets}" var="bet">
												<c:if
													test="${bet.bet_type.bet_category eq  'Interval goluri'}">
													<td class="text-center"><form:form
															modelAttribute="matchBet" method="post">
															<form:input type="hidden" path="bet_name"
																value="${bet.bet_name}" />
															<form:input type="hidden" path="bet_type.bet_name"
																value="${bet.bet_type.bet_name}" />
															<form:input type="hidden" path="bet_type.bet_category"
																value="${bet.bet_type.bet_category}" />
															<form:input type="hidden" path="match"
																value="${bet.match}" />
															<form:input type="hidden"
																path="match.home_team.team_name"
																value="${bet.match.home_team.team_name}" />
															<form:input type="hidden"
																path="match.away_team.team_name"
																value="${bet.match.away_team.team_name}" />
															<form:input type="hidden" path="match_id"
																value="${bet.match_id}" />
															<form:input type="hidden" path="odd" value="${bet.odd}" />
															<button class="btn-clean" type="submit">
																<fmt:formatNumber type="number" value="${bet.odd}"
																	pattern="#.00">
																</fmt:formatNumber>
															</button>
														</form:form></td>
												</c:if>
											</c:forEach>
										</tr>
									</tbody>
								</table>
							</th>
						</tr>
					</tbody>
				</table>
			</div>
			<hr class="divider-blue">
		</c:forEach>
	</div>
</div>