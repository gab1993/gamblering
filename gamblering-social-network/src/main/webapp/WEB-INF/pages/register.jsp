<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<title>Pagina inregistrare</title>
<!-- Bootstrap -->
<link
	href="<%=request.getContextPath()%>/resources/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />

<!-- Theme -->
<link href="<%=request.getContextPath()%>/resources/assets/css/main.css"
	rel="stylesheet" type="text/css" />
<link
	href="<%=request.getContextPath()%>/resources/assets/css/plugins.css"
	rel="stylesheet" type="text/css" />
<link
	href="<%=request.getContextPath()%>/resources/assets/css/responsive.css"
	rel="stylesheet" type="text/css" />
<link
	href="<%=request.getContextPath()%>/resources/assets/css/icons.css"
	rel="stylesheet" type="text/css" />

<!-- Login -->
<link
	href="<%=request.getContextPath()%>/resources/assets/css/login.css"
	rel="stylesheet" type="text/css" />

<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/assets/css/fontawesome/font-awesome.min.css">

<!--[if IE 7]>
		<link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
	<![endif]-->

<!--[if IE 8]>
		<link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
	<![endif]-->

<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700'
	rel='stylesheet' type='text/css'>

<!--=== JavaScript ===-->

<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/assets/js/libs/jquery-1.10.2.min.js"></script>

<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/assets/js/libs/lodash.compat.min.js"></script>

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
		<script src="assets/js/libs/html5shiv.js"></script>
	<![endif]-->

<!-- Beautiful Checkboxes -->
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/plugins/uniform/jquery.uniform.min.js"></script>

<!-- Form Validation -->
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/plugins/validation/jquery.validate.min.js"></script>

<!-- Slim Progress Bars -->
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/plugins/nprogress/nprogress.js"></script>

<!-- App -->
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/assets/js/login.js"></script>
<script>
	$(document).ready(function() {
		"use strict";

		Login.init(); // Init login JavaScript
	});
</script>
</head>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>

<body class="login">
	<div class="logo">
		<strong>GAMBLER</strong>ING
	</div>
	<div class="box">
		<div class="content">
			<!-- Register Formular (hidden by default) -->
			<c:if test="${email eq true}">
				<div class="alert alert-warning">Email-ul introdus este
					utilizat.</div>
			</c:if>
			<c:if test="${success eq true}">
				<div class="alert alert-success">V-ati inregistrat cu success</div>
			</c:if>
			<c:if test="${username eq true}">
				<div class="alert alert-warning">Username-ul introdus este
					utilizat.</div>
			</c:if>
			<form:form class="form-vertical register-form" modelAttribute="user"
				method="POST">

				<h3 class="form-title">Inregistreaza-te gratuit</h3>

				<div class="form-group">
					<div class="input-icon">
						<i class="icon-user"></i>
						<form:input type="text" name="username" path="username"
							class="form-control" placeholder="Username" autofocus="autofocus"
							data-rule-required="true" />
						<c:if test="${username eq true}">
							<div class="has-error help-block">Foloseste alt username</div>
						</c:if>
					</div>
				</div>
				<div class="form-group">
					<div class="input-icon">
						<i class="icon-lock"></i>
						<form:input type="password" name="password" path="password"
							class="form-control" placeholder="Password"
							id="register_password" data-rule-required="true" />
					</div>
				</div>
				<div class="form-group">
					<div class="input-icon">
						<i class="icon-ok"></i>
						<form:input type="password" name="password_confirm"
							path="match_password" class="form-control"
							placeholder="Confirm Password" data-rule-required="true"
							data-rule-equalTo="#register_password" />
					</div>
				</div>
				<div class="form-group">
					<div class="input-icon">
						<i class="icon-envelope"></i>
						<form:input type="text" name="Email" path="email"
							class="form-control" placeholder="Email address"
							data-rule-required="true" data-rule-email="true" />
					</div>
					<c:if test="${email eq true}">
						<div class="has-error help-block">Foloseste alt email</div>
					</c:if>
				</div>
				<div class="form-group spacing-top">
					<label class="checkbox"><input type="checkbox"
						class="uniform" name="remember" data-rule-required="true"
						data-msg-required="Please accept ToS first."> Trebuie sa
						accepti <a href="javascript:void(0);">termenii si conditiile </a></label>
					<label for="remember" class="has-error help-block"
						style="display: none;"></label>
				</div>
				<!-- /Input Fields -->

				<!-- Form Actions -->
				<div class="form-actions">
					<a href="<spring:url value='/login.html'/>">
						<button type="button" class="btn btn-default pull-left">
							<i class="icon-angle-left"></i> Login
						</button>
					</a>
					<button type="submit" class="submit btn btn-success pull-right">
						Inregistreaza-ma <i class="icon-angle-right"></i>
					</button>
				</div>
			</form:form>
			<!-- /Register Formular -->

		</div>
	</div>
	<div class="footer">
		<a href="<spring:url value='/inregistrare.html'/>">Nu ai un cont
			pe Gamblering? <strong>inregistreaza-te acum!</strong>
		</a>
		<p>Drepturi de autor Tifui Gabriel</p>
	</div>
</body>