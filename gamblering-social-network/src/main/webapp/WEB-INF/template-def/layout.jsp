<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../lib/taglib.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<head>
<style>
.btn-circle {
	width: 30px;
	height: 30px;
	text-align: center;
	padding: 6px 0;
	font-size: 12px;
	line-height: 1.428571429;
	border-radius: 15px;
}
</style>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<title><tiles:getAsString name="title" /></title>

<link
	href="<%=request.getContextPath()%>/resources/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/resources/assets/css/main.css"
	rel="stylesheet" type="text/css" />
<link
	href="<%=request.getContextPath()%>/resources/assets/css/plugins.css"
	rel="stylesheet" type="text/css" />
<link
	href="<%=request.getContextPath()%>/resources/assets/css/responsive.css"
	rel="stylesheet" type="text/css" />
<link
	href="<%=request.getContextPath()%>/resources/assets/css/icons.css"
	rel="stylesheet" type="text/css" />

<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/assets/css/fontawesome/font-awesome.min.css">
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700'
	rel='stylesheet' type='text/css'>

<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/assets/js/libs/jquery-1.10.2.min.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/assets/js/libs/lodash.compat.min.js"></script>

<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/plugins/event.swipe/jquery.event.move.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/plugins/event.swipe/jquery.event.swipe.js"></script>

<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/assets/js/libs/breakpoints.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/plugins/respond/respond.min.js"></script>
<!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/plugins/cookie/jquery.cookie.min.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/assets/js/app.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/assets/js/plugins.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/assets/js/plugins.form-components.js"></script>

<tiles:insertAttribute name="head" />

<script>
	$(document).ready(function() {
		"use strict";

		App.init(); // Init layout and core plugins
		Plugins.init(); // Init all plugins
		FormComponents.init(); // Init all form-specific plugins
	});
</script>
<security:authorize access="isAuthenticated()">
	<!-- Sidebar Toggler -->
	<script>
		$(document).ready(function() {
			$(".sidebar-btn").first().button("toggle");
		});
	</script>
</security:authorize>
<c:set var="s_updated" scope="application" value="${updated}" />
<c:if test="${s_updated eq 0}">
</c:if>
</head>
<body>

	<!-- Header -->
	<header class="header navbar navbar-fixed-top" role="banner">
		<!-- Top Navigation Bar -->
		<div class="container">
			<c:if test="${no_funds eq true }">
				<div class="alert alert-success">Bankroll-ul este prea mic.</div>
			</c:if>
			<!-- Only visible on smartphones, menu toggle -->
			<ul class="nav navbar-nav">
				<li class="nav-toggle"><a href="javascript:void(0);" title=""><i
						class="icon-reorder"></i></a></li>
			</ul>
			<!-- Logo -->
			<a class="navbar-brand" href="<spring:url value="/index.html"/>"><strong>GAMBLE</strong>RING
			</a>
			<!-- /logo -->
			<security:authorize access="isAuthenticated()">
				<a id="sidebar-btn" href="#" class="toggle-sidebar bs-tooltip"
					data-placement="bottom" data-original-title="Meniu"> <i
					class="icon-reorder"></i>
				</a>
			</security:authorize>
			<!-- Top Left Menu -->
			<ul class="nav navbar-nav navbar-left">
				<li class="dropdown"><a href="#"
					class="project-switcher-btn dropdown-toggle"> <i
						class="icon-globe"></i> <span>Pariuri</span>
				</a></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown"> Blog <i class="icon-caret-down small"></i>
				</a>
					<ul class="dropdown-menu">
						<li><a href="#"><i class="icon-user"></i> Pariuri </a></li>
						<li><a href="#"><i class="icon-calendar"></i> Jocuri de
								noroc </a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-tasks"></i> Sport </a></li>
					</ul></li>
			</ul>
			<!-- /Top Left Menu -->
			<!-- Top Right Menu -->
			<ul class="nav navbar-nav navbar-right">
				<!-- User Login Dropdown -->
				<security:authorize access="isAuthenticated()">
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown"> <i class="icon-warning-sign"></i> <c:if
								test="${notif_no > 0}">
								<span class="badge">${notif_no}</span>
							</c:if>
					</a>
						<ul class="dropdown-menu extended notification">
							<li class="title">
								<p>Ai ${notif_no} notificari noi</p>
							</li>
							<c:forEach items="${tipster_notifications}" var="notification">
								<li><a
									href="<spring:url value="/profil/${fn:substringBefore(notification.message, ' ')}.html"/>">
										<span class="label label-success"><i class="icon-plus"></i></span>
										<span class="message">${notification.message}.</span> <span
										class="time"><br>${fn:substringBefore(notification.date, ' ')}</span>
								</a></li>
							</c:forEach>
							<li class="footer"><a href="javascript:void(0);">Vezi
									toate notificarile</a></li>
						</ul></li>
				</security:authorize>
				<security:authorize access="isAuthenticated()">
					<li class="dropdown user"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown"> <i class="icon-male"></i> <span
							class="username">${auth_user_username}</span> <i
							class="icon-caret-down small"></i>
					</a>
						<ul class="dropdown-menu">
							<li><div class="status" align="center">${auth_user_role}
									<br> <i class="icomoon icon-coin"><strong>Bankroll:
									</strong>${bankroll} LEI</i>
								</div></li>
							<li class="divider"></li>
							<li><a href="<spring:url value="/profilul-meu.html"/>"><i
									class="icon-user"></i>Profilul meu</a></li>
							<li class="divider"></li>
							<li><a href="<spring:url value='/logout'/>"><i
									class="icon-key"></i> Log Out</a></li>
						</ul></li>
				</security:authorize>
				<security:authorize access="!isAuthenticated()">
					<li><a href="<spring:url value="/login.html"/>">Login</a></li>
					<li><a href="<spring:url value="/inregistrare.html"/>">Inregistrare</a>
					</li>
				</security:authorize>
				<!-- /user login dropdown -->
			</ul>
			<!-- /Top Right Menu -->
		</div>
		<!-- /top navigation bar -->
		<!--=== Project Switcher ===-->
		<div id="project-switcher" class="container project-switcher">
			<div id="scrollbar">
				<div class="handle"></div>
			</div>

			<div id="frame">
				<ul class="project-list">
					<li><a href="<spring:url value="/pronosticuri.html"/>"> <span
							class="image"><i class="icon-male"></i></span> <span
							class="title">Pronosticuri</span>
					</a></li>
					<li><a href="<spring:url value='/pronosticuri/biletul-zilei/1.html'/>"> <span class="image"><i
								class="icon-tasks"></i></span> <span class="title">Biletul zilei</span>
					</a></li>
					<li><a
						href="<spring:url value="/pronosticuri/clasamente.html"/>"> <span
							class="image"><i class="icon-play"></i></span> <span
							class="title">Clasamente</span>
					</a></li>
					<li><a href="javascript:void(0);"> <span class="image"><i
								class="icon-play"></i></span> <span class="title">Joc Cultura
								generala</span>
					</a></li>
				</ul>
			</div>
			<!-- /#frame -->
		</div>
		<!-- /#project-switcher -->
	</header>
	<!-- /.header -->

	<div id="container">
		<div id="sidebar" class="sidebar-fixed">
			<security:authorize access="isAuthenticated()">
				<div id="sidebar-content">
					<!-- De aici -->

					<!--=== Navigation ===-->

					<ul id="nav">
						<li>
							<div class="sidebar-title">
								<span><strong><center>Pariuri</center> </strong> </span>
							</div>
						</li>
						<li><a href="javascript:void(0);"> <i
								class="icon-desktop"></i> Campionate Europa
						</a>
							<ul class="sub-menu">
								<c:forEach items="${countries}" var="country">
									<li><a
										href="<spring:url value="/pariuri/tara/${country}.html"/>">

											<i class="icon-angle-right"></i> ${country}
									</a></li>
								</c:forEach>
							</ul></li>
						<li><a href="javascript:void(0);"> <i
								class="icon-desktop"></i> Competitii Europene
						</a>
							<ul class="sub-menu">
								<li><a href="ui_general.html"> <i
										class="icon-angle-right"></i> Champions League
								</a></li>
								<li><a href="ui_general.html"> <i
										class="icon-angle-right"></i> Europa League
								</a></li>
							</ul></li>
						<li><a href="charts.html"> <i class="icon-bar-chart"></i>
								Tennis
						</a></li>
						<li><a href="charts.html"> <i class="icon-bar-chart"></i>
								Baschet
						</a></li>
					</ul>

					<!-- /Navigation -->
					<c:choose>
						<c:when test="${no_ticket eq true}">
						</c:when>
						<c:otherwise>
							<div class="sidebar-title">

								<span><strong><center>Bilet virtual</center></strong></span>
							</div>
							<ul id="nav">
								<li>
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th class="ticket-header">Meci</th>
													<th class="ticket-header">Pont</th>
													<th class="ticket-header">Cota</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${bets}" var="bet">
													<form id="match"
														action="${pageContext.request.contextPath}/sterge-meciul.html"
														class="form-horizontal">
														<input name="home_team" type="hidden"
															value="${bet.match.home_team.team_name }" /> <input
															name="away_team" type="hidden"
															value="${bet.match.away_team.team_name }" />
														<tr>
															<td class="ticket-content"><span
																style="cursor: no-drop">
																	<button class="glyphicon glyphicon-minus btn-danger"
																		type="submit"></button>
																	${fn:substringBefore(bet.match.home_team.team_name, " ")}
																	- ${fn:substringBefore(bet.match.away_team.team_name, " ")}
															</span></td>
															<td class="ticket-content"><span
																style="cursor: no-drop">${bet.bet_name}</span></td>
															<td class="ticket-content"><span
																style="cursor: no-drop">${bet.odd}</span></td>
														</tr>
													</form>

												</c:forEach>
											</tbody>
											<tfoot>
												<form id="stake"
													action="${pageContext.request.contextPath}/salveaza-biletul.html"
													class="form-horizontal">
													<tr>
														<td class="status">Cota totala</td>
														<td colspan="2" class="status"><fmt:formatNumber
																type="number" value="${odd}" pattern="#.00" /></td>
													</tr>
													<tr>

														<td colspan="2" class="status">
															<div class="checkbox">
																<label class="checkbox"> <input type="checkbox"
																	id="dayticket" name="dayticket">
																	Biletul zilei
																</label>
																
															</div>
														</td>
														<td class="status"></td>
													</tr>
													<tr>
														<td class="ticket-footer"><div class="form group">
																<input class="form-control" placeholder="Miza: 10 lei"
																	name="stake" type="number" min="10" step="10"
																	max="${bankroll}" style="background-color: #EAECE8;" />
															</div></td>
														<td colspan="2" class="ticket-footer">
															<button class="btn btn-success" type="submit">
																Pariati</button>
														</td>
													</tr>
												</form>
											</tfoot>

										</table>
									</div>
								</li>
							</ul>
						</c:otherwise>
					</c:choose>
				</div>
				<div id="divider" class="resizeable"></div>

			</security:authorize>
			<security:authorize access="!isAuthenticated()">
				<div id="sidebar-content">
					<ul id="nav">
						<li>
							<div class="alert alert-success">Access privat,
								inregistrati-va!</div>
						</li>
					</ul>
				</div>
				<div id="divider"></div>

			</security:authorize>
		</div>
		<!-- /Sidebar -->
		<div id="content">
			<div class="container">
				<!-- Breadcrumbs line -->
				<security:authorize access="isAuthenticated()">
					<div class="crumbs">
						<ul class="crumb-buttons">
							<security:authorize access="hasRole('USER')">
								<li><a href="<spring:url value="/tipstari-urmariti.html"/>"><i
										class="icon-signal"></i><span>Tipsteri urmariti</span></a></li>
							</security:authorize>
							<security:authorize access="hasRole('EDITOR')">
								<li><a href="<spring:url value="/blog/posteaza.html"/>"><i
										class="icon-signal"></i><span>Posteaza</span></a></li>
							</security:authorize>
							<security:authorize access="hasRole('ADMIN')">
								<li><a
									href="<spring:url value="/administratie/adauga-liga.html"/>">&nbsp;&nbsp;Ligi
										&nbsp;</a></li>
							</security:authorize>
							<security:authorize access="hasRole('ADMIN')">
								<li><a
									href="<spring:url value="/administratie/adauga-echipa.html"/>">Echipe</a>
								</li>
							</security:authorize>
							<security:authorize access="hasRole('ADMIN')">
								<li><a
									href="<spring:url value="/administratie/adauga-meci/ligi.html"/>">Meciuri</a>
								</li>
							</security:authorize>
							<security:authorize access="hasRole('ADMIN')">
								<li><a
									href="<spring:url value="/administratie/adauga-liga.html"/>">Utilizatori</a>
								</li>
							</security:authorize>
						</ul>
					</div>
				</security:authorize>
				<security:authorize access="isAuthenticated()">
					<c:if test="${profile_procent < 5 }">
						<div class="alert alert-success">
							<a href="#" data-toggle="modal" data-target="#myModal"
								class="btn btn-xs btn-success pull-right">Completeaza
								profilul</a> <strong>Info:</strong> Profilul tau nu este complet,
							completeaza-ti acum profilul si o sa primesti 100 lei virtuali.
						</div>
					</c:if>
				</security:authorize>
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
					aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<h4 class="modal-title" id="myModalLabel">Detaliile tale</h4>
							</div>
							<spring:url value="/update-profile.html" var="action" />
							<form:form method="POST" modelAttribute="profile"
								class="form-horizontal" action="${action}">
								<div class="modal-body">
									<div class="col-md-5">
										<div class="row">
											<div class="form-group">
												<label for="nume">Nume: </label>
												<form:input id="nume" type="text" class="form-control"
													path="first_name" />
											</div>
											<div class="form-group">
												<label for="prenume">Prenume: </label>
												<form:input id="prenume" type="text" class="form-control"
													path="second_name" />
											</div>
											<div class="form-group">
												<label for="tara">Tara: </label>
												<form:select id="tara" data-placeholder="Alege tara"
													class="form-control" path="country">
													<c:forEach items="${all_countries}" var="country">
														<form:option value="${country}" selected="selected">${country}</form:option>
													</c:forEach>
												</form:select>
											</div>
											<div class="form-group">
												<label for="oras">Oras: </label>
												<form:input id="oras" type="text" class="form-control"
													path="city" />
											</div>
										</div>
									</div>
									<div class="col-md-2"></div>
									<div class="col-md-5">
										<div class="form-group">
											<label for="sex">Sex: </label>
											<form:select id="sex" path="gender"
												data-placeholder="Alege sex-ul" class="form-control">
												<form:option value="Masculin" selected="selected">Masculin</form:option>
												<form:option value="Masculin">Feminin</form:option>
											</form:select>
										</div>
										<div class="form-group">
											<label for="birthdate">Ziua de nastere</label>
											<form:input class="form-control" type="text" id="birthdate"
												path="birthdate" />
										</div>
										<div class="form-group">
											<label for="sex">Agentie pariuri: </label>
											<form:select path="betagency" id="sex"
												data-placeholder="Alege sex-ul" class="form-control">
												<form:option value="Bet365" selected="selected">Bet365</form:option>
												<form:option value="WinMaster">WinMaster</form:option>
												<form:option value="Netbet">Netbet</form:option>
												<form:option value="Stanleybet">Stanleybet</form:option>
											</form:select>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default"
										data-dismiss="modal">Inchide</button>
									<form:button type="submit" class="btn btn-primary">Salveaza</form:button>
								</div>
							</form:form>
						</div>
					</div>
				</div>
				<!-- /Breadcrumbs line -->
				<!--=== Page Content ===-->
				<div class="row">
					<div class="top-buffer"></div>
					<div class="col-md-12">
						<tiles:insertAttribute name="body" />
						<tiles:insertAttribute name="rsidebar" />
					</div>
					<!-- /.col-md-12 -->
					<!-- /Example Box -->
				</div>
				<!-- /.row -->
				<!-- /Page Content -->

			</div>
			<!-- /.container -->
		</div>
	</div>

</body>

</html>