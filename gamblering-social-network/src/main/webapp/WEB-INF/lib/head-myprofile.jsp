<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<link
	href="<%=request.getContextPath()%>/resources/ticketplugin/ticket.css"
	rel="stylesheet" type="text/css" />
	
<!-- Page specific plugins -->
<!-- Charts -->
<!--[if lt IE 9]>
		<script type="text/javascript" src="plugins/flot/excanvas.min.js"></script>
	<![endif]-->

<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/plugins/daterangepicker/moment.min.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/plugins/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/plugins/blockui/jquery.blockUI.min.js"></script>

<!-- App -->
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/assets/js/app.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/assets/js/plugins.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/assets/js/plugins.form-components.js"></script>

<script
	src="<%=request.getContextPath()%>/resources/bootstrap/js/locales-dates.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/bootstrap/js/bootstrap-date.js"></script>
