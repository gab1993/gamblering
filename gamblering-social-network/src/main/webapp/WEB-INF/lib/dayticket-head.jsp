<style>
.widget-products li>a:after {
	content: "Vezi biletul";
	font-size: 0.875em;
	font-style: normal;
	font-weight: normal;
	margin-top: 32px;
	position: absolute;
	right: 10px;
	text-decoration: inherit;
	top: 0;
	color: #2ecc71;
	font-size: 1.3em;
}

.checkbox-nice label {
	padding-top: 3px;
}

.checkbox-nice input[type=checkbox] {
	visibility: hidden;
}

.checkbox-nice {
	position: relative;
	padding-left: 15px;
}

.widget-todo .name {
	float: left;
}

.widget-todo>li {
	border-bottom: 1px solid #ebebeb;
	padding: 10px 5px;
}

.widget-todo {
	list-style: none;
	margin: 0;
	padding: 0;
}

.widget-products li .product>.warranty>i {
	color: #f1c40f;
}

.widget-products li .product>.warranty {
	display: block;
	text-decoration: none;
	width: 50%;
	float: left;
}

.widget-products li .product>.price>i {
	color: #2ecc71;
}

.widget-products li .product>.price {
	display: block;
	text-decoration: none;
	width: 50%;
	float: left;
	font-size: 0.875em;
}

.widget-products li .product>.name {
	display: block;
	font-weight: 600;
	padding-bottom: 7px;
}

.widget-products li .product {
	display: block;
	margin-left: 90px;
	margin-top: 19px;
}

.widget-products li .img {
	display: block;
	float: left;
	text-align: center;
	width: 70px;
	height: 68px;
	overflow: hidden;
	margin-top: 7px;
}

.widget-products li>a {
	height: 88px;
	display: block;
	width: 100%;
	color: #344644;
	padding: 3px 10px;
	position: relative;
	-webkit-transition: border-color 0.1s ease-in-out 0s, background-color
		0.1s ease-in-out 0s;
	transition: border-color 0.1s ease-in-out 0s, background-color 0.1s
		ease-in-out 0s;
}

.widget-products li {
	border-bottom: 1px solid #ebebeb;
}

.widget-products {
	list-style: none;
	margin: 0;
	padding: 0;
}
</style>