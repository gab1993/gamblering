package com.gab.gsn.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gab.gsn.entity.Post;
import com.gab.gsn.entity.Profile;
import com.gab.gsn.services.PostService;

@Controller
public class HomeController {

	@Autowired
	private PostService postService;
	
	@ModelAttribute("profile") Profile createProfile(){
		return new Profile();
	}
	@RequestMapping("/")
	public String home(Model model) {
		Page<Post> posts_page = postService.getAllPostsByDate(1);
		List<Post> posts = posts_page.getContent();
		model.addAttribute("posts", posts);
		model.addAttribute("no_ticket",true);
		return "index";
	}
	
	@RequestMapping("/index")
	public String index(Model model) {
		Page<Post> posts_page = postService.getAllPostsByDate(1);
		List<Post> posts = posts_page.getContent();
		model.addAttribute("posts", posts);
		model.addAttribute("no_ticket",true);
		return "index";
	}

}
