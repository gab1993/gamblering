package com.gab.gsn.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gab.gsn.entity.BetTicket;
import com.gab.gsn.entity.Profile;
import com.gab.gsn.entity.Tipster;
import com.gab.gsn.entity.Vote;
import com.gab.gsn.exceptions.ExistingVoteException;
import com.gab.gsn.services.BetTicketService;
import com.gab.gsn.services.TipsterService;
import com.gab.gsn.services.VoteService;

@Controller
public class VisitorController {

	@Autowired
	private BetTicketService betTicketService;
	
	@Autowired 
	private VoteService voteService;
	
	@Autowired
	private TipsterService tipsterService;
	
	@ModelAttribute("vote")
	public Vote createVote(){
		return new Vote();
	}
	
	@ModelAttribute("profile") Profile createProfile(){
		return new Profile();
	}
	
	@RequestMapping(value = "/pronosticuri.*", method = RequestMethod.GET)
	public String showPage() {

		return "visitor-search-engine";
	}

	@RequestMapping(value = "/pronosticuri/ultimile-bilete/{page_number}.html", method = RequestMethod.GET)
	public String getLastTickets(@PathVariable("page_number") int page_number,
			Model model) {
		Page<BetTicket> page = betTicketService.getPageNo(page_number);

		int current = page.getNumber() + 1;
		int begin = Math.max(1, current - 5);
		int end = Math.min(begin + 10, page.getTotalPages());
		
		List<BetTicket> tickets = page.getContent();
		betTicketService.setTotalOdd(tickets);
		
		model.addAttribute("tickets", tickets);
		model.addAttribute("ticketsLog", page);
		model.addAttribute("beginIndex", begin);
		model.addAttribute("endIndex", end);
		model.addAttribute("currentIndex", current);

		return "last-tickets";
	}
	
	@RequestMapping(value = "/pronosticuri/cele-mai-multe-voturi/{page_number}.html", method = RequestMethod.GET)
	public String getTopVotes(@PathVariable("page_number") int page_number,
			Model model) {
		Page<BetTicket> page = betTicketService.getPageByVotes(page_number);

		int current = page.getNumber() + 1;
		int begin = Math.max(1, current - 5);
		int end = Math.min(begin + 10, page.getTotalPages());
		
		List<BetTicket> tickets = page.getContent();
		betTicketService.setTotalOdd(tickets);
		
		model.addAttribute("tickets", tickets);
		model.addAttribute("ticketsLog", page);
		model.addAttribute("beginIndex", begin);
		model.addAttribute("endIndex", end);
		model.addAttribute("currentIndex", current);

		return "last-tickets";
	}
	
	@RequestMapping(value = "/pronosticuri/cele-mai-mari-cote/{page_number}.html", method = RequestMethod.GET)
	public String getBiggestOdds(@PathVariable("page_number") int page_number,
			Model model) {
		Page<BetTicket> page = betTicketService.getPageOdds(page_number);

		int current = page.getNumber() + 1;
		int begin = Math.max(1, current - 5);
		int end = Math.min(begin + 10, page.getTotalPages());
		
		List<BetTicket> tickets = page.getContent();
		betTicketService.setTotalOdd(tickets);
		
		model.addAttribute("tickets", tickets);
		model.addAttribute("ticketsLog", page);
		model.addAttribute("beginIndex", begin);
		model.addAttribute("endIndex", end);
		model.addAttribute("currentIndex", current);

		return "last-tickets";
	}
	
	@RequestMapping(value="/pronosticuri/ultimile-bilete/{page_number}.html", method = RequestMethod.POST)
	public String voteUp(@PathVariable("page_number") long page_number, 
						 	@ModelAttribute("vote") Vote vote, RedirectAttributes ra){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if(auth instanceof AnonymousAuthenticationToken) {
			ra.addFlashAttribute("anonymous_vote", true);
			return "redirect:/pronosticuri/ultimile-bilete/"+page_number+".html";
		}
		else {
			try {
				voteService.voteTicket(auth.getName(), vote.getTicket_id());
			} catch (ExistingVoteException e) {
				ra.addFlashAttribute("vote_exists", true);
				return "redirect:/pronosticuri/ultimile-bilete/"+page_number+".html";
			}
		}
		ra.addFlashAttribute("success", true);
		return "redirect:/pronosticuri/ultimile-bilete/"+page_number+".html";
	}
	
	@RequestMapping(value="/pronosticuri/cele-mai-mari-cote/{page_number}.html", method = RequestMethod.POST)
	public String voteUpTopOdds(@PathVariable("page_number") long page_number, 
						 	@ModelAttribute("vote") Vote vote, RedirectAttributes ra){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if(auth instanceof AnonymousAuthenticationToken) {
			ra.addFlashAttribute("anonymous_vote", true);
			return "redirect:/pronosticuri/cele-mai-mari-cote/"+page_number+".html";
		}
		else {
			try {
				voteService.voteTicket(auth.getName(), vote.getTicket_id());
			} catch (ExistingVoteException e) {
				ra.addFlashAttribute("vote_exists", true);
				return "redirect:/pronosticuri/cele-mai-mari-cote/"+page_number+".html";
			}
		}
		ra.addFlashAttribute("success", true);
		return "redirect:/pronosticuri/cele-mai-mari-cote/"+page_number+".html";
	}
	
	@RequestMapping(value="/pronosticuri/cele-mai-multe-voturi/{page_number}.html", method = RequestMethod.POST)
	public String voteUpTopVoted(@PathVariable("page_number") long page_number, 
						 	@ModelAttribute("vote") Vote vote, RedirectAttributes ra){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if(auth instanceof AnonymousAuthenticationToken) {
			ra.addFlashAttribute("anonymous_vote", true);
			return "redirect:/pronosticuri/cele-mai-multe-voturi/"+page_number+".html";
		}
		else {
			try {
				voteService.voteTicket(auth.getName(), vote.getTicket_id());
			} catch (ExistingVoteException e) {
				ra.addFlashAttribute("vote_exists", true);
				return "redirect:/pronosticuri/cele-mai-multe-voturi/"+page_number+".html";
			}
		}
		ra.addFlashAttribute("success", true);
		return "redirect:/pronosticuri/cele-mai-multe-voturi/"+page_number+".html";
	}
	
	@RequestMapping(value = "/pronosticuri/bilete-castigatoare/{page_number}.html", method = RequestMethod.GET)
	public String winnerTicketsGet(@PathVariable("page_number") int page_number,
			Model model) {
		Page<BetTicket> page = betTicketService.getWinnerTicketsPage(page_number);

		int current = page.getNumber() + 1;
		int begin = Math.max(1, current - 5);
		int end = Math.min(begin + 10, page.getTotalPages());
		
		List<BetTicket> tickets = page.getContent();
		betTicketService.setTotalOdd(tickets);
		
		model.addAttribute("tickets", tickets);
		model.addAttribute("ticketsLog", page);
		model.addAttribute("beginIndex", begin);
		model.addAttribute("endIndex", end);
		model.addAttribute("currentIndex", current);

		return "last-tickets";
	}
	
	@RequestMapping(value="/pronosticuri/bilete-castigatoare/{page_number}.html", method = RequestMethod.POST)
	public String winnerTicketsPost(@PathVariable("page_number") long page_number, 
						 	@ModelAttribute("vote") Vote vote, RedirectAttributes ra){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if(auth instanceof AnonymousAuthenticationToken) {
			ra.addFlashAttribute("anonymous_vote", true);
			return "redirect:/pronosticuri/cele-mai-multe-voturi/"+page_number+".html";
		}
		else {
			try {
				voteService.voteTicket(auth.getName(), vote.getTicket_id());
			} catch (ExistingVoteException e) {
				ra.addFlashAttribute("vote_exists", true);
				return "redirect:/pronosticuri/cele-mai-multe-voturi/"+page_number+".html";
			}
		}
		ra.addFlashAttribute("success", true);
		return "redirect:/pronosticuri/cele-mai-multe-voturi/"+page_number+".html";
	}
	
	@RequestMapping(value = "/pronosticuri/tipster/{username}/{page_number}.html", method = RequestMethod.GET)
	public String tipsterTicektsGet(@PathVariable("page_number") int page_number, @PathVariable("username") String username,
			Model model) {
		Page<BetTicket> page = betTicketService.getTipsterTickets(page_number, username);

		int current = page.getNumber() + 1;
		int begin = Math.max(1, current - 5);
		int end = Math.min(begin + 10, page.getTotalPages());
		
		List<BetTicket> tickets = page.getContent();
		betTicketService.setTotalOdd(tickets);
		
		model.addAttribute("tickets", tickets);
		model.addAttribute("ticketsLog", page);
		model.addAttribute("beginIndex", begin);
		model.addAttribute("endIndex", end);
		model.addAttribute("currentIndex", current);

		return "last-tickets";
	}
	
	@RequestMapping(value = "/pronosticuri/cota-minima/{min_odd}/{page_number}.html", method = RequestMethod.GET)
	public String minOddTicektsGet(@PathVariable("page_number") int page_number, @PathVariable("min_odd") int odd,
			Model model) {
		Page<BetTicket> page = betTicketService.getTicketsWithMinOdd(page_number, odd);

		int current = page.getNumber() + 1;
		int begin = Math.max(1, current - 5);
		int end = Math.min(begin + 10, page.getTotalPages());
		
		List<BetTicket> tickets = page.getContent();
		betTicketService.setTotalOdd(tickets);
		
		model.addAttribute("tickets", tickets);
		model.addAttribute("ticketsLog", page);
		model.addAttribute("beginIndex", begin);
		model.addAttribute("endIndex", end);
		model.addAttribute("currentIndex", current);

		return "last-tickets";
	}
	
	@RequestMapping(value = "/pronosticuri/cota-maxima/{max_odd}/{page_number}.html", method = RequestMethod.GET)
	public String maxOddTicektsGet(@PathVariable("page_number") int page_number, @PathVariable("max_odd") int odd,
			Model model) {
		Page<BetTicket> page = betTicketService.getTicketsWithMaxOdd(page_number, odd);

		int current = page.getNumber() + 1;
		int begin = Math.max(1, current - 5);
		int end = Math.min(begin + 10, page.getTotalPages());
		
		List<BetTicket> tickets = page.getContent();
		betTicketService.setTotalOdd(tickets);
		
		model.addAttribute("tickets", tickets);
		model.addAttribute("ticketsLog", page);
		model.addAttribute("beginIndex", begin);
		model.addAttribute("endIndex", end);
		model.addAttribute("currentIndex", current);

		return "last-tickets";
	}
	
	@RequestMapping(value = "/pronosticuri/biletul-zilei/{page_number}.html", method = RequestMethod.GET)
	public String dayTicket(@PathVariable("page_number") int page_number, Model model) {
		Page<BetTicket> page = betTicketService.getDaysTickets(page_number);

		int current = page.getNumber() + 1;
		int begin = Math.max(1, current - 5);
		int end = Math.min(begin + 10, page.getTotalPages());
		
		List<BetTicket> tickets = page.getContent();
		betTicketService.setTotalOdd(tickets);
		
		model.addAttribute("tickets", tickets);
		model.addAttribute("ticketsLog", page);
		model.addAttribute("beginIndex", begin);
		model.addAttribute("endIndex", end);
		model.addAttribute("currentIndex", current);

		return "dayticket";
	}
	
	@RequestMapping(value = "/pronosticuri/top-tipsteri/{page_number}.html", method = RequestMethod.GET)
	public String topTipstersTicektsGet(@PathVariable("page_number") int page_number,
			Model model) {
		Page<BetTicket> page = betTicketService.getTopTipstersTickets(page_number);

		int current = page.getNumber() + 1;
		int begin = Math.max(1, current - 5);
		int end = Math.min(begin + 10, page.getTotalPages());
		
		List<BetTicket> tickets = page.getContent();
		betTicketService.setTotalOdd(tickets);
		
		model.addAttribute("tickets", tickets);
		model.addAttribute("ticketsLog", page);
		model.addAttribute("beginIndex", begin);
		model.addAttribute("endIndex", end);
		model.addAttribute("currentIndex", current);

		return "last-tickets";
	}
	
	@RequestMapping(value = "/pronosticuri/clasamente.html", method = RequestMethod.GET)
	public String tipsterTableGet(Model model){
		java.util.Set<Tipster> tipsters = tipsterService.getTipstersRanking();
		tipsterService.getTipstersTotalOdd(tipsters);
		model.addAttribute("tipsters", tipsters);
		return "tipster-rankings";	
	}
	@ModelAttribute("min-odd")
	public Odd createOdd(){
		return new Odd();
	}
	@RequestMapping(value="min-odd.html", method = RequestMethod.POST)
	public String parseMinOdd(@ModelAttribute("min-odd") Odd min_odd, RedirectAttributes ra){
		return "redirect:/pronosticuri/cota-minima/"+min_odd.getOdd_value()+"/1.html";
	}
	
	@RequestMapping(value="max-odd.html", method = RequestMethod.POST)
	public String parseMaxOdd(@ModelAttribute("min-odd") Odd min_odd, RedirectAttributes ra){
		return "redirect:/pronosticuri/cota-maxima/"+min_odd.getOdd_value()+"/1.html";
	}

	
	private class Odd {
		
		int odd_value;
		
		public void setOdd_value(int odd_value){
			this.odd_value = odd_value;
		}
		
		public int getOdd_value(){
			return this.odd_value;
		}
	}
}
