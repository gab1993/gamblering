package com.gab.gsn.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gab.gsn.entity.Post;
import com.gab.gsn.entity.Profile;
import com.gab.gsn.services.PostService;
import com.gab.gsn.services.TipsterService;

@Controller
public class BloggerController {
	
	@Autowired
	private PostService postService;
	
	@Autowired
	private TipsterService tipsterService;
	
	@ModelAttribute("post")
	public Post createPost(){
		return new Post();
	}
	
	@ModelAttribute("profile") Profile createProfile(){
		return new Profile();
	}
	
	@RequestMapping(value = "/blog/posteaza.html", method = RequestMethod.GET)
	public String createPostView(){
		
		return "blog-post";
	}
	
	@RequestMapping(value = "/blog/posteaza.html", method = RequestMethod.POST)
	public String savePost(@ModelAttribute("post") Post post, BindingResult result){
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if(!result.hasErrors()){
			post.setAuthor(tipsterService.findOneByUsername(auth.getName()));
			postService.save(post);
		}
		return "blog-post";
	}
}
