package com.gab.gsn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gab.gsn.entity.Profile;
import com.gab.gsn.entity.Role;
import com.gab.gsn.entity.Team;
import com.gab.gsn.entity.Tipster;
import com.gab.gsn.repository.RoleRepository;
import com.gab.gsn.services.LeagueService;
import com.gab.gsn.services.TeamService;
import com.gab.gsn.services.TipsterService;

@Controller
public class TeamController {
	
	@Autowired
	private LeagueService leagueService;
	
	@Autowired
	private TipsterService tipsterService;
	
	@Autowired 
	private RoleRepository roleRepository;
	
	@Autowired
	private TeamService teamService;
	
	@ModelAttribute("team")
	public Team createTeam(){
		return new Team();
	}
	
	@ModelAttribute("profile") Profile createProfile(){
		return new Profile();
	}
	
	@RequestMapping(value="/administratie/adauga-echipa.*", method = RequestMethod.GET)
	public String getAddTeamPage(Model model){
		model.addAttribute("admin_leagues", leagueService.getAllLeagues());
		return "addteam";
	}
	
	@RequestMapping(value="/administratie/adauga-echipa.*", method = RequestMethod.POST)
	public String addTeam(@ModelAttribute("team") Team team, BindingResult results, 
							Errors error, RedirectAttributes ra){
		
		Authentication admin_auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (admin_auth instanceof AnonymousAuthenticationToken)
			return "redirect:/login.html";
		
		Tipster admin = tipsterService.findOneByUsername(admin_auth.getName());
		Role admin_role = roleRepository.findOneByName("ADMIN");
		if(!admin.getRoles().contains(admin_role))
			return "redirect:/login.html";
		
		teamService.saveNewTeamInLeague(team);
		ra.addFlashAttribute("success", true);
		return "redirect:/administratie/adauga-echipa.html";
	}
}
