package com.gab.gsn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gab.gsn.entity.League;
import com.gab.gsn.entity.Ticket;
import com.gab.gsn.services.LeagueService;

@Controller
@Scope("request")
public class LeagueController {
	
	@Autowired
	private LeagueService leagueService;
	
	@Autowired
	private Ticket ticket;
	
	@ModelAttribute("league")
	public League createLeague(){
		return new League();
	}
	
	@ModelAttribute("league")
	public League createLeaguesList(){
		return new League();
	}
	
	@RequestMapping(value = "/administratie/adauga-liga.*", method = RequestMethod.GET)
	public String addLeagueGet(Model model){
		model.addAttribute("admin_leagues", leagueService.getAllLeagues());
		return "addleague";
	}
	
	@RequestMapping(value = "/administratie/adauga-liga.*", method = RequestMethod.POST)
	public String addLeaguePost(@ModelAttribute("league") League league, 
															BindingResult results, Error erros, RedirectAttributes ra){
		if(!results.hasErrors()){
			leagueService.save(league);
			ra.addFlashAttribute("success", true);
			return "redirect:/administratie/adauga-liga.html";
		}
		return "addleague";
	}
	
	@RequestMapping(value = "/liga/{league_id}.*", method = RequestMethod.GET)
	public String getLeagueTeams(@PathVariable("league_id") int league_id, Model model){
		model.addAttribute("league_teams", leagueService.findLeagueTeams(league_id));
		model.addAttribute("bets", ticket.getMatchBets());
		model.addAttribute("odd", ticket.getOdd());

		return "league-profile";
	}
	
	@RequestMapping(value="/pariuri/tara/{country}.html",method=RequestMethod.GET)
	public String getCountries(@PathVariable("country") String country, Model model){
		model.addAttribute("country_leagues", leagueService.getCountryLeagues(country));
		model.addAttribute("bets", ticket.getMatchBets());
		model.addAttribute("odd", ticket.getOdd());

		return "country-league";
	}
}
