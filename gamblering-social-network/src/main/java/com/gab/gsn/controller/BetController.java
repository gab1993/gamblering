package com.gab.gsn.controller;

import java.util.Set;




import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gab.gsn.entity.Match;
import com.gab.gsn.entity.MatchBet;
import com.gab.gsn.entity.Profile;
import com.gab.gsn.entity.Ticket;
import com.gab.gsn.exceptions.InsufficientFundsException;
import com.gab.gsn.services.LeagueService;
import com.gab.gsn.services.MatchBetService;
import com.gab.gsn.services.MatchService;
import com.gab.gsn.services.TipsterService;
import com.gab.gsn.util.Stake;

@Controller
@Scope("request")
public class BetController {

	@Autowired
	private Ticket ticket;

	@Autowired
	private MatchService matchService;

	@Autowired
	private TipsterService tipsterService;

	@Autowired
	private MatchBetService matchBetService;

	@Autowired
	private LeagueService leagueService;

	@ModelAttribute("matchBet")
	public MatchBet createBet() {
		return new MatchBet();
	}

	@ModelAttribute("stake_value")
	public Stake createStake() {
		return new Stake();
	}
	
	@ModelAttribute("profile") Profile createProfile(){
		return new Profile();
	}
	
	@RequestMapping(value = "/pariuri/liga/{league_id}.*", method = RequestMethod.GET)
	public String getOffer(@PathVariable("league_id") int league_id, Model model) {
		Set<Match> matchLeagueList = matchService.getLeagueMatches(league_id);
		model.addAttribute("league_matches", matchLeagueList);
		model.addAttribute("bets", ticket.getMatchBets());
		model.addAttribute("odd", ticket.getOdd());
		return "betemplate";
	}

	@RequestMapping(value = "/pariuri/liga/{league_id}.*", method = RequestMethod.POST)
	public String addMatch(@PathVariable("league_id") long league_id,
			@ModelAttribute("matchBet") MatchBet bet, BindingResult result,
			Error eror, RedirectAttributes ra) {

		if (ticket.getMatchBets().contains(bet)) {
			ticket.getMatchBets().remove(bet);
			ticket.setOdd(ticket.getOdd() / bet.getOdd());
			ra.addFlashAttribute("pariu_existent", true);
			return "redirect:/pariuri/liga/" + league_id + ".html";
		} else 
			if (ticket.getMatchesSet().contains(bet.getMatch())) {
				ra.addFlashAttribute("meci_existent", true);
			return "redirect:/pariuri/liga/" + league_id + ".html";
		} else {
			ticket.addMatchBet(bet);
			return "redirect:/pariuri/liga/" + league_id + ".html";
		}

	}

	@RequestMapping(value = "/salveaza-biletul.html")
	public String saveTicket(@RequestParam(value = "stake") double stake, @RequestParam(value = "dayticket") boolean dayticket,
			RedirectAttributes ra) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		try {
			tipsterService.parseTicket(auth.getName(), ticket, stake, dayticket);
		} catch (InsufficientFundsException e) {
			ra.addFlashAttribute("no_funds", true);
			return "redirect:/index.html";
		}
		ticket.clearTicket();
		return "redirect:/pronosticuri/ultimile-bilete/1.html";
	}
	
	@RequestMapping(value = "/sterge-meciul.html")
	public String delete_match(HttpServletRequest request, @RequestParam(value = "home_team") String home_team,@RequestParam(value = "away_team") String away_team,
			RedirectAttributes ra) {
		
		String referer = request.getHeader("Referer");
		ticket.deleteMatch(home_team, away_team);
		return "redirect:"+referer;
	}
	
	@ModelAttribute("bet")
	public MatchBet createMBet(){
		return new MatchBet();
	}

}
