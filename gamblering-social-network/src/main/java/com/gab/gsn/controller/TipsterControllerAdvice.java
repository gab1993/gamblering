package com.gab.gsn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.gab.gsn.entity.Profile;
import com.gab.gsn.services.BetTicketService;
import com.gab.gsn.services.LeagueService;
import com.gab.gsn.services.NotificationService;
import com.gab.gsn.services.ProfileService;
import com.gab.gsn.services.RoleService;
import com.gab.gsn.services.TipsterService;

@ControllerAdvice
public class TipsterControllerAdvice {

	@Autowired
	private ProfileService profileService;
	
	@Autowired
	private TipsterService tipsterService;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired 
	private LeagueService leagueService;
	
	@Autowired 
	private NotificationService notificationService;
	
	@Autowired
	private BetTicketService betTicketService;
	
	@ModelAttribute("profile") Profile createProfile(){
		return new Profile();
	}
	
	@ModelAttribute
	public void findUserDetails(Model tipster){
		
		 String[] all_countries = {"Albania", "Andorra","Armenia","Austria","Azerbaidjan","Belarus","Belgia","Bosnia și Herțegovina","Bulgaria","Cehia","Cipru","Croația","Danemarca","Elveția"
					,"Estonia","Finlanda","Franța","Georgia","Germania","Grecia","Irlanda","Islanda","Italia","Kazahstan","Letonia","Liechtenstein","Lituania","Luxemburg","Macedonia"
					,"Malta","Marea Britanie","Moldova","Monaco","Muntenegru","Norvegia","Olanda","Polonia","Portugalia","Romania","Rusia","San Marino","Serbia","Slovacia","Slovenia","Spania"
					,"Suedia","Turcia", "Ucraina", "Ungaria", "Vatican"};
		 
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			String username = auth.getName();
			tipster.addAttribute("all_countries", all_countries);
			tipster.addAttribute("status", betTicketService.findTipsterReport(auth.getName()));
			tipster.addAttribute("auth_user_username", username);
			tipster.addAttribute("auth_user_role", roleService.getUserRole(username).getRole());
			tipster.addAttribute("leagues", leagueService.findAll());
			tipster.addAttribute("countries", leagueService.findAllCountries());
			tipster.addAttribute("tipster_notifications",notificationService.findTipsterNotifications(username));
			tipster.addAttribute("notif_no", notificationService.findTipsterNotifications(username).size());
			tipster.addAttribute("bankroll", tipsterService.findOneByUsername(username).getBankroll());
			tipster.addAttribute("profile_procent", profileService.getProfileProcent(username));
		}
	}
}
