package com.gab.gsn.controller;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gab.gsn.entity.Match;
import com.gab.gsn.entity.Profile;
import com.gab.gsn.entity.Scores;
import com.gab.gsn.repository.MatchBetRepository;
import com.gab.gsn.services.BetTypeService;
import com.gab.gsn.services.LeagueService;
import com.gab.gsn.services.MatchService;
import com.gab.gsn.util.Time;

@Controller
public class MatchController {

	@Autowired
	private LeagueService leagueService;

	@Autowired
	private MatchService matchService;

	@Autowired
	private BetTypeService betTypeService;

	@Autowired
	private MatchBetRepository matchBetRepository;

	@ModelAttribute("match")
	public Match createMatch() {
		return new Match();
	}

	@ModelAttribute("scores")
	public Scores createScore() {
		return new Scores();
	}
	
	@ModelAttribute("profile") Profile createProfile(){
		return new Profile();
	}
	
	@RequestMapping(value = "/administratie/adauga-meci/liga/{league_id}.*", method = RequestMethod.POST)
	public String addMatch(@PathVariable("league_id") int league_id,
			@ModelAttribute("match") Match match, BindingResult results,
			Error errors, RedirectAttributes ra) {

		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date = null;
		try {
			date = sdf1.parse(match.getMatch_time_string());
		} catch (ParseException e) {
			e.printStackTrace();
		}

		java.sql.Date sqlStartDate = new Date(date.getTime());
		match.setMatch_time(sqlStartDate);
		match.setLeague_id(league_id);
		matchService.saveMatch(match);
		matchService.saveMatchOdds(match.getMatchBetList(), match);
		ra.addFlashAttribute("success", true);
		return "redirect:/administratie/adauga-meci/liga/" + league_id
				+ ".html";
	}

	@RequestMapping(value = "/administratie/adauga-meci/liga/{league_id}.*", method = RequestMethod.GET)
	public String addMatchView(@PathVariable("league_id") int league_id,
			Model model) {
		model.addAttribute("league_id", league_id);
		model.addAttribute("league_teams",
				leagueService.findLeagueTeams(league_id));
		model.addAttribute("hours_list", Time.getHours());
		model.addAttribute("bet_types", betTypeService.findAllBetTypes());
		return "addmatch";
	}

	@RequestMapping(value = "/administratie/adauga-meci/ligi.*", method = RequestMethod.GET)
	public String findLeagueView(Model model) {
		model.addAttribute("admin_leagues", leagueService.getAllLeagues());
		return "getleagues";
	}

	@RequestMapping(value = "/administratie/adauga-scor/liga/{league_id}.*", method = RequestMethod.GET)
	public String addScoreView(@PathVariable("league_id") int league_id,
			Model model) {
		model.addAttribute("league_matches",
				matchService.getLeagueNoFinishedMatches(league_id));
		return "addscore";
	}

	@RequestMapping(value = "/administratie/adauga-scor/liga/{league_id}.*", method = RequestMethod.POST)
	public String addScorePost(@ModelAttribute("scores") Scores scores,
			@PathVariable("league_id") int league_id, RedirectAttributes ra) {
		matchService.saveScores(scores);
		ra.addFlashAttribute("vote_exists", true);
		return "redirect:/administratie/adauga-scor/liga/"+league_id+".html";
	}
}
