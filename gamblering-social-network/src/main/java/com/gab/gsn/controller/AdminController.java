package com.gab.gsn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gab.gsn.services.BetTicketService;

@Controller
public class AdminController {
		
	@Autowired
	private BetTicketService betService;
	
	@RequestMapping(value = "/validating-process.html", method = RequestMethod.GET)
	public String validate(){
		betService.validateTickets();
		return "redirect:/validate";
	}
}
