package com.gab.gsn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gab.gsn.entity.Profile;
import com.gab.gsn.entity.Tipster;
import com.gab.gsn.exceptions.EmailExistsException;
import com.gab.gsn.exceptions.UsernameExistsException;
import com.gab.gsn.services.TipsterService;

@Controller
public class RegisterController {
	
	@Autowired
	private TipsterService tipsterService;
	
	@ModelAttribute("user")
	public Tipster createTipster(){
		return new Tipster();
	}
	
	@ModelAttribute("profile") Profile createProfile(){
		return new Profile();
	}
	
	@RequestMapping(value = "/inregistrare.*", method = RequestMethod.GET)
	public String showRegister(){
		return "register";
	}
	
	@RequestMapping(value="/inregistrare.*", method = RequestMethod.POST)
	public String registerUserAccount( @ModelAttribute("user") Tipster tipster, 
			 										BindingResult result, Errors errors, RedirectAttributes ra) {
		int ok = 1;
		if(!result.hasErrors()){
			try {
				tipsterService.registerUser(tipster);
			} catch (EmailExistsException e) {
				ok = 0;
				ra.addFlashAttribute("email", true);
				return "redirect:/inregistrare.html";
			} catch (UsernameExistsException e){
				ok = 0;
				ra.addFlashAttribute("username", true);
				return "redirect:/inregistrare.html";
			}
		}
		
		if(ok == 1) {
			ra.addFlashAttribute("success", true);
			return "redirect:/inregistrare.html";
		}
		return "register";
	}
	
}
