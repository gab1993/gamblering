package com.gab.gsn.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gab.gsn.entity.BetTicket;
import com.gab.gsn.entity.Follower;
import com.gab.gsn.entity.Profile;
import com.gab.gsn.entity.Ticket;
import com.gab.gsn.entity.Tipster;
import com.gab.gsn.exceptions.FollowingException;
import com.gab.gsn.services.BetTicketService;
import com.gab.gsn.services.ProfileService;
import com.gab.gsn.services.RoleService;
import com.gab.gsn.services.TipsterService;
import com.gab.gsn.services.VoteService;

@Controller
@Scope("request")
public class TipsterController {

	@Autowired
	private ProfileService profileService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private BetTicketService betTicketService;

	@Autowired
	private Ticket ticket;

	@Autowired
	private VoteService voteService;

	@Autowired
	private TipsterService tipsterService;

	@ModelAttribute("profile")
	public Profile createProfile() {
		return new Profile();
	}
	
	@ModelAttribute("follower")
	public Follower getUsername() {
		return new Follower();
	}
	
	@RequestMapping("/login.*")
	public String login() {
		return "login";
	}

	@RequestMapping(value = "/profil/{username}.html", method = RequestMethod.GET)
	public String getProfile(@PathVariable("username") String username,
			Model model, RedirectAttributes ra) {

		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			Profile tipster_profile = profileService
					.findOneByUsername(username);
			if (tipster_profile == null) {
				return "redirect:/404.html";
			}

			Page<BetTicket> page = betTicketService.getLastFive(username, 1);

			List<BetTicket> tickets = page.getContent();
			betTicketService.setTotalOdd(tickets);

			model.addAttribute("status",
					betTicketService.findTipsterReport(username));
			model.addAttribute("last_five", tickets);
			model.addAttribute("last_ticket",
					betTicketService.findLastTipsterTicket(username));
			model.addAttribute("tipster_profile", tipster_profile);
			model.addAttribute("tipster_role",
					roleService.getUserRole(username));
			model.addAttribute("followers_no",
					profileService.getProfileFollowersNumber(username));
			model.addAttribute("isfollowed", profileService.isFollowedBy(
					auth.getName(), tipster_profile));
			model.addAttribute("bets", ticket.getMatchBets());
			model.addAttribute("odd", ticket.getOdd());
			model.addAttribute("tipster_bankroll", tipsterService
					.findOneByUsername(username).getBankroll());

		}
		return "tipster-profile";
	}

	
	@RequestMapping(value = "/follow_profile.html", method = RequestMethod.POST, headers = "Content-Type=application/x-www-form-urlencoded")
	public String followProfile(@ModelAttribute("follower") Follower follower,
			BindingResult result, Errors errors, RedirectAttributes ra) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();

		if (follower.getFollowed_username() == null || auth.getName() == null)
			return "redirect:/profilul-meu.html";
		try {
			profileService.linkProfiles(auth.getName(),
					follower.getFollowed_username());
		} catch (FollowingException e) {
			ra.addFlashAttribute("follow", true);
			return "redirect:/profil/" + follower.getFollowed_username()
					+ ".html";
		}
		return "redirect:/profil/" + follower.getFollowed_username() + ".html";
	}

	@RequestMapping(value = "/unfollow_profile.html", method = RequestMethod.POST, headers = "Content-Type=application/x-www-form-urlencoded")
	public String unFollowProfile(
			@ModelAttribute("follower") Follower follower,
			BindingResult result, Errors errors, RedirectAttributes ra) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();

		if (follower.getFollowed_username() == null || auth.getName() == null)
			return "redirect:/profilul-meu.html";
		try {
			profileService.unlinkProfiles(auth.getName(),
					follower.getFollowed_username());
		} catch (FollowingException e) {
			ra.addFlashAttribute("follow", false);
			return "redirect:/profil/" + follower.getFollowed_username()
					+ ".html";
		}
		return "redirect:/profil/" + follower.getFollowed_username() + ".html";
	}

	@RequestMapping(value = "/profilul-meu.*", method = RequestMethod.GET)
	public String myProfile(Model model) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		Profile auth_profile = profileService.findOneByUsername(auth.getName());

		Page<BetTicket> page = betTicketService.getLastFive(auth.getName(), 1);

		List<BetTicket> tickets = page.getContent();
		betTicketService.setTotalOdd(tickets);

		model.addAttribute("last_five", tickets);

		model.addAttribute("my_profile", auth_profile);
		model.addAttribute("auth_followers_no",
				profileService.getProfileFollowersNumber(auth.getName()));
		model.addAttribute("last_ticket",
				betTicketService.findLastTipsterTicket(auth.getName()));
		model.addAttribute("bets", ticket.getMatchBets());
		model.addAttribute("odd", ticket.getOdd());

		return "my-profile";
	}

	@RequestMapping(value = "/profilul-meu.*", method = RequestMethod.POST)
	public String updateProfile(@ModelAttribute("profile") Profile profile,
			BindingResult result, Errors errors, RedirectAttributes ra) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();

		if (!result.hasErrors()) {

			profileService.modifyAndUpdate(profile, auth.getName());
			return "redirect:/profilul-meu.html";
		}
		return "my-profile";
	}

	@RequestMapping(value = "/update-profile.html", method = RequestMethod.POST)
	public String modalUpdateProfile(
			@ModelAttribute("profile") Profile profile, Model model,
			BindingResult result, Errors errors, RedirectAttributes ra) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (!result.hasErrors()) {

			profileService.modifyAndUpdate(profile, auth.getName());
			tipsterService.addFundsToAccount(auth.getName(), 100.00);
			return "redirect:/profilul-meu.html";
		}
		return "my-profile";
	}

	@RequestMapping(value = "/tipstari-urmariti.*", method = RequestMethod.GET)
	public String getFollowsList(Model model) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();

		if (!(auth instanceof AnonymousAuthenticationToken)) {
			List<Profile> auth_followers_profiles = profileService
					.findOneByUsername(auth.getName()).getFollowers();
			List<Tipster> auth_followers = profileService
					.extractProfilesTipsters(auth_followers_profiles);
			model.addAttribute("tipsteri_urmariti", auth_followers);
			model.addAttribute("bets", ticket.getMatchBets());
			model.addAttribute("odd", ticket.getOdd());

		}
		return "tipster-followers";
	}

}
