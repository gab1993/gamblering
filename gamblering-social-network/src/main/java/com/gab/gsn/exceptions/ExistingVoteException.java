package com.gab.gsn.exceptions;

public class ExistingVoteException extends Exception {
	
	private static final long serialVersionUID = 639296109638172578L;

	public ExistingVoteException(String message){
		super(message);
	}
}
