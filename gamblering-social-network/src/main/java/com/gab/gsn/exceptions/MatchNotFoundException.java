package com.gab.gsn.exceptions;

public class MatchNotFoundException extends Exception {
	
	private static final long serialVersionUID = 1194545938007838570L;

	public MatchNotFoundException(String message){
		super(message);
	}
}
