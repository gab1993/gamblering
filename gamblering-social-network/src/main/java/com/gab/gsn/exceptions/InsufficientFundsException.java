package com.gab.gsn.exceptions;

public class InsufficientFundsException extends Exception {

	private static final long serialVersionUID = -4719426318416222923L;
	
	public InsufficientFundsException(String message){
		super(message);
	}
}
