package com.gab.gsn.exceptions;

public class FollowingException extends Exception {
	
	private static final long serialVersionUID = -5045188765622753435L;

	public FollowingException(String message){
		super(message);
	}
}
