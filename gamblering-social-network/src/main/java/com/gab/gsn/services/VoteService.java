package com.gab.gsn.services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gab.gsn.entity.BetTicket;
import com.gab.gsn.entity.Tipster;
import com.gab.gsn.exceptions.ExistingVoteException;
import com.gab.gsn.repository.BetTicketRepository;
import com.gab.gsn.repository.TipsterRepository;

@Service
public class VoteService {
	
	@Autowired
	private BetTicketRepository betTicketRepository;
	
	@Autowired
	private TipsterRepository tipsterRepository;
	
	public void voteTicket(String username, long ticket_id) throws ExistingVoteException{
		BetTicket bt = betTicketRepository.findOne(ticket_id);
		Tipster tipster = tipsterRepository.findOneByUsername(username);
		
		Set<Tipster> votes;
		if(bt.getVotes() !=null)
			votes = bt.getVotes();
		else 
			votes = new HashSet<Tipster>();
		
		if(votes.size() == 0) {
			votes.add(tipster);
			bt.setVotes(votes);
			betTicketRepository.save(bt);
		}
		else 
			if(votes.contains(tipster))
				throw new ExistingVoteException("Tipsterul a votat deja acest bilet");
		else {
			votes.add(tipster);
			bt.setVotes(votes);
			betTicketRepository.save(bt);
		}
	}
}
