package com.gab.gsn.services;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.gab.gsn.entity.Post;
import com.gab.gsn.repository.PostRepository;

@Service
public class PostService {

	private static final int PAGE_SIZE = 5;

	@Autowired
	private PostRepository postRepository;

	public void save(Post post) {
		String current_date = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss")
				.format(new Date().getTime());
		post.setPost_date(current_date);
		postRepository.save(post);
	}

	public Page<Post> getAllPostsByDate(int page_number) {
		PageRequest req = new PageRequest(page_number - 1, PAGE_SIZE,
				Sort.Direction.DESC, "post_date");
		return postRepository.getAllPostsByDate(req);
	}
}
