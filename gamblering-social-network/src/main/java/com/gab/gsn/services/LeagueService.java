package com.gab.gsn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gab.gsn.entity.League;
import com.gab.gsn.entity.Team;
import com.gab.gsn.repository.LeagueRepository;

@Service
public class LeagueService {
	
	@Autowired
	private LeagueRepository leagueRepository;
	
	public List<League> getAllLeagues(){
		return leagueRepository.findAll();
	}
	
	public void saveLeagues(List<League> leagues){
		leagueRepository.save(leagues);
	}
	
	public List<Team> findLeagueTeams(int league_id){
		return leagueRepository.findLeagueTeams(league_id);
	}
	
	public League findOne(int league_id){
		return leagueRepository.findOne(league_id);
	}
	
	public List<String> findAllCountries(){
		return leagueRepository.findAllCountries();
	}
	public List<League> findAll() {
		return leagueRepository.findAll();
	}

	public List<League> getCountryLeagues(String country) {
		return leagueRepository.findLeaguesByCountry(country);
	}

	public void save(League league) {
		leagueRepository.save(league);
	}
}
