package com.gab.gsn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gab.gsn.entity.BetType;
import com.gab.gsn.repository.BetTypeRepository;

@Service
public class BetTypeService {
	
	@Autowired
	private BetTypeRepository betTypeRepository;
	
	public List<BetType> findAllBetTypes(){
		return betTypeRepository.findAllBetTypes();
	}
}
