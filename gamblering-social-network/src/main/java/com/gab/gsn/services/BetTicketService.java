package com.gab.gsn.services;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.gab.gsn.entity.BetTicket;
import com.gab.gsn.entity.MatchBet;
import com.gab.gsn.entity.Tipster;
import com.gab.gsn.repository.BetTicketRepository;
import com.gab.gsn.repository.TipsterRepository;

@Service
@Transactional
public class BetTicketService {

	private static final int PAGE_SIZE = 50;

	@Autowired
	private BetTicketRepository betRepository;

	@Autowired
	private TipsterRepository tipsterRepository;

	@Autowired
	private ProfileService profileService;

	public void save(BetTicket betTicket) {
		betRepository.save(betTicket);
	}

	public BetTicket findLastTipsterTicket(String username) {
		List<BetTicket> btlist = betRepository.findLastTipsterTicket(username);
		if (btlist.size() > 0)
			return btlist.get(0);
		else
			return null;
	}

	public Page<BetTicket> getPageNo(Integer pageNumber) {
		PageRequest req = new PageRequest(pageNumber - 1, PAGE_SIZE,
				Sort.Direction.DESC, "betTime");
		return betRepository.findAll(req);
	}

	public Page<BetTicket> getLastFive(String username, Integer pageNumber) {
		PageRequest req = new PageRequest(pageNumber - 1, PAGE_SIZE / 10,
				Sort.Direction.DESC, "betTime");
		return betRepository.findTipsterTickets(username, req);
	}

	public void setTotalOdd(List<BetTicket> tickets) {
		for (int i = 0; i < tickets.size(); i++)
			BetTicket.calculateTotalOdd(tickets.get(i));
	}

	public Page<BetTicket> getPageOdds(int page_number) {
		PageRequest req = new PageRequest(page_number - 1, PAGE_SIZE,
				Sort.Direction.DESC, "odd");
		return betRepository.findAll(req);
	}

	public Page<BetTicket> getPageByVotes(int page_number) {
		PageRequest req = new PageRequest(page_number - 1, PAGE_SIZE,
				Sort.Direction.DESC, "vnumber");
		return betRepository.findAll(req);
	}

	public Page<BetTicket> getWinnerTicketsPage(int page_number) {
		PageRequest req = new PageRequest(page_number - 1, PAGE_SIZE,
				Sort.Direction.DESC, "betTime");
		return betRepository.findWinnerTickets(req);
	}

	@Scheduled(cron = "*/100 * * * * ?")
	public void validateTickets() {
		Set<BetTicket> tickets = betRepository.findUnvalidatedMatches();
		for (BetTicket bt : tickets) {
			bt.setValidated(checkTicketMatches(bt));
			if (checkTicketMatches(bt) == 1) {
				Tipster tipster = bt.getTipster();
				tipster.setBankroll(bt.getStake() * bt.getTotal_odd()
						+ tipster.getBankroll());
				tipsterRepository.save(tipster);
			}

			betRepository.save(bt);
		}
	}

	private int checkTicketMatches(BetTicket ticket) {
		Set<MatchBet> bets = ticket.getMatchBets();
		int winner = 1;
		int validated = 0;
		for (MatchBet mb : bets) {
			if (mb.getWinner() == 0) {
				winner = 0;
			} else if (mb.getWinner() == -1) {
				validated = -1;
			}
		}

		if (validated == -1)
			return validated;
		else
			return winner;
	}

	public Page<BetTicket> getTipsterTickets(int page_number, String username) {
		PageRequest req = new PageRequest(page_number - 1, PAGE_SIZE / 10,
				Sort.Direction.DESC, "betTime");
		return betRepository.findTipsterTickets(username, req);
	}

	public Page<BetTicket> getTicketsWithMinOdd(int page_number, int odd) {
		PageRequest req = new PageRequest(page_number - 1, PAGE_SIZE / 10,
				Sort.Direction.DESC, "betTime");
		double d_odd = (double) odd;
		return betRepository.getTicketsWithMinOdd(d_odd, req);
	}

	public Page<BetTicket> getTicketsWithMaxOdd(int page_number, int odd) {
		PageRequest req = new PageRequest(page_number - 1, PAGE_SIZE / 10,
				Sort.Direction.DESC, "betTime");
		double d_odd = (double) odd;
		return betRepository.getTicketsWithMaxOdd(d_odd, req);
	}

	public Page<BetTicket> getTopTipstersTickets(int page_number) {
		PageRequest req = new PageRequest(page_number - 1, PAGE_SIZE / 10,
				Sort.Direction.DESC, "bankroll");
		return betRepository.getTopTipstersTickets(req);
	}

	public String findTipsterReport(String username) {
		int winner_tickets = betRepository.getTipsterWinnerTicketsNo(username);
		int total_tickets = betRepository.getTipsterTotalTicketsNo(username);
		return winner_tickets + "/" + total_tickets;
	}

	public Page<BetTicket> getDaysTickets(int page_number) {
		PageRequest req = new PageRequest(page_number - 1, PAGE_SIZE / 10,
				Sort.Direction.DESC, "betTime");
		return betRepository.getDaysTickets(req);
	}
}
