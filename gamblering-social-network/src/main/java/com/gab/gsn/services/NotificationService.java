package com.gab.gsn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gab.gsn.entity.Notification;
import com.gab.gsn.repository.NotificationRepository;

@Service
public class NotificationService {
	
	@Autowired
	private NotificationRepository notificationRepository;
	
	public void saveNotification(Notification notification){
		notificationRepository.save(notification);
	}

	public List<Notification> findTipsterNotifications(String username) {
		return notificationRepository.findTipsterNotifications(username);
	}
}
