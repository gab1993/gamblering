package com.gab.gsn.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gab.gsn.entity.Tipster;
import com.gab.gsn.repository.RoleRepository;
import com.gab.gsn.entity.Role;

@Service
public class RoleService {
	
	@Autowired
	private RoleRepository roleRepository;
	
	public void setTipsterRole(Tipster tipster, String rolename) {
		Role role = roleRepository.findOneByName(rolename);
		List<Role> roles = new ArrayList<Role>();
		roles.add(role);
		tipster.setRoles(roles);
	}
	
	public Role getUserRole(String username){
		List<Role> user_roles = roleRepository.getUserRole(username);
		Collections.sort(user_roles);
		Role user_role = user_roles.get(user_roles.size()-1);
		return user_role;
	}

}
