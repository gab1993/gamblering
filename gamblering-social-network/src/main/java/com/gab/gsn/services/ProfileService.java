package com.gab.gsn.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gab.gsn.entity.Notification;
import com.gab.gsn.entity.Profile;
import com.gab.gsn.entity.Tipster;
import com.gab.gsn.exceptions.FollowingException;
import com.gab.gsn.repository.ProfileRepository;

@Service
public class ProfileService {
	
	@Autowired
	private ProfileRepository profileRepository;
	
	@Autowired
	private TipsterService tipsterService;
	
	@Autowired
	private NotificationService notificationService;
	
	public Profile findOneByUsername(String username){
		return profileRepository.findOne(username);
	}
	
	public int getProfileFollowersNumber(String username){
		int no = profileRepository.getFollowersNumber(username);
		return no;
	}
	
	public short isFollowedBy(String follower, Profile tipster_profile){
		List<Profile> profile_followers = profileRepository.findOne(follower).getFollowers();
		if(profile_followers.contains(tipster_profile))
			return 1;
		else return 0;
	}
	public void createProfileForUser(Tipster tipster){
		Profile tipster_profile = new Profile();
		tipster_profile.setUsername(tipster.getUsername());
		tipster_profile.setTipster(tipster);
		profileRepository.save(tipster_profile);
	}
	
	public void updateProfile(Profile profile){
		Profile p = profileRepository.findOne(profile.getUsername());
		p = profile;
		profileRepository.save(p);
	}

	public void modifyAndUpdate(Profile profile, String username) {
		Profile auth_profile = profileRepository.findOne(username);
		auth_profile.setFirst_name(profile.getFirst_name());
		auth_profile.setSecond_name(profile.getSecond_name());
		auth_profile.setGender(profile.getGender());
		auth_profile.setBirthdate(profile.getBirthdate());
		auth_profile.setBetagency(profile.getBetagency());
		auth_profile.setAvatar_url(profile.getAvatar_url());
		auth_profile.setCountry(profile.getCountry());
		auth_profile.setCity(profile.getCity());
		auth_profile.setDescription(profile.getDescription());
		auth_profile.setUpdated(1);
		profileRepository.save(auth_profile);
	}

	public void linkProfiles(String follower, String followed) throws FollowingException{
		java.util.Date date = new Date();
		
		Profile follower_profile = profileRepository.findOne(follower);
		Profile followed_profile = profileRepository.findOne(followed);
		
		List<Profile> followers;
		if(follower_profile.getFollowers() == null)
			followers = new ArrayList<Profile>();
		else 
			followers = follower_profile.getFollowers();
		
		if(follower_profile.getFollowers() != null)
			if(follower_profile.getFollowers().contains(followed_profile))
				throw new FollowingException("Puteti urmari o singura data o persoana");
		
		followers.add(followed_profile);
		follower_profile.setFollowers(followers);
		profileRepository.saveAndFlush(follower_profile);
		
		Notification notification = new Notification();
		notification.setMessage(follower + " te urmareste.");
		notification.setTipster(tipsterService.findOneByUsername(followed));
		notification.setDate(date);
		notificationService.saveNotification(notification);
	}

	public void unlinkProfiles(String follower, String followed) throws FollowingException{
		Profile follower_profile = profileRepository.findOne(follower);
		Profile followed_profile = profileRepository.findOne(followed);
		
		List<Profile> followers;
		if(follower_profile.getFollowers() == null)
			followers = new ArrayList<Profile>();
		else 
			followers = follower_profile.getFollowers();
		
		if(follower_profile.getFollowers() != null)
			if(!follower_profile.getFollowers().contains(followed_profile))
				throw new FollowingException("Nu urmariti aceasta persoana");
		
		followers.remove(followed_profile);
		follower_profile.setFollowers(followers);
		profileRepository.saveAndFlush(follower_profile);
	}

	public List<Tipster> extractProfilesTipsters(List<Profile> profiles) {
		ArrayList<Tipster> tipsters = new ArrayList<Tipster>();
		for(int i = 0; i < profiles.size(); i++)
			tipsters.add(tipsterService.findOneByUsername(profiles.get(i).getUsername()));
		return tipsters;
	}

	public int getProfileProcent(String username) {
		Profile profile = profileRepository.findProfileByUsername(username);
		int fields_no = 0;
		
		if(profile.getBetagency() != null) fields_no ++;
		if(profile.getBirthdate() != null) fields_no ++;
		if(profile.getCity() != null) fields_no ++;
		if(profile.getCountry() != null) fields_no ++;
		if(profile.getFirst_name() != null) fields_no ++;
		if(profile.getSecond_name() != null) fields_no ++;
		if(profile.getGender() != null) fields_no ++;
		
		return fields_no;
	}
}
	
