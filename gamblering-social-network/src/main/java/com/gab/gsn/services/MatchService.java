package com.gab.gsn.services;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gab.gsn.entity.FirstHalfScore;
import com.gab.gsn.entity.Match;
import com.gab.gsn.entity.MatchBet;
import com.gab.gsn.entity.MatchScore;
import com.gab.gsn.entity.Scores;
import com.gab.gsn.entity.SecondHalfScore;
import com.gab.gsn.repository.FirstHalfScoreRepository;
import com.gab.gsn.repository.MatchBetRepository;
import com.gab.gsn.repository.MatchRepository;
import com.gab.gsn.repository.SecondHalfScoreRepository;
import static com.gab.gsn.util.MatchBetsValidator.*;

@Service
public class MatchService {

	@Autowired
	private MatchRepository matchRepository;

	@Autowired
	private MatchBetRepository matchBetRepository;

	@Autowired
	private FirstHalfScoreRepository firstHalfRepository;

	@Autowired
	private SecondHalfScoreRepository secondHalfRepository;

	public void saveMatch(Match match) {
		matchRepository.save(match);
	}

	public Set<Match> getLeagueMatches(int league_id) {
		return matchRepository.getLeagueMatches(league_id);
	}

	public Set<Match> getLeagueNoFinishedMatches(int league_id) {
		return matchRepository.getLeaguesMatchesWithNoScore(league_id);
	}

	public void saveMatchOdds(List<MatchBet> odds, Match match) {
		for (MatchBet m : odds) {
			m.setMatch_id(match.getMatch_id());
			m.setWinner(-1);
			matchBetRepository.save(m);
		}
	}

	public Match getMatchFetchBets(long match_id) {
		return matchRepository.getMatchFetchBets(match_id);
	}

	public void validateMatchBets(Match match) {
		Set<MatchBet> bets = match.getMatchBets();
		for (MatchBet bet : bets) {
			if (bet.getBet_name().equals("GG")) {
				bet.setWinner(validateGG(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("NGG")) {
				bet.setWinner(validateGG(match) == (short) 1 ? (short) 0
						: (short) 1);
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("GG3+")) {
				bet.setWinner(validateGGp3(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("GG3-")) {
				bet.setWinner(validateGGp3(match) == (short) 1 ? (short) 0
						: (short) 1);
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("GG4+")) {
				bet.setWinner(validateGGp4(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("GG4-")) {
				bet.setWinner(validateGGp4(match) == (short) 1 ? (short) 0
						: (short) 1);
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("1")) {
				bet.setWinner(validate1(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("X")) {
				bet.setWinner(validateX(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("2")) {
				bet.setWinner(validate2(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("1X")) {
				bet.setWinner(validate1X(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("X2")) {
				bet.setWinner(validateX2(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("12")) {
				bet.setWinner(validate12(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("1+")) {
				bet.setWinner(validate1p(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("2+")) {
				bet.setWinner(validate2p(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("3+")) {
				bet.setWinner(validate3p(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("4+")) {
				bet.setWinner(validate4p(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("5+")) {
				bet.setWinner(validate5p(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("6+")) {
				bet.setWinner(validate6p(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("PR1")) {
				bet.setWinner(validatePR1(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("PRX")) {
				bet.setWinner(validatePRX(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("PR2")) {
				bet.setWinner(validatePR2(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("DR1")) {
				bet.setWinner(validateDR1(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("DRX")) {
				bet.setWinner(validateDRX(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("DR2")) {
				bet.setWinner(validateDR2(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("0-1")) {
				bet.setWinner(validateITG01(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("0-2")) {
				bet.setWinner(validateITG02(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("0-3")) {
				bet.setWinner(validateITG03(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("0-4")) {
				bet.setWinner(validateITG04(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("0-5")) {
				bet.setWinner(validateITG05(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("1-2")) {
				bet.setWinner(validateITG12(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("1-3")) {
				bet.setWinner(validateITG13(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("1-4")) {
				bet.setWinner(validateITG14(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("1-5")) {
				bet.setWinner(validateITG15(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("2-3")) {
				bet.setWinner(validateITG23(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("2-4")) {
				bet.setWinner(validateITG24(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("2-5")) {
				bet.setWinner(validateITG25(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("3-4")) {
				bet.setWinner(validateITG34(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("3-5")) {
				bet.setWinner(validateITG35(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("PSF1")) {
				bet.setWinner(validatePSF1(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("PSF2")) {
				bet.setWinner(validatePSF2(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("PSFX")) {
				bet.setWinner(validatePSFX(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("1/1")) {
				bet.setWinner(validatePF11(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("1/X")) {
				bet.setWinner(validatePF1X(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("1/2")) {
				bet.setWinner(validatePF12(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("X/1")) {
				bet.setWinner(validatePFX1(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("X/X")) {
				bet.setWinner(validatePFXX(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("X/2")) {
				bet.setWinner(validatePFX2(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("2/1")) {
				bet.setWinner(validatePF21(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("2/X")) {
				bet.setWinner(validatePF2X(match));
				matchBetRepository.save(bet);
			}
			if (bet.getBet_name().equals("2/2")) {
				bet.setWinner(validatePF22(match));
				matchBetRepository.save(bet);
			}
		}
	}

	public void saveScores(Scores scores) {
		List<MatchScore> scoresList = scores.getMatches();

		FirstHalfScore fhs = new FirstHalfScore();
		SecondHalfScore shs = new SecondHalfScore();

		Match match;
		for (MatchScore ms : scoresList) {
			match = matchRepository.getMatchFetchBets(ms.getMatch_id());
			fhs = ms.getFirstHalfScore();
			shs = ms.getSecondHalfScore();
			fhs.setMatch(match);
			shs.setMatch(match);
			firstHalfRepository.save(fhs);
			secondHalfRepository.save(shs);
			match.setFirst_half_score(fhs);
			match.setSecond_half_score(shs);
			validateMatchBets(match);
		}
	}

}
