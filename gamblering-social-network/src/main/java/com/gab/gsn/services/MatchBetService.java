package com.gab.gsn.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gab.gsn.entity.MatchBet;
import com.gab.gsn.repository.MatchBetRepository;

@Service
public class MatchBetService {

	@Autowired
	private MatchBetRepository matchBetRepository;
	
	public MatchBet findByMatchAndBet(long match_id, String bet_name){
		return matchBetRepository.findByMatchAndBet(match_id,bet_name);
	}
}
