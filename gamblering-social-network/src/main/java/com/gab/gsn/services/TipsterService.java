package com.gab.gsn.services;

import java.util.Date;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gab.gsn.entity.BetTicket;
import com.gab.gsn.entity.Profile;
import com.gab.gsn.entity.Ticket;
import com.gab.gsn.entity.Tipster;
import com.gab.gsn.exceptions.EmailExistsException;
import com.gab.gsn.exceptions.InsufficientFundsException;
import com.gab.gsn.exceptions.UsernameExistsException;
import com.gab.gsn.repository.ProfileRepository;
import com.gab.gsn.repository.RoleRepository;
import com.gab.gsn.repository.TipsterRepository;

@Service
public class TipsterService {
	
	@Autowired
	private TipsterRepository tipsterRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private ProfileRepository profileRepository;
	
	@Autowired
	private ProfileService profileService;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private BetTicketService betTicketService;
	
	public Tipster findOneByUsername(String username){
		return tipsterRepository.findOneByUsername(username);
	}
	
	@Transactional
	public Tipster findOneByUsernameFetchProfile(String username) {
		Tipster tipster = tipsterRepository.findOneByUsername(username);
		Profile tipster_profile = profileRepository.findOne(username);
		tipster.setProfile(tipster_profile);
		return tipster;
	}
	
	public void registerUser(Tipster tipster)  throws EmailExistsException, UsernameExistsException {
		if(existsEmail(tipster.getEmail()))
			throw new EmailExistsException("Exista deja un cont inregistrat cu email-ul: "+tipster.getEmail());
		
		if(existsUser(tipster.getUsername()))
			throw new UsernameExistsException("Username-ul: "+tipster.getUsername()+" este folosit");
		
		tipster.setActive(1);
		tipster.setBankroll(100);
		roleService.setTipsterRole(tipster, "USER");
		tipsterRepository.save(tipster);
		profileService.createProfileForUser(tipster);
		
	}
	
	public boolean existsEmail(String email){
		Tipster tipster = tipsterRepository.findOneByEmail(email);
		if(tipster == null)
			return false;
		else 
			return true;
	}
	
	public boolean existsUser(String username){
		Tipster tipster = tipsterRepository.findOneByUsername(username);
		if(tipster == null)
			return false;
		else
			return true;
	}

	public void parseTicket(String username, Ticket ticket, double stake, boolean dayticket) throws InsufficientFundsException {
		java.util.Date utilDate = new Date();
		java.sql.Date date = new java.sql.Date(utilDate.getTime());
		
		Tipster tipster = tipsterRepository.findOneByUsername(username);
		
		if(tipster.getBankroll() < stake)
			throw new InsufficientFundsException("Fonduri insuficiente");
		
		BetTicket bt = new BetTicket();
		bt.setMatchBets(ticket.getMatchBets());
		bt.setStake(stake);
		tipster.setBankroll(tipster.getBankroll() - stake);
		bt.setTipster(tipster);
		bt.setBetTime(date);
		bt.setValidated(-1);
		BetTicket.calculateTotalOdd(bt);
		
		if(dayticket == true)
			bt.setDayTicket(1);
		else 
			bt.setDayTicket(0);
		
		betTicketService.save(bt);
		tipsterRepository.save(tipster);
	}

	public Set<Tipster> getTipstersRanking() {
		return tipsterRepository.getTipstersRanking();
	}

	public void getTipstersTotalOdd(Set<Tipster> tipsters) {
		for(Tipster t : tipsters) {
			String t_total_odd = tipsterRepository.getTipsterTotalOdd(t.getUsername());
			if(t_total_odd == null)
				t.setTotal_odd("0.0");
			else t.setTotal_odd(t_total_odd);
			t.setStatus(betTicketService.findTipsterReport(t.getUsername()));
		}
	}

	public void addFundsToAccount(String username, double funds) {
		Tipster tipster = tipsterRepository.findOneByUsername(username);
		tipster.setBankroll(tipster.getBankroll() + funds);
		tipsterRepository.save(tipster);
	}

}
