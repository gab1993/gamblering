package com.gab.gsn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;




import com.gab.gsn.entity.League;
import com.gab.gsn.entity.Team;
import com.gab.gsn.repository.LeagueRepository;
import com.gab.gsn.repository.TeamRepository;

@Service
public class TeamService {

	@Autowired
	private TeamRepository teamRepository;
	
	@Autowired
	private LeagueService leagueService;
	
	@Autowired 
	private LeagueRepository leagueRepository;
	
	public void save(Team team){
		teamRepository.saveAndFlush(team);
	}

	public void saveNewTeamInLeague(Team team) {
		int league_id = team.getLeague_id();
		League league = leagueService.findOne(league_id);	
		List<Team> league_teams = leagueService.findLeagueTeams(league_id);
		league_teams.add(team);
		league.setTeams(league_teams);
		
		leagueRepository.save(league);
		teamRepository.save(team);
	}
}
