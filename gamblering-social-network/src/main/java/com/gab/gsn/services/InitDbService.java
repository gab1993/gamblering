package com.gab.gsn.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.gab.gsn.repository.ProfileRepository;
import com.gab.gsn.repository.RoleRepository;
import com.gab.gsn.repository.TipsterRepository;

@Transactional
@Service
public class InitDbService {
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private TipsterRepository tipsterRepository;
	
	@Autowired
	private ProfileRepository profileRepository;
	
}
