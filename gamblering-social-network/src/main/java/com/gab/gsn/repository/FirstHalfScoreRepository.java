package com.gab.gsn.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gab.gsn.entity.FirstHalfScore;

public interface FirstHalfScoreRepository extends JpaRepository<FirstHalfScore, Long> {

}
