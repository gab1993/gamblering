package com.gab.gsn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gab.gsn.entity.Profile;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, String> {
	@Query("SELECT COUNT(p) FROM Profile p JOIN p.followers f where f.username = :username ")
	int getFollowersNumber(@Param("username") String username);
	
	@Query("SELECT p FROM Profile p WHERE p.username = :username")
	Profile findProfileByUsername(@Param("username") String username);
}
