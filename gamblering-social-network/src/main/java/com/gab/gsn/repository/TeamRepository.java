package com.gab.gsn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gab.gsn.entity.Team;

@Repository
public interface TeamRepository extends JpaRepository<Team, Integer> {

}
