package com.gab.gsn.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gab.gsn.entity.Role;

public interface RoleRepository extends JpaRepository<Role,Long>{
	
	@Query("SELECT r FROM Role r WHERE r.role = :rolename")
	Role findOneByName(@Param("rolename") String rolename);

	@Query("SELECT t.roles FROM Tipster t WHERE t.username = :username")
	ArrayList<Role> getUserRole(@Param("username") String username);
	
	/*
	@Query("SELECT r FROM Role r JOIN r.tipsters t WHERE t.username = :username")
	ArrayList<Role> getUserRole(@Param("username") String username);
	*/
	
	
}
