package com.gab.gsn.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gab.gsn.entity.BetTicket;

public interface BetTicketRepository extends JpaRepository<BetTicket, Long> {
	
	@Query("SELECT bt FROM BetTicket bt JOIN FETCH bt.matchBets WHERE bt.tipster.username = :username"
			+ " ORDER BY bt.betTime DESC")
	List<BetTicket> findLastTipsterTicket(@Param("username") String username);
	
	@Query("SELECT bt FROM BetTicket bt WHERE bt.tipster.username = :username")
	Page<BetTicket> findTipsterTickets(@Param("username") String username, Pageable req);
	
	@Query("SELECT count(bt) FROM BetTicket bt WHERE bt.tipster.username = :username AND validated = 1")
	int getTipsterWinnerTicketsNo(@Param("username") String username);
	
	@Query("SELECT count(bt) FROM BetTicket bt WHERE bt.tipster.username = :username")
	int getTipsterTotalTicketsNo(@Param("username") String username);
	
	@Query("SELECT bt FROM BetTicket bt JOIN FETCH bt.votes WHERE bt.bet_id = :bet_id")
	BetTicket findTicketVotesFetched(@Param("bet_id") long bet_id);
	
	@Query("SELECT bt FROM BetTicket bt JOIN FETCH bt.matchBets WHERE bt.validated = -1")
	Set<BetTicket> findUnvalidatedMatches();
	
	@Query("SELECT bt FROM BetTicket bt WHERE bt.validated = 1")
	Page<BetTicket> findWinnerTickets(Pageable req);
	
	@Query("SELECT bt FROM BetTicket bt WHERE bt.odd >= :odd")
	Page<BetTicket> getTicketsWithMinOdd(@Param("odd") double odd, Pageable req);
	
	@Query("SELECT bt FROM BetTicket bt WHERE bt.odd <= :odd")
	Page<BetTicket> getTicketsWithMaxOdd(@Param("odd") double odd, Pageable req);
	
	@Query("SELECT bt FROM BetTicket bt ORDER BY bt.tipster.bankroll DESC")
	Page<BetTicket> getTopTipstersTickets(Pageable req);
	
	@Query("SELECT bt FROM BetTicket bt WHERE bt.dayTicket = 1")
	Page<BetTicket> getDaysTickets(Pageable req);
}
