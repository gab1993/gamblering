package com.gab.gsn.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gab.gsn.entity.SecondHalfScore;

public interface SecondHalfScoreRepository extends JpaRepository<SecondHalfScore, Long>{

}
