package com.gab.gsn.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gab.gsn.entity.League;
import com.gab.gsn.entity.Team;

@Repository
public interface LeagueRepository extends JpaRepository<League, Integer> {
	
	@Query("SELECT l.teams FROM League l WHERE l.league_id = :league_id")
	List<Team> findLeagueTeams(@Param("league_id") int league_id);
	
	@Query("SELECT DISTINCT l.league_country FROM League l")
	List<String> findAllCountries();
	
	@Query("SELECT l FROM League l WHERE l.league_country = :country")
	List<League> findLeaguesByCountry(@Param("country") String country);
}
