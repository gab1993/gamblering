package com.gab.gsn.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.gab.gsn.entity.BetType;

public interface BetTypeRepository extends JpaRepository<BetType, String> {
	
	@Query("SELECT bt FROM BetType bt ORDER BY bt.bet_category")
	List<BetType> findAllBetTypes();
}
