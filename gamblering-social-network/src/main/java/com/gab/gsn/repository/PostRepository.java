package com.gab.gsn.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.gab.gsn.entity.Post;

public interface PostRepository extends JpaRepository<Post, Long> {
	
	@Query("SELECT p FROM Post p")
	Page<Post> getAllPostsByDate(Pageable req);
	
	@Query("SELECT p FROM Post p ORDER BY p.post_date DESC")
	List<Post> findAllAndSortByDate();

}
