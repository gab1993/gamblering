package com.gab.gsn.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gab.gsn.entity.Tipster;

public interface TipsterRepository extends JpaRepository<Tipster,Long> {
	
	@Query("SELECT t FROM Tipster t WHERE t.username = :username")
	Tipster findOneByUsername(@Param("username") String username);
	
	@Query("SELECT t FROM Tipster t JOIN FETCH t.profile WHERE t.username = :username")
	Tipster findOneByUsernameEagerProfile(@Param("username") String username);
	
	@Query("SELECT t FROM Tipster t WHERE t.email = :email")
	Tipster findOneByEmail(@Param("email") String email);
	
	@Query("SELECT t FROM Tipster t ORDER BY bankroll DESC")
	Set<Tipster> getTipstersRanking();
	
	@Query("SELECT sum(bt.odd) FROM BetTicket bt WHERE bt.tipster.username = :username AND bt.validated = 1")
	String getTipsterTotalOdd(@Param("username") String username);

}
