package com.gab.gsn.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gab.gsn.entity.Match;

public interface MatchRepository extends JpaRepository<Match,Long> {
	
	@Query("SELECT m FROM Match m JOIN FETCH m.matchBets mb WHERE m.league_id = :league_id AND "+
	" m.match_time > trunc(sysdate) AND mb.match_id = m.match_id")
	Set<Match> getLeagueMatches(@Param("league_id") int league_id);
	
	@Query("SELECT m "
		+ "FROM Match m "
		+ "LEFT JOIN m.first_half_score fs "
		+ "LEFT JOIN m.second_half_score hs "
		+ "WHERE m.league_id = :league_id AND fs IS NULL AND hs IS NULL")
	Set<Match> getLeaguesMatchesWithNoScore(@Param("league_id") int league_id);
	
	@Query("SELECT m FROM Match m JOIN FETCH m.matchBets mb WHERE m.match_id = :match_id")
	Match getMatchFetchBets(@Param("match_id") long match_id);
	
}
