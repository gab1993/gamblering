package com.gab.gsn.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gab.gsn.entity.Notification;

public interface NotificationRepository extends
		JpaRepository<Notification, Long> {
	
	@Query("SELECT n FROM Notification n WHERE n.tipster.username =:username") // mysql trunc(notif_date)=sysdatel
	List<Notification> findTipsterNotifications(@Param("username") String username);

}
