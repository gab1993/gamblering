package com.gab.gsn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gab.gsn.entity.MatchBet;
import com.gab.gsn.entity.MatchBetPK;

public interface MatchBetRepository extends JpaRepository<MatchBet, MatchBetPK> {
	
	@Query("SELECT mb FROM MatchBet mb WHERE mb.match_id = :match_id AND mb.bet_name = :bet_name")
	MatchBet findByMatchAndBet(@Param("match_id") long match_id, @Param("bet_name") String bet_name);

}
