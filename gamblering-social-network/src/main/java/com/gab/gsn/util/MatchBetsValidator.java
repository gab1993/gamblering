package com.gab.gsn.util;

import com.gab.gsn.entity.FirstHalfScore;
import com.gab.gsn.entity.Match;
import com.gab.gsn.entity.SecondHalfScore;

public class MatchBetsValidator {
	
	public static int homeTeamGoals(FirstHalfScore fhs, SecondHalfScore shs){
		return fhs.getHome_team_goals() + shs.getHome_team_goals();
	}
	
	public static int awayTeamGoals(FirstHalfScore fhs, SecondHalfScore shs){
		return fhs.getAway_team_goals() + shs.getAway_team_goals();
	}
	
	public static int totalGoals(FirstHalfScore fhs, SecondHalfScore shs){
		return fhs.getHome_team_goals() + fhs.getAway_team_goals() + shs.getHome_team_goals() + shs.getAway_team_goals();
	}
	
	public static short validatePF22(Match match) {
		int hteam_r1_goals = match.getFirst_half_score().getHome_team_goals();
		int hteam_t_goals = match.getSecond_half_score().getHome_team_goals()
				+ hteam_r1_goals;

		int ateam_r1_goals = match.getFirst_half_score().getAway_team_goals();
		int ateam_t_goals = match.getSecond_half_score().getAway_team_goals()
				+ ateam_r1_goals;

		if (hteam_r1_goals < ateam_r1_goals && hteam_t_goals < ateam_t_goals)
			return 1;
		else
			return 0;
	}
	
	public static short validatePF2X(Match match) {
		int hteam_r1_goals = match.getFirst_half_score().getHome_team_goals();
		int hteam_t_goals = match.getSecond_half_score().getHome_team_goals()
				+ hteam_r1_goals;

		int ateam_r1_goals = match.getFirst_half_score().getAway_team_goals();
		int ateam_t_goals = match.getSecond_half_score().getAway_team_goals()
				+ ateam_r1_goals;

		if (hteam_r1_goals < ateam_r1_goals && hteam_t_goals == ateam_t_goals)
			return 1;
		else
			return 0;
	}
	public static short validatePF21(Match match) {
		int hteam_r1_goals = match.getFirst_half_score().getHome_team_goals();
		int hteam_t_goals = match.getSecond_half_score().getHome_team_goals()
				+ hteam_r1_goals;

		int ateam_r1_goals = match.getFirst_half_score().getAway_team_goals();
		int ateam_t_goals = match.getSecond_half_score().getAway_team_goals()
				+ ateam_r1_goals;

		if (hteam_r1_goals < ateam_r1_goals && hteam_t_goals > ateam_t_goals)
			return 1;
		else
			return 0;
	}
	public static short validatePFX2(Match match) {
		int hteam_r1_goals = match.getFirst_half_score().getHome_team_goals();
		int hteam_t_goals = match.getSecond_half_score().getHome_team_goals()
				+ hteam_r1_goals;

		int ateam_r1_goals = match.getFirst_half_score().getAway_team_goals();
		int ateam_t_goals = match.getSecond_half_score().getAway_team_goals()
				+ ateam_r1_goals;

		if (hteam_r1_goals == ateam_r1_goals && hteam_t_goals < ateam_t_goals)
			return 1;
		else
			return 0;
	}
	public static short validatePFXX(Match match) {
		int hteam_r1_goals = match.getFirst_half_score().getHome_team_goals();
		int hteam_t_goals = match.getSecond_half_score().getHome_team_goals()
				+ hteam_r1_goals;

		int ateam_r1_goals = match.getFirst_half_score().getAway_team_goals();
		int ateam_t_goals = match.getSecond_half_score().getAway_team_goals()
				+ ateam_r1_goals;

		if (hteam_r1_goals == ateam_r1_goals && hteam_t_goals == ateam_t_goals)
			return 1;
		else
			return 0;
	}
	public static short validatePFX1(Match match) {
		int hteam_r1_goals = match.getFirst_half_score().getHome_team_goals();
		int hteam_t_goals = match.getSecond_half_score().getHome_team_goals()
				+ hteam_r1_goals;

		int ateam_r1_goals = match.getFirst_half_score().getAway_team_goals();
		int ateam_t_goals = match.getSecond_half_score().getAway_team_goals()
				+ ateam_r1_goals;

		if (hteam_r1_goals == ateam_r1_goals && hteam_t_goals > ateam_t_goals)
			return 1;
		else
			return 0;
	}
	public static short validatePF12(Match match) {
		int hteam_r1_goals = match.getFirst_half_score().getHome_team_goals();
		int hteam_t_goals = match.getSecond_half_score().getHome_team_goals()
				+ hteam_r1_goals;

		int ateam_r1_goals = match.getFirst_half_score().getAway_team_goals();
		int ateam_t_goals = match.getSecond_half_score().getAway_team_goals()
				+ ateam_r1_goals;

		if (hteam_r1_goals > ateam_r1_goals && hteam_t_goals < ateam_t_goals)
			return 1;
		else
			return 0;
	}
	public static short validatePF1X(Match match) {
		int hteam_r1_goals = match.getFirst_half_score().getHome_team_goals();
		int hteam_t_goals = match.getSecond_half_score().getHome_team_goals()
				+ hteam_r1_goals;

		int ateam_r1_goals = match.getFirst_half_score().getAway_team_goals();
		int ateam_t_goals = match.getSecond_half_score().getAway_team_goals()
				+ ateam_r1_goals;

		if (hteam_r1_goals > ateam_r1_goals && hteam_t_goals == ateam_t_goals)
			return 1;
		else
			return 0;
	}
	public static short validatePF11(Match match) {
		int hteam_r1_goals = match.getFirst_half_score().getHome_team_goals();
		int hteam_t_goals = match.getSecond_half_score().getHome_team_goals()
				+ hteam_r1_goals;

		int ateam_r1_goals = match.getFirst_half_score().getAway_team_goals();
		int ateam_t_goals = match.getSecond_half_score().getAway_team_goals()
				+ ateam_r1_goals;

		if (hteam_r1_goals > ateam_r1_goals && hteam_t_goals > ateam_t_goals)
			return 1;
		else
			return 0;
	}

	public static short validatePSFX(Match match) {
		int hteam_r1_goals = match.getFirst_half_score().getHome_team_goals();
		int hteam_t_goals = match.getSecond_half_score().getHome_team_goals()
				+ hteam_r1_goals;

		int ateam_r1_goals = match.getFirst_half_score().getAway_team_goals();
		int ateam_t_goals = match.getSecond_half_score().getAway_team_goals()
				+ ateam_r1_goals;

		if (hteam_r1_goals == ateam_r1_goals || hteam_t_goals == ateam_t_goals)
			return 1;
		else
			return 0;
	}

	public static short validatePSF2(Match match) {
		int hteam_r1_goals = match.getFirst_half_score().getHome_team_goals();
		int hteam_t_goals = match.getSecond_half_score().getHome_team_goals()
				+ hteam_r1_goals;

		int ateam_r1_goals = match.getFirst_half_score().getAway_team_goals();
		int ateam_t_goals = match.getSecond_half_score().getAway_team_goals()
				+ ateam_r1_goals;

		if (hteam_r1_goals < ateam_r1_goals || hteam_t_goals < ateam_t_goals)
			return 1;
		else
			return 0;
	}

	public static short validatePSF1(Match match) {
		int hteam_r1_goals = match.getFirst_half_score().getHome_team_goals();
		int hteam_t_goals = match.getSecond_half_score().getHome_team_goals()
				+ hteam_r1_goals;

		int ateam_r1_goals = match.getFirst_half_score().getAway_team_goals();
		int ateam_t_goals = match.getSecond_half_score().getAway_team_goals()
				+ ateam_r1_goals;

		if (hteam_r1_goals > ateam_r1_goals || hteam_t_goals > ateam_t_goals)
			return 1;
		else
			return 0;
	}

	public static short validateITG35(Match match) {
		int total_goals = MatchBetsValidator.totalGoals(
				match.getFirst_half_score(), match.getSecond_half_score());
		if (total_goals >= 3 && total_goals <= 5)
			return 1;
		else
			return 0;
	}

	public static short validateITG34(Match match) {
		int total_goals = MatchBetsValidator.totalGoals(
				match.getFirst_half_score(), match.getSecond_half_score());
		if (total_goals >= 3 && total_goals <= 4)
			return 1;
		else
			return 0;
	}

	public static short validateITG25(Match match) {
		int total_goals = MatchBetsValidator.totalGoals(
				match.getFirst_half_score(), match.getSecond_half_score());
		if (total_goals >= 2 && total_goals <= 5)
			return 1;
		else
			return 0;
	}

	public static short validateITG24(Match match) {
		int total_goals = MatchBetsValidator.totalGoals(
				match.getFirst_half_score(), match.getSecond_half_score());
		if (total_goals >= 2 && total_goals <= 4)
			return 1;
		else
			return 0;
	}

	public static short validateITG23(Match match) {
		int total_goals = MatchBetsValidator.totalGoals(
				match.getFirst_half_score(), match.getSecond_half_score());
		if (total_goals >= 2 && total_goals <= 3)
			return 1;
		else
			return 0;
	}

	public static short validateITG15(Match match) {
		int total_goals = MatchBetsValidator.totalGoals(
				match.getFirst_half_score(), match.getSecond_half_score());
		if (total_goals >= 1 && total_goals <= 5)
			return 1;
		else
			return 0;
	}

	public static short validateITG14(Match match) {
		int total_goals = MatchBetsValidator.totalGoals(
				match.getFirst_half_score(), match.getSecond_half_score());
		if (total_goals >= 1 && total_goals <= 4)
			return 1;
		else
			return 0;
	}

	public static short validateITG13(Match match) {
		int total_goals = MatchBetsValidator.totalGoals(
				match.getFirst_half_score(), match.getSecond_half_score());
		if (total_goals >= 1 && total_goals <= 3)
			return 1;
		else
			return 0;
	}

	public static short validateITG12(Match match) {
		int total_goals = MatchBetsValidator.totalGoals(
				match.getFirst_half_score(), match.getSecond_half_score());
		if (total_goals >= 1 && total_goals <= 2)
			return 1;
		else
			return 0;
	}

	public static short validateITG05(Match match) {
		int total_goals = MatchBetsValidator.totalGoals(
				match.getFirst_half_score(), match.getSecond_half_score());
		if (total_goals >= 0 && total_goals <= 5)
			return 1;
		else
			return 0;
	}

	public static short validateITG04(Match match) {
		int total_goals = MatchBetsValidator.totalGoals(
				match.getFirst_half_score(), match.getSecond_half_score());
		if (total_goals >= 0 && total_goals <= 4)
			return 1;
		else
			return 0;
	}

	public static short validateITG03(Match match) {
		int total_goals = MatchBetsValidator.totalGoals(
				match.getFirst_half_score(), match.getSecond_half_score());
		if (total_goals >= 0 && total_goals <= 3)
			return 1;
		else
			return 0;
	}

	public static short validateITG02(Match match) {
		int total_goals = MatchBetsValidator.totalGoals(
				match.getFirst_half_score(), match.getSecond_half_score());
		if (total_goals >= 0 && total_goals <= 2)
			return 1;
		else
			return 0;
	}

	public static short validateITG01(Match match) {
		int total_goals = MatchBetsValidator.totalGoals(
				match.getFirst_half_score(), match.getSecond_half_score());
		if (total_goals >= 0 && total_goals <= 1)
			return 1;
		else
			return 0;
	}

	public static short validateDR2(Match match) {
		if (match.getSecond_half_score().getHome_team_goals() < match
				.getSecond_half_score().getAway_team_goals())
			return 1;
		else
			return 0;
	}

	public static short validateDRX(Match match) {
		if (match.getSecond_half_score().getHome_team_goals() == match
				.getSecond_half_score().getAway_team_goals())
			return 1;
		else
			return 0;
	}

	public static short validateDR1(Match match) {
		if (match.getSecond_half_score().getHome_team_goals() > match
				.getSecond_half_score().getAway_team_goals())
			return 1;
		else
			return 0;
	}

	public static short validatePR2(Match match) {
		if (match.getFirst_half_score().getAway_team_goals() > match
				.getFirst_half_score().getHome_team_goals())
			return 1;
		else
			return 0;
	}

	public static short validatePRX(Match match) {
		if (match.getFirst_half_score().getAway_team_goals() == match
				.getFirst_half_score().getHome_team_goals())
			return 1;
		else
			return 0;
	}

	public static short validatePR1(Match match) {
		if (match.getFirst_half_score().getAway_team_goals() < match
				.getFirst_half_score().getHome_team_goals())
			return 1;
		else
			return 0;
	}

	public static short validate12(Match match) {
		if (MatchBetsValidator.homeTeamGoals(match.getFirst_half_score(),
				match.getSecond_half_score()) != MatchBetsValidator
				.awayTeamGoals(match.getFirst_half_score(),
						match.getSecond_half_score()))
			return 1;
		else
			return 0;
	}

	public static short validateX2(Match match) {
		if (MatchBetsValidator.homeTeamGoals(match.getFirst_half_score(),
				match.getSecond_half_score()) <= MatchBetsValidator
				.awayTeamGoals(match.getFirst_half_score(),
						match.getSecond_half_score()))
			return 1;
		else
			return 0;
	}

	public static short validate1X(Match match) {
		if (MatchBetsValidator.homeTeamGoals(match.getFirst_half_score(),
				match.getSecond_half_score()) >= MatchBetsValidator
				.awayTeamGoals(match.getFirst_half_score(),
						match.getSecond_half_score()))
			return 1;
		else
			return 0;
	}

	public static short validate6p(Match match) {
		if (MatchBetsValidator.totalGoals(match.getFirst_half_score(),
				match.getSecond_half_score()) > 6)
			return 1;
		else
			return 0;
	}

	public static short validate5p(Match match) {
		if (MatchBetsValidator.totalGoals(match.getFirst_half_score(),
				match.getSecond_half_score()) > 5)
			return 1;
		else
			return 0;
	}

	public static short validate4p(Match match) {
		if (MatchBetsValidator.totalGoals(match.getFirst_half_score(),
				match.getSecond_half_score()) > 4)
			return 1;
		else
			return 0;
	}

	public static short validate3p(Match match) {
		if (MatchBetsValidator.totalGoals(match.getFirst_half_score(),
				match.getSecond_half_score()) > 3)
			return 1;
		else
			return 0;
	}

	public static short validate2p(Match match) {
		if (MatchBetsValidator.totalGoals(match.getFirst_half_score(),
				match.getSecond_half_score()) > 2)
			return 1;
		else
			return 0;
	}

	public static short validate1p(Match match) {
		if (MatchBetsValidator.totalGoals(match.getFirst_half_score(),
				match.getSecond_half_score()) > 1)
			return 1;
		else
			return 0;
	}

	public static short validate2(Match match) {
		if (MatchBetsValidator.homeTeamGoals(match.getFirst_half_score(),
				match.getSecond_half_score()) < MatchBetsValidator
				.awayTeamGoals(match.getFirst_half_score(),
						match.getSecond_half_score()))
			return 1;
		else
			return 0;
	}

	public static short validateX(Match match) {
		if (MatchBetsValidator.homeTeamGoals(match.getFirst_half_score(),
				match.getSecond_half_score()) == MatchBetsValidator
				.awayTeamGoals(match.getFirst_half_score(),
						match.getSecond_half_score()))
			return 1;
		else
			return 0;
	}

	public static short validate1(Match match) {
		if (MatchBetsValidator.homeTeamGoals(match.getFirst_half_score(),
				match.getSecond_half_score()) > MatchBetsValidator
				.awayTeamGoals(match.getFirst_half_score(),
						match.getSecond_half_score()))
			return 1;
		else
			return 0;
	}

	public static short validateGGp4(Match match) {
		if (validateGG(match) == 0)
			return 0;
		else if (MatchBetsValidator.totalGoals(match.getFirst_half_score(),
				match.getSecond_half_score()) >= 4)
			return 1;
		else
			return 0;
	}

	public static short validateGGp3(Match match) {
		if (validateGG(match) == 0)
			return 0;
		else if (MatchBetsValidator.totalGoals(match.getFirst_half_score(),
				match.getSecond_half_score()) >= 3)
			return 1;
		else
			return 0;
	}

	public static short validateGG(Match match) {
		if (MatchBetsValidator.homeTeamGoals(match.getFirst_half_score(),
				match.getSecond_half_score()) > 0
				&& MatchBetsValidator.awayTeamGoals(
						match.getFirst_half_score(),
						match.getSecond_half_score()) > 0)
			return 1;
		else
			return 0;
	}
}
