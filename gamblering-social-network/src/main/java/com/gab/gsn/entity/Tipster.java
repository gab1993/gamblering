package com.gab.gsn.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="tipster")
public class Tipster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="tipster_id_seq")
    @SequenceGenerator(name="tipster_id_seq", sequenceName="tipster_id_seq")
	@Column(name="tipsterId")
	private Long tipsterId;
	
	@NotNull
	@NotEmpty
	@Column(name="username", unique=true)
	private String username;
	
	@NotNull
	@NotEmpty
	@Column(name="email", unique=true)
	private String email;
	
	@NotNull
	@NotEmpty
	@Column(name="password")
	private String password;
	
	@Transient
	private String match_password;
	
	@Column(name="active")
	private int active;
	
	@Column(name="bankroll")
	private double bankroll;
	
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable
	private List<Role> roles;
	
	@OneToOne(mappedBy = "tipster")
	private Profile profile;
	
	@OneToMany(mappedBy="tipster", cascade = CascadeType.REMOVE)
	private List<Notification> notifications;
	
	@OneToMany(mappedBy="tipster", cascade = CascadeType.REMOVE)
	private List<BetTicket> betTickets;
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "votes")
	private Set<BetTicket> votes;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "author")
	private Set<Post> posts;
	
	@Transient
	private String total_odd;
	
	@Transient
	private String status;
	
	public Set<Post> getPosts() {
		return posts;
	}

	public void setPosts(Set<Post> posts) {
		this.posts = posts;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTotal_odd() {
		return total_odd;
	}

	public void setTotal_odd(String total_odd) {
		this.total_odd = total_odd;
	}

	public Set<BetTicket> getVotes() {
		return votes;
	}

	public void setVotes(Set<BetTicket> votes) {
		this.votes = votes;
	}

	public Long getTipsterId() {
		return tipsterId;
	}

	public void setTipsterId(Long tipsterId) {
		this.tipsterId = tipsterId;
	}
	
	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public String getMatch_password() {
		return match_password;
	}

	public void setMatch_password(String match_password) {
		this.match_password = match_password;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public List<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}
	
	public List<BetTicket> getBetTickets() {
		return betTickets;
	}

	public void setBetTickets(List<BetTicket> betTickets) {
		this.betTickets = betTickets;
	}
	
	public double getBankroll() {
		return bankroll;
	}

	public void setBankroll(double bankroll) {
		this.bankroll = bankroll;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + (int) (tipsterId ^ (tipsterId >>> 32));
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Tipster))
			return false;
		Tipster other = (Tipster) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (tipsterId != other.tipsterId)
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
	
}