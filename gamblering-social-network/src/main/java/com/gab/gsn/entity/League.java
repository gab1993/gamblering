package com.gab.gsn.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="League")
public class League implements Serializable {

	private static final long serialVersionUID = 5793250697752516785L;
	
	@Id
	@Column(name="league_id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="league_id_seq")
    @SequenceGenerator(name="league_id_seq", sequenceName="league_id_seq")
	private int league_id;
	
	@Column(name="league_name")
	private String league_name;
	
	@Column(name="league_country")
	private String league_country;
	
	@ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name="league_team", 
                joinColumns={@JoinColumn(name="league_id")}, 
                inverseJoinColumns={@JoinColumn(name="team_id")})
	private List<Team> teams;
	
	public int getLeague_id() {
		return league_id;
	}

	public void setLeague_id(int league_id) {
		this.league_id = league_id;
	}

	public String getLeague_name() {
		return league_name;
	}

	public void setLeague_name(String league_name) {
		this.league_name = league_name;
	}

	public String getLeague_country() {
		return league_country;
	}

	public void setLeague_country(String league_country) {
		this.league_country = league_country;
	}
	
	public List<Team> getTeams() {
		return teams;
	}

	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((league_country == null) ? 0 : league_country.hashCode());
		result = prime * result + league_id;
		result = prime * result
				+ ((league_name == null) ? 0 : league_name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof League))
			return false;
		League other = (League) obj;
		if (league_country == null) {
			if (other.league_country != null)
				return false;
		} else if (!league_country.equals(other.league_country))
			return false;
		if (league_id != other.league_id)
			return false;
		if (league_name == null) {
			if (other.league_name != null)
				return false;
		} else if (!league_name.equals(other.league_name))
			return false;
		return true;
	}
	
}
