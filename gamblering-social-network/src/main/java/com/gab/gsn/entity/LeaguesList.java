package com.gab.gsn.entity;

import java.util.List;

public class LeaguesList {
	
	private List<League> leaguesList;

	public List<League> getLeaguesList() {
		return leaguesList;
	}

	public void setLeaguesList(List<League> leaguesList) {
		this.leaguesList = leaguesList;
	}
	
	public void addLeague(League league){
		this.leaguesList.add(league);
	}
}
