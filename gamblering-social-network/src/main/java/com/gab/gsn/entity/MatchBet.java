package com.gab.gsn.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "Match_Bet")
@IdClass(MatchBetPK.class)
public class MatchBet implements Serializable {
	private static final long serialVersionUID = 4630327497031076182L;

	@Id
	private long match_id;

	@Id
	private String bet_name;

	@Column(name = "odd")
	private double odd;
	
	@Column(name="winner", columnDefinition = "int default -1")
	private int winner;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "match_id", updatable = false, insertable = false)
	private Match match;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "bet_name", updatable = false, insertable = false)
	private BetType bet_type;
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "matchBets")
	private Set<BetTicket> betTickets;
	
	public long getMatch_id() {
		return match_id;
	}

	public void setMatch_id(long match_id) {
		this.match_id = match_id;
	}

	public String getBet_name() {
		return bet_name;
	}

	public void setBet_name(String bet_name) {
		this.bet_name = bet_name;
	}

	public double getOdd() {
		return odd;
	}

	public void setOdd(double odd) {
		this.odd = odd;
	}

	public Match getMatch() {
		return match;
	}

	public void setMatch(Match match) {
		this.match = match;
	}

	public BetType getBet_type() {
		return bet_type;
	}

	public void setBet_type(BetType bet_type) {
		this.bet_type = bet_type;
	}
	
	public Set<BetTicket> getBetTickets() {
		return betTickets;
	}

	public void setBetTickets(Set<BetTicket> betTickets) {
		this.betTickets = betTickets;
	}
	
	public int getWinner() {
		return winner;
	}

	public void setWinner(int winner) {
		this.winner = winner;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((bet_name == null) ? 0 : bet_name.hashCode());
		result = prime * result + (int) (match_id ^ (match_id >>> 32));
		long temp;
		temp = Double.doubleToLongBits(odd);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof MatchBet))
			return false;
		MatchBet other = (MatchBet) obj;
		if (bet_name == null) {
			if (other.bet_name != null)
				return false;
		} else if (!bet_name.equals(other.bet_name))
			return false;
		if (match_id != other.match_id)
			return false;
		if (Double.doubleToLongBits(odd) != Double.doubleToLongBits(other.odd))
			return false;
		return true;
	}
	
	
}
