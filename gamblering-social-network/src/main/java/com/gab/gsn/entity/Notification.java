package com.gab.gsn.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="Notification")
public class Notification implements Serializable {

	private static final long serialVersionUID = 7825632783550888301L;

	@Id
	@Column(name="notification_id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="notif_id_seq")
    @SequenceGenerator(name="notif_id_seq", sequenceName="notif_id_seq")
	private	long notification_id;
	
	@Column(name="message")
	private String message;
	
	@ManyToOne
	@JoinColumn(name = "username")
	private Tipster tipster;
	
	@Column(name = "notif_date")
	private Date date;
	
	public long getNotification_id() {
		return notification_id;
	}

	public void setNotification_id(long notification_id) {
		this.notification_id = notification_id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Tipster getTipster() {
		return tipster;
	}

	public void setTipster(Tipster tipster) {
		this.tipster = tipster;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ (int) (notification_id ^ (notification_id >>> 32));
		result = prime * result + ((tipster == null) ? 0 : tipster.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Notification))
			return false;
		Notification other = (Notification) obj;
		if (notification_id != other.notification_id)
			return false;
		if (tipster == null) {
			if (other.tipster != null)
				return false;
		} else if (!tipster.equals(other.tipster))
			return false;
		return true;
	}
		
}
