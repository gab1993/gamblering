package com.gab.gsn.entity;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Match implements Serializable{

	private static final long serialVersionUID = -8815543930723251321L;
	
	@Id
	@Column(name="match_id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="match_id_seq")
    @SequenceGenerator(name="match_id_seq", sequenceName="match_id_seq")
	private long match_id;
	
	@ManyToOne
	@JoinColumn(name="home_team_id", referencedColumnName="team_id")
	private Team home_team;
	
	@ManyToOne
	@JoinColumn(name="away_team_id", referencedColumnName="team_id")
	private Team away_team;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name="match_time")
	private Date match_time;
	
	@Transient
	private String match_time_string;
	
	@Column(name="match_hour", nullable=true)
	private String match_hour;
	
	@Column(name="league_id")
	private int league_id;
	
	@OneToMany(mappedBy="match", fetch = FetchType.LAZY)
	private Set<MatchBet> matchBets;
	
	@OneToOne(mappedBy="match")
	private FirstHalfScore first_half_score;
	
	@OneToOne(mappedBy="match")
	private SecondHalfScore second_half_score;
	
	@Transient
	private List<MatchBet> matchBetList;

	public List<MatchBet> getMatchBetList() {
		return matchBetList;
	}

	public void setMatchBetList(List<MatchBet> matchBetList) {
		this.matchBetList = matchBetList;
	}
	
	
	public String getMatch_hour() {
		return match_hour;
	}
	
	public void setMatch_hour(String match_hour) {
		this.match_hour = match_hour;
	}

	public String getMatch_time_string() {
		return match_time_string;
	}

	public void setMatch_time_string(String match_time_string) {
		this.match_time_string = match_time_string;
	}

	public long getMatch_id() {
		return match_id;
	}

	public void setMatch_id(long match_id) {
		this.match_id = match_id;
	}

	public Team getHome_team() {
		return home_team;
	}

	public void setHome_team(Team home_team) {
		this.home_team = home_team;
	}

	public Team getAway_Team() {
		return away_team;
	}

	public void setAway_Team(Team away_Team) {
		this.away_team = away_Team;
	}

	public Team getAway_team() {
		return away_team;
	}

	public void setAway_team(Team away_team) {
		this.away_team = away_team;
	}

	public Date getMatch_time() {
		return match_time;
	}

	public void setMatch_time(Date match_time) {
		this.match_time = match_time;
	}

	public int getLeague_id() {
		return league_id;
	}

	public void setLeague_id(int league_id) {
		this.league_id = league_id;
	}
	
	
	public Set<MatchBet> getMatchBets() {
		return matchBets;
	}

	public void setMatchBets(Set<MatchBet> matchBets) {
		this.matchBets = matchBets;
	}

	public FirstHalfScore getFirst_half_score() {
		return first_half_score;
	}

	public void setFirst_half_score(FirstHalfScore first_half_score) {
		this.first_half_score = first_half_score;
	}

	public SecondHalfScore getSecond_half_score() {
		return second_half_score;
	}

	public void setSecond_half_score(SecondHalfScore second_half_score) {
		this.second_half_score = second_half_score;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((away_team == null) ? 0 : away_team.hashCode());
		result = prime * result
				+ ((home_team == null) ? 0 : home_team.hashCode());
		result = prime * result + league_id;
		result = prime * result + (int) (match_id ^ (match_id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Match))
			return false;
		Match other = (Match) obj;
		if (away_team == null) {
			if (other.away_team != null)
				return false;
		} else if (!away_team.equals(other.away_team))
			return false;
		if (home_team == null) {
			if (other.home_team != null)
				return false;
		} else if (!home_team.equals(other.home_team))
			return false;
		if (league_id != other.league_id)
			return false;
		if (match_id != other.match_id)
			return false;
		return true;
	}

	
}
