package com.gab.gsn.entity;

import java.io.Serializable;

public class MatchBetPK implements Serializable {
	
	private static final long serialVersionUID = -8362887995206365635L;

	private long match_id;
	
	private String bet_name;
	
	public long getMatch_id() {
		return match_id;
	}

	public void setMatch_id(long match_id) {
		this.match_id = match_id;
	}

	public String getBet_name() {
		return bet_name;
	}

	public void setBet_name(String bet_name) {
		this.bet_name = bet_name;
	}

	
}
