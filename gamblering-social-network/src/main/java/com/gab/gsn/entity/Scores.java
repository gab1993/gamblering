package com.gab.gsn.entity;

import java.util.List;

public class Scores {
	
	private List<MatchScore> matches;

	public List<MatchScore> getMatches() {
		return matches;
	}

	public void setMatches(List<MatchScore> matches) {
		this.matches = matches;
	}

	
	
}
