package com.gab.gsn.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

@Entity
public class Team implements Serializable {

	private static final long serialVersionUID = 6416026322054978134L;
	
	@Id
	@Column(name="team_id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="team_id_seq")
    @SequenceGenerator(name="team_id_seq", sequenceName="team_id_seq")
	private int team_id;
	
	@Column(name="team_name")
	private String team_name;
	
	@ManyToMany(mappedBy="teams")
	private List<League> leagues;
	
	@OneToMany(mappedBy="home_team", cascade = CascadeType.REMOVE)
	private List<Match> home_matches;
	
	@OneToMany(mappedBy="away_team", cascade = CascadeType.REMOVE)
	private List<Match> away_matches;
	
	@Transient
	private int league_id;
	
	public int getTeam_id() {
		return team_id;
	}

	public void setTeam_id(int team_id) {
		this.team_id = team_id;
	}
	
	public int getLeague_id() {
		return league_id;
	}

	public void setLeague_id(int league_id) {
		this.league_id = league_id;
	}

	public String getTeam_name() {
		return team_name;
	}

	public void setTeam_name(String team_name) {
		this.team_name = team_name;
	}
	
	public List<League> getLeagues() {
		return leagues;
	}

	public void setLeagues(List<League> leagues) {
		this.leagues = leagues;
	}
	
	public List<Match> getHome_matches() {
		return home_matches;
	}

	public void setHome_matches(List<Match> home_matches) {
		this.home_matches = home_matches;
	}

	public List<Match> getAway_matches() {
		return away_matches;
	}

	public void setAway_matches(List<Match> away_matches) {
		this.away_matches = away_matches;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + team_id;
		result = prime * result
				+ ((team_name == null) ? 0 : team_name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Team))
			return false;
		Team other = (Team) obj;
		if (team_id != other.team_id)
			return false;
		if (team_name == null) {
			if (other.team_name != null)
				return false;
		} else if (!team_name.equals(other.team_name))
			return false;
		return true;
	}
	
}
