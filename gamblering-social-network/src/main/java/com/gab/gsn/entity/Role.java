package com.gab.gsn.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;

@Entity
@Table(name = "role")
public class Role implements Serializable, Comparable<Role> {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="role_id_seq")
    @SequenceGenerator(name="role_id_seq", sequenceName="role_id_seq")
	@Column(name="roleId")
	private Long roleId;

	@Column(name = "role", unique = true)
	private String role;

	@ManyToMany(mappedBy = "roles")
	private List<Tipster> tipsters;
	
	@Column(name = "role_description")
	private String roleDescription;
	
	
	@Column(name="user_power", unique = true)
	private int user_power;
	
	public Long getRoleId() {
		return this.roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public List<Tipster> getTipsters() {
		return tipsters;
	}
	

	public void setTipsters(List<Tipster> tipsters) {
		this.tipsters = tipsters;
	}
	

	public String getRoleDescription() {
		return roleDescription;
	}
	

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}
	
	public int getUser_power() {
		return user_power;
	}

	public void setUser_power(int user_power) {
		this.user_power = user_power;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + (int) (roleId ^ (roleId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Role))
			return false;
		Role other = (Role) obj;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		if (roleId != other.roleId)
			return false;
		return true;
	}

	@Override
	public int compareTo(Role o) {
		if(this.user_power > o.user_power) return 1;
		else if(this.user_power < o.user_power) return -1;
		else return 0;
	}

}