package com.gab.gsn.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="BetType")
public class BetType implements Serializable {

	private static final long serialVersionUID = -2467686286552067061L;
	
	@Id
	@Column(name="bet_name", unique=true)
	private String bet_name;
	
	@Column(name="bet_category")
	private String bet_category;
	
	@OneToMany(mappedBy="bet_type", fetch = FetchType.LAZY)
	private Set<MatchBet> matchBets;
	
	public String getBet_name() {
		return bet_name;
	}

	public void setBet_name(String bet_name) {
		this.bet_name = bet_name;
	}

	public String getBet_category() {
		return bet_category;
	}

	public void setBet_category(String bet_category) {
		this.bet_category = bet_category;
	}
	
	
	public Set<MatchBet> getMatchBets() {
		return matchBets;
	}

	public void setMatchBets(Set<MatchBet> matchBets) {
		this.matchBets = matchBets;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((bet_category == null) ? 0 : bet_category.hashCode());
		result = prime * result
				+ ((bet_name == null) ? 0 : bet_name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof BetType))
			return false;
		BetType other = (BetType) obj;
		if (bet_category == null) {
			if (other.bet_category != null)
				return false;
		} else if (!bet_category.equals(other.bet_category))
			return false;
		if (bet_name == null) {
			if (other.bet_name != null)
				return false;
		} else if (!bet_name.equals(other.bet_name))
			return false;
		return true;
	}
	
	
}
