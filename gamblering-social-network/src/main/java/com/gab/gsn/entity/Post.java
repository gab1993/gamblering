package com.gab.gsn.entity;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "Post")
public class Post implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "post_id")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "post_id_seq")
	@SequenceGenerator(name = "post_id_seq", sequenceName = "post_id_seq")
	private long id;

	@Column(name = "title")
	private String title;
	
	@Column
	private String post_date;
	
	@Column(name = "description")
	private String description;

	@Column(name = "keywords")
	private String keywords;

	@Column(name = "category")
	private String category;

	@Column(name = "img_link")
	private String img_link;

	@Column(name = "link")
	private String link;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipsterId", nullable = false)
	private Tipster author;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getPost_date() {
		return post_date;
	}

	public void setPost_date(String post_date) {
		this.post_date = post_date;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String param) {
		this.title = param;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String param) {
		this.description = param;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String param) {
		this.keywords = param;
	}

	public Tipster getAuthor() {
		return author;
	}

	public void setAuthor(Tipster author) {
		this.author = author;
	}

	public String getImg_link() {
		return img_link;
	}

	public void setImg_link(String img_link) {
		this.img_link = img_link;
	}

}