package com.gab.gsn.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="BetTicket")
public class BetTicket implements Serializable{

	private static final long serialVersionUID = 30153084054687702L;
	
	@Id
	@Column(name="bet_id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="bet_id_seq")
    @SequenceGenerator(name="bet_id_seq", sequenceName="bet_id_seq")
	private long bet_id;
	
	@Column(name="stake")
	private double stake;
	
	@Column(name="betTime")
	private java.sql.Date betTime;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "TicektMatches", joinColumns = { 
			  @JoinColumn(name = "bet_id", nullable = false, updatable = false) }, 
			inverseJoinColumns = { 
					@JoinColumn(name = "match_id", nullable = false, updatable = false),
					@JoinColumn(name = "bet_name", nullable = false, updatable = false)})
	private Set<MatchBet> matchBets;
	
	@ManyToOne
	@JoinColumn(name = "tipster_id")
	private Tipster tipster;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name="Ticket_Votes", joinColumns = {
			@JoinColumn(name= "bet_id", nullable = false, updatable = false)},
			inverseJoinColumns = {
				@JoinColumn(name = "tipster_id", nullable = false, updatable=false)
	})
	private Set<Tipster> votes;
	
	@Column(name="vnumber",nullable = false, columnDefinition = "int default 0")
	private int vnumber;
	
	@Column(name="odd")
	private double odd = 1.0;
	
	@Column(name="validated")
	private int validated;
	
	@Column(name="dayTicket")
	private int dayTicket;
	
	public static void calculateTotalOdd(BetTicket ticket){
		double odd = 1.0;
		for(MatchBet mb : ticket.matchBets)
			odd = mb.getOdd() * odd;
		ticket.setTotal_odd(odd);
	}
	public long getBet_id() {
		return bet_id;
	}

	public void setBet_id(long bet_id) {
		this.bet_id = bet_id;
	}

	public double getStake() {
		return stake;
	}

	public void setStake(double stake) {
		this.stake = stake;
	}
	
	public int getDayTicket() {
		return dayTicket;
	}
	public void setDayTicket(int dayTicket) {
		this.dayTicket = dayTicket;
	}
	public java.sql.Date getBetTime() {
		return betTime;
	}

	public void setBetTime(java.sql.Date betTime) {
		this.betTime = betTime;
	}

	public Set<MatchBet> getMatchBets() {
		return matchBets;
	}
	public void setMatchBets(Set<MatchBet> matchBets) {
		this.matchBets = matchBets;
	}
	
	public Tipster getTipster() {
		return tipster;
	}

	public void setTipster(Tipster tipster) {
		this.tipster = tipster;
	}

	public double getTotal_odd() {
		return odd;
	}

	public void setTotal_odd(double total_odd) {
		this.odd = total_odd;
	}
	public Set<Tipster> getVotes() {
		return votes;
	}
	public void setVotes(Set<Tipster> votes) {
		this.votes = votes;
		setVnumber(votes.size());
	}
	public int getVnumber() {
		return vnumber;
	}
	public void setVnumber(int vnumber) {
		this.vnumber = vnumber;
	}
	public int getValidated() {
		return validated;
	}
	public void setValidated(int validated) {
		this.validated = validated;
	}
	
}
