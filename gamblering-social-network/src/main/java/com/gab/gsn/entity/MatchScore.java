package com.gab.gsn.entity;

public class MatchScore {
	
	private long match_id;
	
	private FirstHalfScore firstHalfScore;
	
	private SecondHalfScore secondHalfScore;
	
	public long getMatch_id() {
		return match_id;
	}
	public void setMatch_id(long match_id) {
		this.match_id = match_id;
	}
	public FirstHalfScore getFirstHalfScore() {
		return firstHalfScore;
	}
	public void setFirstHalfScore(FirstHalfScore firstHalfScore) {
		this.firstHalfScore = firstHalfScore;
	}
	public SecondHalfScore getSecondHalfScore() {
		return secondHalfScore;
	}
	public void setSecondHalfScore(SecondHalfScore secondHalfScore) {
		this.secondHalfScore = secondHalfScore;
	}
	
	
}
