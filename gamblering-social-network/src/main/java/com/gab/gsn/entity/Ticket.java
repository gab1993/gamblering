package com.gab.gsn.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value="session")
public class Ticket implements Serializable{
		
	private static final long serialVersionUID = 144405419495243171L;

	private Set<MatchBet> matchBets;
	
	private int stake;
	
	private double odd;
	
	public Ticket(){
		matchBets = new HashSet<MatchBet>();
		odd = 1.0;
	}

	public Set<MatchBet> getMatchBets() {
		return matchBets;
	}
	
	public void setMatchBets(Set<MatchBet> matchBets) {
		this.matchBets = matchBets;
	}
	
	public void addMatchBet(MatchBet bet){
		this.matchBets.add(bet);
		this.odd = this.odd * bet.getOdd();
	}
	
	public Set<Match> getMatchesSet(){
		Set<Match> ms = new HashSet<Match>();
		for(MatchBet m: matchBets)
			ms.add(m.getMatch());
		return ms;
	}
	public int getStake() {
		return stake;
	}

	public void setStake(int stake) {
		this.stake = stake;
	}
	
	public double getOdd() {
		return odd;
	}

	public void setOdd(double odd) {
		this.odd = odd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((matchBets == null) ? 0 : matchBets.hashCode());
		result = prime * result + stake;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Ticket))
			return false;
		Ticket other = (Ticket) obj;
		if (matchBets == null) {
			if (other.matchBets != null)
				return false;
		} else if (!matchBets.equals(other.matchBets))
			return false;
		if (stake != other.stake)
			return false;
		return true;
	}

	public void clearTicket() {
		matchBets.clear();
		odd = 1.0;
		stake = 0;
	}

	public void deleteMatch(String home_team, String away_team) {
		for(MatchBet mb: matchBets)
			if(mb.getMatch().getHome_team().getTeam_name().equals(home_team) && mb.getMatch().getAway_team().getTeam_name().equals(away_team)){
				matchBets.remove(mb);
				odd = odd/mb.getOdd();
				break;
		}
	}
	
	
}
