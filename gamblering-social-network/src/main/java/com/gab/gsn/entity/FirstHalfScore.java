package com.gab.gsn.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="FirstHalfScore")
public class FirstHalfScore implements Serializable {

	private static final long serialVersionUID = 327809745442107104L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="fhs_id_seq")
    @SequenceGenerator(name="fhs_id_seq", sequenceName="fhs_id_seq")
	private long score_id;
	
	public long getScore_id() {
		return score_id;
	}
	public void setScore_id(long score_id) {
		this.score_id = score_id;
	}
	
	@OneToOne
	@JoinColumn(name="match_id")
	private Match match;
	
	private int home_team_goals;
	
	private int away_team_goals;
	
	public Match getMatch() {
		return match;
	}
	public void setMatch(Match match) {
		this.match = match;
	}
	public int getHome_team_goals() {
		return home_team_goals;
	}
	public void setHome_team_goals(int home_team_goals) {
		this.home_team_goals = home_team_goals;
	}
	public int getAway_team_goals() {
		return away_team_goals;
	}
	public void setAway_team_goals(int away_team_goals) {
		this.away_team_goals = away_team_goals;
	}	
}
