package com.gab.gsn.entity;

import java.util.List;

public class MatchBetList {
	
	private List<MatchBet> matchBetList;

	public List<MatchBet> getMatchBetList() {
		return matchBetList;
	}

	public void setMatchBetList(List<MatchBet> matchBetList) {
		this.matchBetList = matchBetList;
	}
	

}
