package com.gab.gsn.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class Profile implements Serializable {

	private static final long serialVersionUID = 8427009643632030394L;
	
	@Id
	@Column(name = "username")
	private String username;
	
	@Column(name = "first_name")
	private String first_name;
	
	@Column(name = "second_name")
	private String second_name;
	
	@Column(name = "gender")
	private String gender;
	
	@Column(name = "birthdate")
	private String birthdate;
	
	@Column(name = "avatar_url")
	private String avatar_url;
	
	@OneToOne
	@JoinColumn(name="tipsterId")
	private Tipster tipster;
	
	@Column(name = "updated")
	private int updated;
	
	@Column(name = "country")
	private String country;
	
	@Column(name = "city")
	private String city;
	
	@Column(name = "betagency")
	private String betagency;
	
	@Column(name = "website")
	private String website;
	
	@Column(name = "description")
	private String description;
	
	@ManyToMany(fetch=FetchType.EAGER)
	@Fetch(value = FetchMode.SELECT)
	@JoinTable(name="profile_followers")
	private List<Profile> followers;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getSecond_name() {
		return second_name;
	}

	public void setSecond_name(String second_name) {
		this.second_name = second_name;
	}

	public String getAvatar_url() {
		return avatar_url;
	}

	public void setAvatar_url(String avatar_url) {
		this.avatar_url = avatar_url;
	}

	public Tipster getTipster() {
		return tipster;
	}

	public void setTipster(Tipster tipster) {
		this.tipster = tipster;
	}
	
	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}
	
	
	public int getUpdated() {
		return updated;
	}

	public void setUpdated(int updated) {
		this.updated = updated;
	}

	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getBetagency() {
		return betagency;
	}

	public void setBetagency(String betagency) {
		this.betagency = betagency;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public List<Profile> getFollowers() {
		return followers;
	}

	public void setFollowers(List<Profile> followers) {
		this.followers = followers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((first_name == null) ? 0 : first_name.hashCode());
		result = prime * result
				+ ((second_name == null) ? 0 : second_name.hashCode());
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Profile))
			return false;
		Profile other = (Profile) obj;
		if (first_name == null) {
			if (other.first_name != null)
				return false;
		} else if (!first_name.equals(other.first_name))
			return false;
		if (second_name == null) {
			if (other.second_name != null)
				return false;
		} else if (!second_name.equals(other.second_name))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
	
	
	
}
